<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| System Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'as' => 'system.'], function () {
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login-form');
    Route::post('login', 'Auth\LoginController@login')->name('login');

    Route::get('/dashboard', 'AppController')->name('dashboard');

    Route::post('logout', 'Auth\LoginController@logout')->name('logout');
});
