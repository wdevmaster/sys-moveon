<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Tenant Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'app', 'as' => 'tenant.'], function () {
    // Route Login
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login-form');
    Route::post('login', 'Auth\LoginController@login')->name('login');

    Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register-form');
    Route::post('register', 'Auth\RegisterController@register')->name('register');

    Route::group([
        'prefix' => '{code}',
        'middleware' => 'redirect'
    ], function () {
        // Route Dashboard
        Route::get('/dashboard', 'AppController')->name('dashboard');

        // Route Teachers
        Route::get('teachers/{type}', 'TeacherController@index')->name('teachers.index');
        Route::get('teachers/{type}/create', 'TeacherController@create')->name('teachers.create');
        Route::get('teachers/{type}/{teacher}/show', 'TeacherController@edit')->name('teachers.show');
        Route::get('teachers/{type}/{teacher}/edit', 'TeacherController@edit')->name('teachers.edit');
        Route::put('teachers/{employee}/restore', 'EmployeeController@restore')->name('teachers.restore');
        Route::put('teachers/manager', 'TeacherController@manager')->name('teachers.manager');
        Route::resource('teachers', 'TeacherController')->except(['index', 'create', 'show', 'edit']);

        // Route Employees
        Route::resource('employees', 'EmployeeController')->except(['index', 'create', 'show', 'edit']);
        Route::get('employees/{role}', 'EmployeeController@index')->name('employees.index');
        Route::get('employees/{role}/create', 'EmployeeController@create')->name('employees.create');
        Route::get('employees/{role}/{employee}/edit', 'EmployeeController@edit')->name('employees.edit');
        Route::get('employees/{role}/{employee}/show', 'EmployeeController@show')->name('employees.show');
        Route::put('employees/{employee}/manager', 'EmployeeController@manager')->name('employees.manager');
        Route::put('employees/{employee}/restore', 'EmployeeController@restore')->name('employees.restore');

        // Route Students
        Route::resource('students', 'StudentController')->except(['index', 'create', 'show', 'edit']);
        Route::get('students/{status}', 'StudentController@index')->name('students.index');
        Route::get('students/{status}/create', 'StudentController@create')->name('students.create');
        Route::get('students/{status}/{student}/show', 'StudentController@edit')->name('students.show');
        Route::get('students/{status}/{student}/edit', 'StudentController@edit')->name('students.edit');
        Route::put('students/{status}/manager', 'StudentController@manager')->name('students.manager');
        Route::put('students/{employee}/restore', 'StudentController@restore')->name('students.restore');

        // Route Representatives
        Route::resource('representatives', 'RepresentativeController')->except(['show']);
        Route::get('representatives/{representative}/show', 'RepresentativeController@edit')->name('representatives.show');
        Route::post('representatives/manager', 'RepresentativeController@manager')->name('representatives.manager');

        // Route Schedules
        Route::get('schedules/{model}/table', 'ScheduleController@index')->name('schedules.index');
        Route::get('schedules/teachers', 'TeacherController@getData')->name('schedules.teachers');
        Route::match(['post', 'put'], 'schedules/sync', 'ScheduleController@sync')->name('schedules.sync');

        // Route Courses
        Route::resource('courses', 'CourseController');
        Route::get('courses/{course}/students/add', 'CourseController@students')->name('courses.students');
        Route::put('courses/manager', 'CourseController@manager')->name('courses.manager');

        // Route Payments
        Route::resource('payments', 'PaymentController');
        Route::put('payments/{payment}/manager', 'PaymentController@manager')->name('payments.manager');

        // Route Settings
        Route::group(['prefix' => 'settings'], function () {

            // Route Branches
            Route::resource('branches', 'BranchController')->except(['show']);
            Route::get('branches/{branch}/show', 'BranchController@edit')->name('branches.show');
            Route::put('branches/{branch}/restore', 'BranchController@restore')->name('branches.restore');

            // Route Roles
            Route::resource('roles', 'RoleController');

            // Route Halls
            Route::get('halls', 'HallController@index')->name('halls.index');
            Route::get('halls/{hall}', 'HallController@show')->name('halls.show');
            Route::post('halls', 'HallController@sync')->name('halls.sync');
            Route::delete('halls/{hall}', 'HallController@destroy')->name('halls.destroy');
            Route::put('halls/manager', 'HallController@manager')->name('halls.manager');

            // Route Lapses
            Route::get('lapses', 'LapseController@index')->name('lapses.index');
            Route::match(['post', 'put'], 'lapses/sync', 'LapseController@sync')->name('lapses.sync');
            Route::delete('lapses/{lapse}/delete', 'LapseController@destroy')->name('lapses.destroy');

            Route::resource('plans', 'PlanController')->except(['show']);
            Route::get('plans/{plan}/show', 'PlanController@edit')->name('plans.show');
            Route::put('plans/{pla}/restore', 'PlanController@restore')->name('plans.restore');
        });

        // Route User Profile
        Route::get('user/profile', 'ProfileController@form')->name('user.profile');
        Route::put('users/profile', 'ProfileController@sync')->name('user.sync');

        // Route Logout
        Route::post('logout', 'Auth\LoginController@logout')->name('logout');

        // ------------------------------------ Routest Ajax ------------------------------------ //
        // Route Reprents
        Route::get('representatives/api/get', 'RepresentativeController@get')->name('api.representatives.get');

        // Route Teachers
        Route::get('teachers/api/get', 'TeacherController@indexAPI')->name('teachers.api.index');

        // Route halss
        Route::get('halls/api/get', 'HallController@indexAPI')->name('halls.api.index');

        // Route Students
        Route::get('students/api/index', 'StudentController@indexAPI')->name('students.api.index');

        // Route Course
        Route::get('courses/api/{course}/get', 'CourseController@indexAPI')->name('courses.api.index');

        // Route Course Students
        Route::get('courses/api/students', 'CourseController@getStudent')->name('courses.api.students');
        Route::put('courses/api/{course}/sync', 'CourseController@sync')->name('courses.api.sync');

        // Route Plans
        Route::get('plans/api/{plan}/get', 'PlanController@indexAPI')->name('plans.api.index');

        // Route Payments
        Route::get('payments/api/get', 'PaymentController@indexAPI')->name('payments.api.get');

        //Route Schedules
        Route::get('schedules/api/get', 'ScheduleController@getData')->name('schedules.api.index');

    });
});
