<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class RedirectTenancyMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (
            Auth::check() &&
            Hash::check('password', Auth::user()->password) &&
            !$request->is('app/*/user/profile') &&
            $request->isMethod('get')
        ) {

            return redirect(tenant_route('user.profile'))->with([
                'warning' => __('Please fill in your profile details.')
            ]);
        }

        return $next($request);
    }
}
