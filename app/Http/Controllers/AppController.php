<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tenant\Branch;
use App\Models\System\Franchise;

class AppController extends Controller
{
    public function getFranchises(Request $request)
    {
        $branches = [];
        foreach (Franchise::get() as $franchise) {
            \Tenancy::setTenant($franchise);

            foreach (Branch::get() as $branch) {
                $name = $branch->name;

                if ($address = $branch->address)
                    $name .= ' ('.ucfirst($address->city_str).', '.strtoupper($address->country_str).')';
                else
                    $name .= ' (Online)';

                $branches["$franchise->code-$branch->id"] = $name;
            }
        }

        return $branches;
    }
}
