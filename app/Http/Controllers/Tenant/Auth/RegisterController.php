<?php

namespace App\Http\Controllers\Tenant\Auth;

use App\Enums;
use App\Models;
use Carbon\Carbon;
use App\Models\Tenant;
use Illuminate\Http\Request;
use App\Models\System\Franchise;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    use RegistersUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        \View::share('pageConfigs', [
            'bodyClass' => "bg-full-screen-image",
            'blankPage' => true
        ]);
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\View\View
     */
    public function showRegistrationForm()
    {
        return view('auth.register')->with([
            'countries' => Models\Country::pluck('name', 'code'),
            'languages' => Models\System\Language::pluck('name', 'id'),
            'origins'   => Enums\UserOrigin::choices()
        ]);
    }

    /**
     * Validate the user regoister request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateRegister(Request $request)
    {
        $split = explode('-', $request->get('franchise'));
        $request->merge([
            'franchise' => $split[0],
            'branch' => $split[1]
        ]);

        $request->validate([
            'franchise' => 'required|string|exists:franchises,code',
            'role'      => 'required|string',
            'email'     => 'required|string',
            'language'  => 'required|string',
        ]);
    }

    protected function validateUser(Request $request)
    {
        $request->validate([
            'email'     => 'unique:users,email',
            'username'  => 'unique:users,username',
        ]);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $this->validateRegister($request);

        if ($franchise = Franchise::findByCode($request->get('franchise')))
            \Tenancy::setTenant($franchise);

        $request->merge([
            'password' => 'password',
            'username' => explode('@', $request->get('email'))[0]
        ]);

        $this->validateUser($request);

        event(new Registered($user = $this->createUser($request)));

        $this->guard()->login($user);

        if ($response = $this->registered($request, $user))
            return $response;

        return $request->wantsJson()
                    ? new JsonResponse([], 201)
                    : redirect(
                        route(
                            'tenant.dashboard',
                            $request->get('franchise')
                        )
                      );
    }

    public function createUser(Request $request)
    {
        try {
            DB::connection('tenant')->beginTransaction();

            $user = Models\User::create(
                array_merge(
                    $request->only('username', 'email', 'origin'),
                    ['password' => bcrypt($request->get('password'))]
                )
            );

            $user->assignRoles($request->get('role'));
            $user->branches()->attach($request->get('branch'));
            $user->languages()->attach($request->get('language'));

            if ($request->get('role') == 'student')
                $user->enrollment()->create([
                    'status' => 'preenrolled',
                    'created_at' => date('Y-m-d H:i:s')
                ]);

            $user->personalData()->create(
                $request->only('first_name', 'last_name')
            );

            DB::connection('tenant')->commit();
            return $user;

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            Inspector::reportException($e);
            Log::debug($e->getMessage());
            return redirect()->back()->withInput()
                    ->with('error', __('Error: :msg', ['msg' => $e->getMessage()]));
        }
    }
}
