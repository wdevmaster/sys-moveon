<?php

namespace App\Http\Controllers\Tenant;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Inspector\Laravel\Facades\Inspector;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Repositories\Tenant\RepresentativeRepositoryInterface as RepresentativeRepository;

class RepresentativeController extends Controller
{
    /** @var RepresentativeRepositoryInterface */
    private $repository;

    public function __construct(RepresentativeRepository $repository) {
        $this->middleware('auth');
        $this->repository = $repository;
        \View::share('pageConfigs', [
            'pageHeader' => false
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($code)
    {
        $rows = $this->repository->all();
        return view('app.representative.index', compact('rows'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $code)
    {
        return view('app.representative.create')->with(
            array_merge(
                $this->repository::with(),
                $request->all()
            )
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::connection('tenant')->beginTransaction();
            $back = tenant_route('representatives.index');

            if ($request->has('user_id') && $request->get('user_id'))
                return redirect(tenant_route('students.create', [
                    'status' => 'active',
                    'representative' => $request->get('user_id')
                ]));

            $model = $this->repository->create($request->except('_token'));

            if ($request->has('student'))
                $back = tenant_route('students.create', [
                    'status' => 'active',
                    'representative' => $model->filter_id
                ]);

            DB::connection('tenant')->commit();
            return redirect($back)
                    ->with('success', __(':name has been created', [
                        'name' => ucfirst(__('Representative') )
                    ]));
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            Inspector::reportException($e);
            Log::debug($e->getMessage());
            return redirect()->back()->withInput()
                    ->with('error', __('Error: :msg', ['msg' => $e->getMessage()]));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($code, $id)
    {
        try {
            $data = $this->repository->find($id);

            return view('app.representative.edit', compact('data'))->with(
                $this->repository::with()
            );
        } catch (\Exception $e) {
            return redirect(tenant_route('representatives.index'))
                    ->with('error', __('Error: :msg', ['msg' => $e->getMessage()]));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $ocde, $id)
    {
        try {
            DB::connection('tenant')->beginTransaction();

            $this->repository->update(
                $request->except(['_token', '_method']),
                $id
            );

            DB::connection('tenant')->commit();
            return redirect(tenant_route('representatives.index'))
                    ->with('success', __(':name has been updated', [
                        'name' => ucfirst(__('Representative') )
                    ]));
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            Inspector::reportException($e);
            Log::debug($e->getMessage());
            return redirect()->back()->withInput()
                    ->with('error', __('Error: :msg', ['msg' => $e->getMessage()]));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($code, $id)
    {
        try {
            DB::connection('tenant')->beginTransaction();
                $this->repository->delete($id);
            DB::connection('tenant')->commit();
        } catch (ModelNotFoundException $e) {
            DB::connection('tenant')->rollBack();
            return response()->json(['error' => $e->getMessage()], 404);
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            Inspector::reportException($e);
            Log::debug($e->getMessage());
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function get(Request $request)
    {
        try {
            return $this->repository->get(
                $request->all()
            );
        } catch (\Exception $e) {
            \Log::debug(__('Error REPRENT GET: :msg', ['msg' => $e->getMessage()]));
            return response()->json([
                'success' => false,
                'error' => [
                    'code' => $e->getCode(),
                    'message' => $e->getMessage(),
                ],
            ], 500);
        }
    }

    public function manager(Request $request)
    {
        try {
            DB::beginTransaction();
            $this->repository->manager(
                $request->except(['_token', 'id']),
                $request->id
            );
            DB::commit();
        } catch (ModelNotFoundException $e) {
            DB::rollBack();
            return response()->json(['error' => $e->getMessage()], 404);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }
}
