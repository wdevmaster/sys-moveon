<?php

namespace App\Http\Controllers\Tenant;

use App\Enums;
use App\Models;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Inspector\Laravel\Facades\Inspector;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Repositories\Tenant\CourseRepositoryInterface as CourseRepository;

class CourseController extends Controller
{
    /** @var CourseRepositoryInterface */
    private $repository;

    public function __construct(CourseRepository $repository) {
        $this->middleware('auth');
        $this->repository = $repository;

        \View::share('pageConfigs', [
            'pageHeader' => false
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('app.course.index')->with([
            'rows' => $this->repository->all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('app.course.create')->with(
            $this->repository::with()
        );
    }

    public function indexAPI($code, $id)
    {
        try {
            $rows = [];
            $courses = $this->repository->model->where('branch_id', $id)->get();
            foreach ($courses as $course) {
                $coursable = $course->coursables->last();
                $rows[$coursable->id] ="{$course->name} {$course->level->name}";
            }

            return $rows;
        } catch (ModelNotFoundException $e) {
            DB::connection('tenant')->rollBack();
            return response()->json(['error' => $e->getMessage()], 404);
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            Inspector::reportException($e);
            Log::debug($e->getMessage());
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function store(Request $request, $code)
    {
        try {
            DB::connection('tenant')->beginTransaction();

            $this->repository->create($request->except('_token'));

            DB::connection('tenant')->commit();
            return redirect(tenant_route('courses.index'))
                    ->with('success', __(':name has been created', [
                        'name' => ucfirst(__('Course') )
                    ]));

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            Inspector::reportException($e);
            Log::debug($e->getMessage());
            return redirect()->back()->withInput()
                    ->with('error', __('Error: :msg', ['msg' => $e->getMessage()]));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($code, $id)
    {
        try {
            $data = $this->repository->find($id);

            return view('app.course.edit', compact('data'))->with(
                $this->repository::with()
            );
        } catch (\Exception $e) {
            return redirect(tenant_route('courses.index'))
                    ->with('error', __('Error: :msg', ['msg' => $e->getMessage()]));
        }
    }

    public function students($code, $id)
    {
        try {
            $data = $this->repository->find($id);

            return view('app.course.show', compact('data'))->with(
                $this->repository::with()
            );
        } catch (\Exception $e) {
            return redirect(tenant_route('courses.index'))
                    ->with('error', __('Error: :msg', ['msg' => $e->getMessage()]));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $code, $id)
    {
        try {
            DB::connection('tenant')->beginTransaction();

            $this->repository->update(
                $request->except(['_token', '_method']),
                $id
            );

            DB::connection('tenant')->commit();
            return redirect(tenant_route('courses.index'))
                    ->with('success', __(':name has been updated', [
                        'name' => ucfirst(__('Course') )
                    ]));
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            Inspector::reportException($e);
            Log::debug($e->getMessage());
            return redirect()->back()->withInput()
                    ->with('error', __('Error: :msg', ['msg' => $e->getMessage()]));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($code, $id)
    {
        try {
            DB::connection('tenant')->beginTransaction();
                $this->repository->delete($id);
            DB::connection('tenant')->commit();
        } catch (ModelNotFoundException $e) {
            DB::connection('tenant')->rollBack();
            return response()->json(['error' => $e->getMessage()], 404);
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            Inspector::reportException($e);
            Log::debug($e->getMessage());
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function sync(Request $request, $code, $id)
    {
        try {
            $model = $this->repository->find($id);
            DB::connection('tenant')->beginTransaction();
                $model = $model->coursables->last();

                if ($request->type == 'attach')
                    $model->students()->attach($request->id, ['plan_id' => $model->plan_id]);
                elseif ($request->type == 'detach')
                    $model->students()->detach($request->id);
                elseif ($request->type == 'sync_plan') {
                    $model->students()->detach($request->id);
                    $model->students()->attach($request->id, ['plan_id' => $request->plan_id]);
                }
            DB::connection('tenant')->commit();
        } catch (ModelNotFoundException $e) {
            DB::connection('tenant')->rollBack();
            return response()->json(['error' => $e->getMessage()], 404);
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            Inspector::reportException($e);
            Log::debug($e->getMessage());
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function getStudent(Request $request, $code)
    {
        try {
            $students = [];
            $rows = $this->repository->getStudent($request->except('_'));
            if ($rows->isNotEmpty())
                foreach ($rows as $row) {
                    $students[] = [
                        'id' => $row->id,
                        'profile_photo' => $row->profile_photo,
                        'ci' => $row->personalData->ci,
                        'fullname' => $row->fullname,
                        'plan_id' => $row->pivot->plan_id
                    ];
                }

            return response()->json($students, 200);
        } catch (\Exception $e) {
            Inspector::reportException($e);
            Log::debug($e->getMessage());
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }
}
