<?php

namespace App\Http\Controllers\Tenant;

use App\Enums;
use App\Models\Tenant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Inspector\Laravel\Facades\Inspector;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Repositories\Tenant\PaymentRepositoryInterface as PaymentRepository;

class PaymentController extends Controller
{
    /** @var PaymentRepositoryInterface */
    private $repository;

    public function __construct(PaymentRepository $repository) {
        $this->middleware('auth');
        $this->repository = $repository;

        \View::share('pageConfigs', [
            'pageHeader' => false
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('app.payment.index')->with([
            'rows' => $this->repository->all()
        ]);
    }

    public function indexAPI(Request $request)
    {
        try {
            return response()->json(
                $this->repository->getPlanDues($request->all()),
            200);
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            Inspector::reportException($e);
            Log::debug($e->getMessage());
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('app.payment.create')->with(
            $this->repository->with()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::connection('tenant')->beginTransaction();

            $this->repository->create(
                $request->except(['_token', 'branches'])
            );

            DB::connection('tenant')->commit();
            return redirect(tenant_route('payments.index'))
                    ->with('success', __(':name has been created', [
                        'name' => ucfirst(__('Payment') )
                    ]));
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            Inspector::reportException($e);
            Log::debug($e->getMessage());
            return redirect()->back()
                    ->with('error', __('Error: :msg', ['msg' => $e->getMessage()]));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($code, $id)
    {
        $data = $this->repository->model->where('code', $id)->get();
        $plan = $data->first()->plan;
        return view('app.payment.show', compact('data', 'plan'))->with(
            $this->repository::with()
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function manager(Request $request)
    {
        try {
            DB::connection('tenant')->beginTransaction();
            $this->repository->manager(
                $request->except(['_token', 'id']),
                $request->id
            );
            DB::connection('tenant')->commit();
        } catch (ModelNotFoundException $e) {
            DB::connection('tenant')->rollBack();
            return response()->json(['error' => $e->getMessage()], 404);
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }
}
