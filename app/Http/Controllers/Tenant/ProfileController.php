<?php

namespace App\Http\Controllers\Tenant;

use App\Enums;
use App\Models;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Inspector\Laravel\Facades\Inspector;
use App\Repositories\Tenant\StudentRepositoryInterface as StudentRepository;


class ProfileController extends Controller
{
    /** @var StudentRepositoryInterface */
    private $repository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(StudentRepository $repository) {
        $this->middleware('auth');
        $this->repository = $repository;
        \View::share('pageConfigs', [
            'pageHeader' => false
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function form()
    {
        return view('app.profile')->with([
            'countries' => Models\Country::pluck('name', 'code'),
            'sexs' => Enums\PersonalSex::choices(),
            'inst_types' => Enums\InstitutionType::choices(),
            'institutions' => Models\Tenant\Institution::select('name', 'type')->get(),
            'titleType' => Enums\AcademicTitle::choices(),
            'roles' => Models\Role::teacher()->get(),
            'branches' => Models\Tenant\Branch::get()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function sync(Request $request, $code)
    {
        try {
            DB::connection('tenant')->beginTransaction();

            $model = $this->repository->update(
                $request->except(['_token', '_method']),
                \Auth::id()
            );

            if (
                $model->enrollment->status->is_preenr &&
                !$model->enrollment->student_type_id
            ) {
                $model->enrollment()->update([
                    'student_type_id' => $this->getTypeStuden(
                        $model->personalData->age
                    )
                ]);
            }

            DB::connection('tenant')->commit();
            return redirect()->back()
                    ->with('success', __(':name has been updated', [
                        'name' => ucfirst(__('Profile') )
                    ]));
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            Inspector::reportException($e);
            Log::debug($e->getMessage());
            return redirect()->back()->withInput()
                    ->with('error', __('Error: :msg', ['msg' => $e->getMessage()]));
        }
    }

    private function getTypeStuden($age)
    {
        foreach (Models\Tenant\StudentType::get() as $row)
            if ($age >= $row->age_from && $age < $row->age_up)
                break;

        return $row->id;
    }
}
