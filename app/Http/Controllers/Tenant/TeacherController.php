<?php

namespace App\Http\Controllers\Tenant;

use App\Enums;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Inspector\Laravel\Facades\Inspector;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Repositories\Tenant\TeacherRepositoryInterface as TeacherRepository;
use App\Repositories\Tenant\ScheduleRepositoryInterface as ScheduleRepository;

class TeacherController extends Controller
{
    /** @var TeacherRepositoryInterface */
    private $repository;
    private $schedule;

    public function __construct(TeacherRepository $repository, ScheduleRepository $schedule) {
        $this->middleware('auth');
        $this->repository = $repository;
        $this->schedule = $schedule;

        \View::share('pageConfigs', [
            'pageHeader' => false
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($code, $type)
    {
        $type = new Enums\TeacherType($type);
        $rows = $this->repository->all($type);
        return view('app.teacher.index', compact('rows', 'type'))->with([
            'teacherStatus' => new Enums\TeacherStatus()
        ]);
    }

    public function indexAPI(Request $request, $code)
    {
        try {
            return response()->json(
                $this->repository->allWith($request->all()),
            200);
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            Inspector::reportException($e);
            Log::debug($e->getMessage());
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function getData(Request $request, $code)
    {
        if (!$request->get('id'))
            return [];

        $teachers = [];
        $rows = $this->repository->model->find(json_decode($request->get('id'), true));

        if ($rows->isNotEmpty())
            foreach ($rows as $row) {
                $teachers[] = [
                    'profile_photo' => $row->profile_photo,
                    'ci' => $row->personalData->ci,
                    'fullname' => $row->fullname
                ];
            }

        return collect($teachers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($code, $type)
    {
        $type = new Enums\TeacherType($type);
        return view('app.teacher.create', compact('type'))->with(array_merge(
            $this->repository::with(),
            [ 'schedules' => $this->schedule->getLapses() ]
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::connection('tenant')->beginTransaction();

            $model = $this->repository->create($request->except('_token'));

            if ($request->get('schedules'))
                $this->schedule->sync([
                    'user_id' => $model->id,
                    'type' => Enums\ScheduleType::TEA(),
                    'data' => $this->schedule->format(
                        $request->get('schedules'), [
                        'user_id' => $model->id
                    ])
                ]);

            DB::connection('tenant')->commit();
            return redirect(tenant_route('teachers.index', ['type' => $request->get('type')]))
                    ->with('success', __(':name has been created', [
                        'name' => ucfirst(__('Teacher') )
                    ]));
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            Inspector::reportException($e);
            Log::debug($e->getMessage());
            return redirect()->back()->withInput()
                    ->with('error', __('Error: :msg', ['msg' => $e->getMessage()]));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($code, $type, $id)
    {
        try {
            $type = new Enums\TeacherType($type);
            $data = $this->repository->find($id);

            return view('app.teacher.edit', compact('data', 'type'))->with(
                $this->repository::with()
            );
        } catch (\Exception $e) {
            return redirect(tenant_route('teachers.index', ['type' => $type]))
                    ->with('error', __('Error: :msg', ['msg' => $e->getMessage()]));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($code, $type, $id)
    {
        try {
            $type = new Enums\TeacherType($type);
            $data = $this->repository->find($id);

            return view('app.teacher.edit', compact('data', 'type'))
                    ->with(array_merge(
                        $this->repository::with(),
                        [ 'schedules' => $this->schedule->getLapses() ]
                    ));
        } catch (\Exception $e) {
            return redirect(tenant_route('teachers.index', ['type' => $type]))
                    ->with('error', __('Error: :msg', ['msg' => $e->getMessage()]));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $code, $id)
    {
        try {
            DB::connection('tenant')->beginTransaction();

            $this->repository->update(
                $request->except(['_token', '_method']),
                $id
            );

            if ($request->get('schedules'))
                $this->schedule->sync([
                    'user_id' => $id,
                    'type' => Enums\ScheduleType::TEA(),
                    'data' => $this->schedule->format(
                        $request->get('schedules'), [
                        'user_id' => $id
                    ])
                ]);

            DB::connection('tenant')->commit();
            return redirect(tenant_route('teachers.index', ['type' => $request->get('type')]))
                    ->with('success', __(':name has been updated', [
                        'name' => ucfirst(__('Teacher') )
                    ]));
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            Inspector::reportException($e);
            Log::debug($e->getMessage());
            return redirect()->back()->withInput()
                    ->with('error', __('Error: :msg', ['msg' => $e->getMessage()]));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($code, $id)
    {
        try {
            DB::connection('tenant')->beginTransaction();
                $this->repository->delete($id);
            DB::connection('tenant')->commit();
        } catch (ModelNotFoundException $e) {
            DB::connection('tenant')->rollBack();
            return response()->json(['error' => $e->getMessage()], 404);
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            Inspector::reportException($e);
            Log::debug($e->getMessage());
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function restore($code, $id)
    {
        try {
            DB::connection('tenant')->beginTransaction();
                $this->repository->restore($id);
            DB::connection('tenant')->commit();
        } catch (ModelNotFoundException $e) {
            DB::connection('tenant')->rollBack();
            return response()->json(['error' => $e->getMessage()], 404);
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            Inspector::reportException($e);
            Log::debug($e->getMessage());
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function manager(Request $request)
    {
        try {
            DB::beginTransaction();
            $this->repository->manager(
                $request->except(['_token', 'id']),
                $request->id
            );
            DB::commit();
        } catch (ModelNotFoundException $e) {
            DB::rollBack();
            return response()->json(['error' => $e->getMessage()], 404);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }
}
