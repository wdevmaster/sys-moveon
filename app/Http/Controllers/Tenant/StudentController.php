<?php

namespace App\Http\Controllers\Tenant;

use App\Enums;
use App\Models;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Inspector\Laravel\Facades\Inspector;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Repositories\Tenant\StudentRepositoryInterface as StudentRepository;

class StudentController extends Controller
{
    /** @var StudentRepositoryInterface */
    private $repository;

    public function __construct(StudentRepository $repository) {
        $this->middleware('auth');
        $this->repository = $repository;
        \View::share('pageConfigs', [
            'pageHeader' => false
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($code, $status)
    {
        $status = new Enums\StudentStatus($status);
        $rows = $this->repository->all($status);
        return view('app.student.index', compact('rows', 'status'));
    }

    public function indexAPI(Request $request, $code)
    {
        try {
            $students = [];
            $rows = $this->repository->allWith($request->except('_'));

            if ($rows->isNotEmpty())
                foreach ($rows as $row) {
                    $students[] = [
                        'id' => $row->id,
                        'profile_photo' => $row->profile_photo,
                        'ci' => $row->personalData->ci,
                        'fullname' => $row->fullname
                    ];
                }

            return response()->json($students, 200);
        } catch (\Exception $e) {
            Inspector::reportException($e);
            Log::debug($e->getMessage());
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $code, $status)
    {
        if ($request->has('representative') && $request->get('representative'))
            $represent = Models\User::find($request->get('representative'));


        $status = new Enums\StudentStatus($status);
        return view('app.student.create', compact('status'))->with(
            array_merge(
                $this->repository::with(),
                isset($represent)
                    ? [
                        'ci' => $represent->ci_student,
                        'rif' => $represent->personalData->rif,
                    ] : []
            )
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::connection('tenant')->beginTransaction();

            $this->repository->create($request->except('_token'));

            DB::connection('tenant')->commit();
            return redirect(tenant_route('students.index', ['status' => $request->get('status')]))
                    ->with('success', __(':name has been created', [
                        'name' => ucfirst(__('Student') )
                    ]));
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            Inspector::reportException($e);
            Log::debug($e->getMessage());
            return redirect()->back()->withInput()
                    ->with('error', __('Error: :msg', ['msg' => $e->getMessage()]));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($code, $status, $id)
    {
        try {
            $status = new Enums\StudentStatus($status);
            $data = $this->repository->find($id);

            return view('app.student.edit', compact('data', 'status'))->with(
                $this->repository::with()
            );
        } catch (\Exception $e) {
            return redirect(tenant_route('students.index', ['status' => $status]))
                    ->with('error', __('Error: :msg', ['msg' => $e->getMessage()]));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($code, $status, $id)
    {
       try {
            $status = new Enums\StudentStatus($status);
            $data = $this->repository->find($id);

            return view('app.student.edit', compact('data', 'status'))->with(
                $this->repository::with()
            );
        } catch (\Exception $e) {
            return redirect(tenant_route('students.index', ['status' => $status]))
                    ->with('error', __('Error: :msg', ['msg' => $e->getMessage()]));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $code, $id)
    {
        try {
            DB::connection('tenant')->beginTransaction();

            $this->repository->update(
                $request->except(['_token', '_method']),
                $id
            );

            DB::connection('tenant')->commit();
            return redirect(tenant_route('students.index', ['status' => $request->get('status')]))
                    ->with('success', __(':name has been updated', [
                        'name' => ucfirst(__('Student') )
                    ]));
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            Inspector::reportException($e);
            Log::debug($e->getMessage());
            return redirect()->back()->withInput()
                    ->with('error', __('Error: :msg', ['msg' => $e->getMessage()]));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($code, $id)
    {
        try {
            DB::connection('tenant')->beginTransaction();
                $this->repository->delete($id);
            DB::connection('tenant')->commit();
        } catch (ModelNotFoundException $e) {
            DB::connection('tenant')->rollBack();
            return response()->json(['error' => $e->getMessage()], 404);
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            Inspector::reportException($e);
            Log::debug($e->getMessage());
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function restore($code, $id)
    {
        try {
            DB::connection('tenant')->beginTransaction();
                $this->repository->restore($id);
            DB::connection('tenant')->commit();
        } catch (ModelNotFoundException $e) {
            DB::connection('tenant')->rollBack();
            return response()->json(['error' => $e->getMessage()], 404);
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            Inspector::reportException($e);
            Log::debug($e->getMessage());
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function manager(Request $request)
    {
        try {
            DB::beginTransaction();
            $this->repository->manager(
                $request->except(['_token', 'id']),
                $request->id
            );
            DB::commit();
        } catch (ModelNotFoundException $e) {
            DB::rollBack();
            return response()->json(['error' => $e->getMessage()], 404);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }
}
