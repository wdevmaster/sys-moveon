<?php

namespace App\Http\Controllers\Tenant;

use App\Enums;
use App\Models;
use App\Models\Tenant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Inspector\Laravel\Facades\Inspector;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Repositories\Tenant\ScheduleRepositoryInterface as ScheduleRepository;
use App\Repositories\Tenant\TeacherRepositoryInterface as TeacherRepository;

class ScheduleController extends Controller
{
    /** @var ScheduleRepositoryInterface */
    private $repository;
    private $teacher;

    public function __construct(ScheduleRepository $repository, TeacherRepository $teacher) {
        $this->middleware('auth');
        $this->repository = $repository;
        $this->teacher = $teacher;

        \View::share('pageConfigs', [
            'pageHeader' => false
        ]);
    }

    public function index($code, $model)
    {
        return view('app.schedule.index')->with(array_merge(
            $this->repository->with(),
            [
                'model' => $model,
                'schedules' => $this->repository->getLapses()
            ]
        ));
    }

    public function getData(Request $request)
    {
        return response()->json(
            $this->repository->allWithinHours($request->all()),
            200
        );
    }

}
