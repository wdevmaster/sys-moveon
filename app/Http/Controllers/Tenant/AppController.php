<?php

namespace App\Http\Controllers\Tenant;

use Illuminate\Http\Request;
use App\Traits\AppController as BaseApp;
use App\Http\Controllers\Controller;

class AppController extends Controller
{
    use BaseApp;
}
