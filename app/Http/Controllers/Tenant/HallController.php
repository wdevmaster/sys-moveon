<?php

namespace App\Http\Controllers\Tenant;

use App\Enums;
use App\Models\Tenant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Inspector\Laravel\Facades\Inspector;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Repositories\Tenant\HallRepositoryInterface as HallRepository;

class HallController extends Controller
{
    /** @var HallRepositoryInterface */
    private $repository;

    public function __construct(HallRepository $repository) {
        $this->middleware('auth');
        $this->repository = $repository;

        \View::share('pageConfigs', [
            'pageHeader' => false
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rows = $this->repository->all();
        return view('app.hall.index', compact('rows'))->with(
            $this->repository->with()
        );
    }

    public function indexAPI(Request $request, $code)
    {
        try {
            $data = collect($request->all());
            $query = Tenant\Hall::where('branch_id', $data->get('branch_id'));

            if ($data->get('course'))
                $query->doesntHaveCourse(
                    $data->only(['days', 'schedule_id'])->toArray()
                );

            return response()->json($query->get()->pluck('name', 'id'), 200);
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            Inspector::reportException($e);
            Log::debug($e->getMessage());
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($code, $id)
    {
        try {
            return $this->repository->find($id);
        } catch (ModelNotFoundException $e) {
            DB::connection('tenant')->rollBack();
            return response()->json(['error' => $e->getMessage()], 404);

        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            Inspector::reportException($e);
            Log::debug($e->getMessage());
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * Create or Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function sync(Request $request, $code)
    {
        try {
            DB::connection('tenant')->beginTransaction();

            $this->repository->sync(
                $request->except(['_token']),
            );

            DB::connection('tenant')->commit();
            return redirect()->back()
                    ->with('success',
                        !$request->get('id')
                            ? __(':name has been created', ['name' => ucfirst(__('Hall') )])
                            : __(':name has been updated', ['name' => ucfirst(__('Hall') )])
                    );
        } catch (\Exception $e) {

            DB::connection('tenant')->rollBack();
            Inspector::reportException($e);
            Log::debug($e->getMessage());
            return redirect()->back()->withInput()
                    ->with('error', __('Error: :msg', ['msg' => $e->getMessage()]));

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($code, $id)
    {
        try {
            DB::connection('tenant')->beginTransaction();
                $this->repository->delete($id);
            DB::connection('tenant')->commit();
        } catch (ModelNotFoundException $e) {
            DB::connection('tenant')->rollBack();
            return response()->json(['error' => $e->getMessage()], 404);
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            Inspector::reportException($e);
            Log::debug($e->getMessage());
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function manager(Request $request)
    {
        try {
            DB::connection('tenant')->beginTransaction();
            $this->repository->manager(
                $request->except(['_token', 'id']),
                $request->id
            );
            DB::connection('tenant')->commit();
        } catch (ModelNotFoundException $e) {
            DB::connection('tenant')->rollBack();
            return response()->json(['error' => $e->getMessage()], 404);
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }
}
