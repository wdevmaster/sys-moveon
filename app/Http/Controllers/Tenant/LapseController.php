<?php

namespace App\Http\Controllers\Tenant;

use App\Enums;
use App\Models\Tenant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Inspector\Laravel\Facades\Inspector;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Repositories\Tenant\ScheduleRepositoryInterface as ScheduleRepository;

class LapseController extends Controller
{
    /** @var ScheduleRepositoryInterface */
    private $repository;

    public function __construct(ScheduleRepository $repository) {
        $this->middleware('auth');
        $this->repository = $repository;

        \View::share('pageConfigs', [
            'pageHeader' => false
        ]);
    }

    public function index()
    {
        return view('app.lapse.index')->with(array_merge(
            $this->repository->with(),
            [ 'rows' => $this->repository->all(Enums\ScheduleType::LAP()) ]
        ));
    }

    public function sync(Request $request, $code)
    {
        $request->merge([
            'type' => Enums\ScheduleType::LAP(),
            'data' => json_decode($request->get('data'), true)
        ]);
        try {
            DB::connection('tenant')->beginTransaction();
            $this->repository->sync($request->except('_token'));
            DB::connection('tenant')->commit();

            return redirect()->back()
                    ->with('success', __(':name has been created', [
                        'name' => ucfirst(__('Lapses') )
                    ]));
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            Inspector::reportException($e);
            Log::debug($e->getMessage());
            return redirect()->back()->withInput()
                    ->with('error', __('Error: :msg', ['msg' => $e->getMessage()]));
        }
    }

    public function destroy($code, $id)
    {
        try {
            DB::connection('tenant')->beginTransaction();
            $this->repository->delete($id);
            DB::connection('tenant')->commit();
        } catch (ModelNotFoundException $e) {
            DB::connection('tenant')->rollBack();
            return response()->json(['error' => $e->getMessage()], 404);
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            Inspector::reportException($e);
            Log::debug($e->getMessage());
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }
}
