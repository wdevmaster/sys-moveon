<?php

namespace App\Http\Controllers\Tenant;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Inspector\Laravel\Facades\Inspector;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Repositories\RoleRepositoryInterface as RoleRepository;

class RoleController extends Controller
{
    /** @var RoleRepositoryInterface */
    private $repository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(RoleRepository $repository) {
        $this->middleware('auth');
        $this->repository = $repository;
        \View::share('pageConfigs', [
            'pageHeader' => false
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rows = $this->repository->all();
        return view('app.role.index', compact('rows'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('app.role.create')->with(
            $this->repository::with()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::connection('tenant')->beginTransaction();

            $role = $this->repository->createWithPermissions(
                $request->only('name'),
                $request->permissions ?: []
            );

            DB::connection('tenant')->commit();
            return redirect(tenant_route('roles.index'))
                    ->with('success', __(':name has been created', [
                        'name' => ucfirst(__('Role') )
                    ]));
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            Inspector::reportException($e);
            Log::debug($e->getMessage());
            return redirect()->back()->withInput()
                    ->with('error', __('Error: :msg', ['msg' => $e->getMessage()]));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($code, $id)
    {
        try {
            $role = $this->repository->find($id);

            return view('app.role.edit', compact('role'))->with(
                $this->repository::with()
            );
        } catch (\Exception $e) {
            return redirect(tenant_route('roles.index'))
                    ->with('error', __('Error: :msg', ['msg' => $e->getMessage()]));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $code, $id)
    {
        try {
            DB::connection('tenant')->beginTransaction();

            $role = $this->repository->updateWithPermissions(
                $id,
                $request->only('name'),
                $request->permissions ?: []
            );

            DB::connection('tenant')->commit();
            return redirect(tenant_route('roles.index'))
                    ->with('success', __(':name has been updated', [
                        'name' => ucfirst(__('Role') )
                    ]));
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            Inspector::reportException($e);
            Log::debug($e->getMessage());
            return redirect()->back()->withInput()
                    ->with('error', __('Error: :msg', ['msg' => $e->getMessage()]));

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($code, $id)
    {
        try {
            DB::connection('tenant')->beginTransaction();
                $this->repository->delete($id);
            DB::connection('tenant')->commit();
        } catch (ModelNotFoundException $e) {
            DB::connection('tenant')->rollBack();
            return response()->json(['error' => $e->getMessage()], 404);
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            Inspector::reportException($e);
            Log::debug($e->getMessage());
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }
}
