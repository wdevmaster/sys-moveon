<?php

namespace App\Http\Controllers\Tenant;

use App\Http\Controllers\Controller;
use App\Traits\EmployeeController as BaseEmployee;

class EmployeeController extends Controller
{
    use BaseEmployee;
}
