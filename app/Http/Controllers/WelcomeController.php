<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    public function __construct() {
        \View::share('pageConfigs', [
            'bodyClass' => "bg-full-screen-image",
            'blankPage' => true
        ]);
    }

    public function __invoke()
    {
        return view('welcome');
    }
}
