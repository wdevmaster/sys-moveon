<?php

namespace App\Models\Tenant;

use App\Enums;
use Carbon\Carbon;
use App\Traits\EncryptModel;
use Konekt\Enum\Eloquent\CastsEnums;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Tenancy\Affects\Connections\Support\Traits\OnTenant;

class Schedule extends Model
{
    use OnTenant,
        CastsEnums,
        EncryptModel;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'schedules';

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = [];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    protected $enums = [
        'status' => Enums\HallStatus::class
    ];

    /** Relationships */
    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    /** Scope */
    public function scopeWithinHours(Builder $builder, $branch)
    {
        return $builder
                ->lapse()
                ->where('branch_id', $branch)
                ->orderBy('start_time');
    }

    public function scopeWhereBranch($query, $branch)
    {
        return $query->whereHas('user.branches', function($query) use ($branch) {
            $query->where('branch_id', $branch);
        });
    }

    public function scopeLapse($query)
    {
        return $query->where('type', Enums\ScheduleType::LAP);
    }

    public function scopeTeacher($query)
    {
        return $query->where('type', Enums\ScheduleType::TEA);
    }

    public function scopeBranch($query, $id)
    {
        return $query->where('branch_id', $id);
    }

    public function scopeBranchHas($query, $id)
    {
        return $query->whereHas('user.branches', function (Builder $query) use ($id) {
            $query->where('id', $id);
        });
    }

    /** Attribute */
    public function getStartTimeAttribute($value)
    {
        return Carbon::parse($value);
    }

    public function getEndTimeAttribute($value)
    {
        return Carbon::parse($value);
    }
}
