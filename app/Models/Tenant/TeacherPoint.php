<?php

namespace App\Models\Tenant;

use App\Enums\TeacherPointType;
use Konekt\Enum\Eloquent\CastsEnums;
use Illuminate\Database\Eloquent\Model;
use Tenancy\Affects\Connections\Support\Traits\OnTenant;

class TeacherPoint extends Model
{
    use OnTenant;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'teacher_points';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    protected $enums = [
        'type' => TeacherPointType::class,
    ];

    /** Relationships */
    public function teachers()
    {
    	return $this->belongsToMany(User::class);
    }

    /** Attribute */
    public function getValueAttribute($value)
    {
        return number_format($value, 2);
    }
}
