<?php

namespace App\Models\Tenant;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Tenancy\Affects\Connections\Support\Traits\OnTenant;


class StudentType extends Model
{
    use OnTenant;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'student_types';

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::creating(function ($model) {
            $model->slug = \Str::slug($model->name);
        });
    }

    /** Relationships */
    public function teachers()
    {
    	return $this->belongsToMany(User::class);
    }

    /** Static */
    public static function findByAge($age)
    {
        return self::whereIn('from', $age)->toSql();
    }

    /** Scopes */
    public function scopeWhereAge($query, $value)
    {
        return $query->where('age_from', '>=', $value)
                     ->where('age_up', 'a=', $value);
    }
}
