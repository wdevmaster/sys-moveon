<?php

namespace App\Models\Tenant;

use App\Models\System;
use App\Traits\EncryptModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Tenancy\Affects\Connections\Support\Traits\OnTenant;

class Course extends Model
{
    use OnTenant,
        SoftDeletes,
        EncryptModel;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'courses';

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['coursables'];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at ', 'updated_at' , 'deleted_at'];


    /** Relationships */
    public function branch()
 	{
 		return $this->belongsTo(Branch::class);
    }

    public function language()
 	{
 		return $this->belongsTo(System\Language::class);
    }

    public function level()
 	{
 		return $this->belongsTo(System\ProgramLevel::class, 'level_id');
    }

    public function studentType()
 	{
 		return $this->belongsTo(StudentType::class);
    }

    public function coursables()
    {
        return $this->hasMany(Coursable::class);
    }

    /** Scopes */
    public function scopeCoursable($query, $value)
    {
        return $query->whereHas('coursables', function (Builder $query) use ($value) {
            $query->where('id', $value);
        });
    }

    /** Attributes */
    public function getCoursableAttribute()
    {
        return $this->coursables->where('status', '!=', 'finished')->last();
    }

    public function getDurationWeekAttribute()
    {
        return $this->coursable->duration_week;
    }

    public function getPriceAttribute()
    {
        return number_format($this->coursable->price, 2);
    }

    public function getPlanIdAttribute()
    {
        return $this->coursable->plan_id;
    }
}
