<?php

namespace App\Models\Tenant;

use Konekt\Enum\Eloquent\CastsEnums;
use App\Enums\PersonalLogType;
use App\Enums\PersonalLogGroup;
use Illuminate\Database\Eloquent\Model;
use Tenancy\Affects\Connections\Support\Traits\OnTenant;

class PersonalLog extends Model
{
    use OnTenant,
        CastsEnums;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'personal_logs';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    protected $enums = [
        'type' => PersonalLogType::class,
        'group' => PersonalLogGroup::class
    ];

}
