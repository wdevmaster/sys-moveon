<?php

namespace App\Models\Tenant;

use Carbon\Carbon;
use Konekt\Enum\Eloquent\CastsEnums;
use App\Enums\InstitutionType;
use App\Enums\AcademicTitle;
use Illuminate\Database\Eloquent\Model;
use Tenancy\Affects\Connections\Support\Traits\OnTenant;

class PersonalAcademicData extends Model
{
    use OnTenant,
        CastsEnums;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'personal_academic_data';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    protected $enums = [
        'type' => InstitutionType::class,
    ];

    /** Relationships */
    public function institution()
    {
        return $this->belongsTo(Institution::class);
    }

    /** Mutators */
    public function getGraduateYearAttribute($value)
    {
        if ($value)
            return Carbon::parse($value)->format('d/m/Y');
    }

    public function getTitleAttribute($value)
    {
        if ($value)
            return new AcademicTitle($value);
    }

    public function getIsGraduatedAttribute()
    {
        return $this->graduate_year != null;
    }
}
