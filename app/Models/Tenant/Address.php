<?php

namespace App\Models\Tenant;

use App\Models\Country;
use App\Traits\EncryptModel;
use Illuminate\Database\Eloquent\Model;
use Tenancy\Affects\Connections\Support\Traits\OnTenant;

class Address extends Model
{
    use OnTenant,
        EncryptModel;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'addresses';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /** Relationships */
    public function country()
    {
        return $this->setConnection('mysql')->belongsTo(Country::class, 'country_str');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city_str');
    }

    /** Attributes */
    public function getFullAddressAttribute()
    {
        $string = $this->address;
        $string .= ", {$this->city->name}";
        $string .= ", {$this->country->native_name}";

        return $string;
    }
}
