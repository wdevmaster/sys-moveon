<?php

namespace App\Models\Tenant;

use App\Enums;
use App\Models;
use App\Traits\EncryptModel;
use Konekt\Enum\Eloquent\CastsEnums;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Tenancy\Affects\Connections\Support\Traits\OnTenant;

class Coursable extends Model
{
    use OnTenant,
        SoftDeletes,
        EncryptModel,
        CastsEnums;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'coursables';

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['days'];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at ', 'updated_at' , 'deletd_at'];

    protected $enums = [
        'status' => Enums\CoursableStatus::class,
    ];

    /** Relationships */
    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    public function schedule()
    {
        return $this->belongsTo(Schedule::class);
    }

    public function hall()
    {
        return $this->belongsTo(Hall::class);
    }

    public function days()
    {
        return $this->hasMany(CoursableDay::class, 'coursable_id');
    }

    public function user()
    {
        return $this->belongsTo(Models\User::class);
    }

    public function students()
    {
        return $this->belongsToMany(Models\User::class)->withPivot('plan_id');;
    }
}
