<?php

namespace App\Models\Tenant;

use Illuminate\Database\Eloquent\Relations\Pivot;
use Tenancy\Affects\Connections\Support\Traits\OnTenant;


class BranchProgram extends Pivot
{
    use OnTenant;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'branch_program';
}
