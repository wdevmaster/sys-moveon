<?php

namespace App\Models\Tenant;

use App\Models\System;
use App\Traits\EncryptModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Tenancy\Affects\Connections\Support\Traits\OnTenant;

class Branch extends Model
{
    use OnTenant,
        SoftDeletes,
        EncryptModel;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'branches';

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['address'];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at ', 'updated_at' , 'deleted_at'];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::creating(function ($model) {
            $model->slug = \Str::slug($model->name);
        });
    }

    public function deleteRelationships()
    {
        //
    }

    /** Relationships */
    public function address()
    {
        return $this->belongsTo(Address::class)->with('country');
    }

    public function programs()
    {
        $db = config('database.connections.tenant.database');
        return $this->belongsToMany(System\Program::class, $db.".branch_program")
                    ->using(BranchProgram::class);
    }

    public function schedules()
    {
        return $this->hasMany(Schedule::class);
    }

    /** Attributes */
    public function getFullAddressAttribute()
    {
        return $this->address ? $this->address->full_address : null;
    }
}
