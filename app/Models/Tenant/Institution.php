<?php

namespace App\Models\Tenant;

use App\Enums\InstitutionType;
use Konekt\Enum\Eloquent\CastsEnums;
use Illuminate\Database\Eloquent\Model;
use Tenancy\Affects\Connections\Support\Traits\OnTenant;

class Institution extends Model
{
    use OnTenant,
        CastsEnums;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'institutions';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    protected $enums = [
        'type' => InstitutionType::class
    ];

    /** Boot */
    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->slug = \Str::slug($model->name);
        });

        static::updating(function ($model) {
            $model->slug = \Str::slug($model->name);
        });
    }
}
