<?php

namespace App\Models\Tenant;

use Carbon\Carbon;
use App\Traits\EncryptModel;
use Illuminate\Database\Eloquent\Model;
use Tenancy\Affects\Connections\Support\Traits\OnTenant;

class PersonalData extends Model
{
    use OnTenant,
        EncryptModel;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'personal_data';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['address'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public function getBirthdateAttribute($value)
    {
        if ($value)
            return Carbon::parse($value)->format('d/m/Y');
    }

    public function getBirthdateYearAttribute()
    {
        if ($this->birthdate)
            return Carbon::parse(str_replace('/', '-', $this->birthdate))
                        ->format('y');
    }

    public function getAgeAttribute()
    {
        return Carbon::parse($this->attributes['birthdate'])->age;
    }

    public function getFullnameAttribute()
    {
        return ucfirst($this->last_name).' '.ucfirst($this->first_name);
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    public function academics()
    {
        return $this->hasMany(PersonalAcademicData::class);
    }

    public function log()
    {
        return $this->hasMany(PersonalLog::class);
    }

    public function getLogsAttribute()
    {
        return $this->log->toJson();
    }
}
