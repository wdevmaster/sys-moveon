<?php

namespace App\Models\Tenant;

use App\Enums;
use App\Traits\EncryptModel;
use Konekt\Enum\Eloquent\CastsEnums;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Tenancy\Affects\Connections\Support\Traits\OnTenant;

class PlanFeature extends Model
{
    use OnTenant,
        CastsEnums,
        EncryptModel;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'plan_features';

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = [];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    protected $enums = [
        'type' => Enums\FeatureType::class,
        'origin' => Enums\FeatureOrigin::class
    ];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::creating(function ($model) {
            $model->sort_order = $model->type == 'advancement';
            $model->interval_unit = $model->type == 'advancement' ?? null;
        });
    }

}
