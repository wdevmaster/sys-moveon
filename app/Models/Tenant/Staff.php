<?php

namespace App\Models\Tenant;

use App\Enums;
use Carbon\Carbon;
use Konekt\Enum\Eloquent\CastsEnums;
use Illuminate\Database\Eloquent\Model;
use Tenancy\Affects\Connections\Support\Traits\OnTenant;

class Staff extends Model
{
    use OnTenant;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'staff';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'contract_start' => 'datetime',
        'contract_end' => 'datetime',
    ];

    public function getContractStartAttribute($value)
    {
        if ($value)
            return Carbon::parse($value)->format('d/m/Y');
    }

    public function getContractEndAttribute($value)
    {
        if ($value)
            return Carbon::parse($value)->format('d/m/Y');
    }

    public function getStatusAttribute($value)
    {
        if ($value)
            return new Enums\TeacherStatus($value);
    }

    public function getTypeAttribute($value)
    {
        if ($value)
            return new Enums\TeacherType($value);
    }
}
