<?php

namespace App\Models\Tenant;

use App\Enums;
use App\Models;
use App\Traits\EncryptModel;
use Konekt\Enum\Eloquent\CastsEnums;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Tenancy\Affects\Connections\Support\Traits\OnTenant;

class Payment extends Model
{
    use OnTenant,
        CastsEnums,
        EncryptModel;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'payments';

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = [];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    protected $enums = [
        'payment_method' => Enums\PaymentMethod::class,
        'status' => Enums\PaymentStatus::class,
    ];

    /** Relations */
    public function coursable()
    {
        return $this->belongsTo(Coursable::class);
    }

    public function user()
    {
        return $this->belongsTo(Models\User::class);
    }

    public function bank()
    {
        return $this->belongsTo(Bank::class);
    }

    public function plan()
    {
        return $this->belongsTo(Plan::class);
    }

    /** Attributes */
    protected function getVoucherAttribute()
    {
        $disk = \Storage::disk(request()->is('app/*') ? 'tenant' : 'public');
        return str_replace(
            storage_path('app/'),
            '',
            $disk->path($this->voucher_path)
        );
    }
}
