<?php

namespace App\Models\Tenant;

use App\Enums;
use App\Traits\EncryptModel;
use Konekt\Enum\Eloquent\CastsEnums;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Tenancy\Affects\Connections\Support\Traits\OnTenant;

class Hall extends Model
{
    use OnTenant,
        CastsEnums,
        SoftDeletes,
        EncryptModel;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'halls';

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = [];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at ', 'updated_at' , 'deleted_at'];

    protected $enums = [
        'status' => Enums\HallStatus::class
    ];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::creating(function ($model) {
            $model->slug = \Str::slug($model->name);
        });
    }

    /** Relationships */
    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

    public function coursables()
    {
        return $this->hasMany(Coursable::class);
    }

    /** Scopes */
    public function scopeDoesntHaveCourse($query, $value)
    {
        return $query->whereDoesntHave('coursables', function(Builder $query) use ($value) {
            $query->whereHas('days', function(Builder $query) use ($value) {
                    $query->whereIn('day', $value['days']);
            })
            ->where('schedule_id', $value['schedule_id']);
        });
    }
}
