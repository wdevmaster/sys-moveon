<?php

namespace App\Models\Tenant;

use App\Enums;
use App\Traits\EncryptModel;
use Konekt\Enum\Eloquent\CastsEnums;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Tenancy\Affects\Connections\Support\Traits\OnTenant;

class Plan extends Model
{
    use OnTenant,
        CastsEnums,
        SoftDeletes,
        EncryptModel;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'plans';

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['features'];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];

    protected $enums = [
        'interval_unit' => Enums\PlanInterval::class,
    ];

    /** Relations */
    public function branches()
    {
        return $this->belongsToMany(Branch::class);
    }

    public function features()
    {
        return $this->hasMany(PlanFeature::class);
    }

    /** Attributes */
    public function getPlanAttribute()
    {
        return $this;
    }

    public function getFullnameAttribute()
    {
        return "{$this->code} {$this->name} ({$this->interval_unit})";
    }

    public function getWithInscriptionAttribute()
    {
        return (boolean) $this->features()->where('type', 'inscription')->count();
    }
}
