<?php

namespace App\Models\Tenant;

use App\Enums;
use Carbon\Carbon;
use Konekt\Enum\Eloquent\CastsEnums;
use Illuminate\Database\Eloquent\Model;
use Tenancy\Affects\Connections\Support\Traits\OnTenant;

class StudentEnrollment extends Model
{
    use OnTenant,
        CastsEnums;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'student_enrollment';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'contract_start' => 'datetime',
        'contract_end' => 'datetime',
    ];

    protected $enums = [
        'status' => Enums\StudentStatus::class,
    ];

    public function studentType()
    {
        return $this->hasOne(StudentType::class);
    }
}
