<?php

namespace App\Models\System;

use Tenancy\Tenant\Events;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Tenancy\Identification\Contracts\Tenant;
use Illuminate\Database\Eloquent\SoftDeletes;
use Symfony\Component\Console\Input\InputInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Tenancy\Identification\Concerns\AllowsTenantIdentification;
use Tenancy\Identification\Drivers\Http\Contracts\IdentifiesByHttp;
use Tenancy\Identification\Drivers\Console\Contracts\IdentifiesByConsole;

class Franchise extends Model implements Tenant, IdentifiesByHttp, IdentifiesByConsole
{
    use SoftDeletes,
        AllowsTenantIdentification;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'franchises';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at ', 'updated_at' , 'deleted_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    //protected $hidden = ['uuid'];

    /**
     * The attribute of the Model to use for the key.
     *
     * @return string
     */
    public function getTenantKeyName(): string
    {
        return 'uuid';
    }

    /**
     * The actual value of the key for the tenant Model.
     *
     * @return string|int
     */
    public function getTenantKey()
    {
        return $this->uuid;
    }

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::creating(function ($model) {
            $model->uuid = env('DB_PREFIX_TENANT').'_'.\Str::random(5);
        });
    }

    protected $dispatchesEvents = [
        'created' => Events\Created::class,
        'updated' => Events\Updated::class,
        'deleted' => Events\Deleted::class,
    ];

    /**
     * Specify whether the tenant model is matching the request.
     *
     * @param Request $request
     * @return Tenant
     */
    public function tenantIdentificationByHttp(Request $request): ?Tenant
    {
        if (!$request->is('app/*'))
            return $this;

        return $this->query()
                    ->where('code', explode('/',$request->path())[1])
                    ->first();
    }

    /**
     * Specify whether the tenant model is matching the request.
     *
     * @param Request $request
     * @return Tenant
     */
    public function tenantIdentificationByConsole(InputInterface $input): ?Tenant
    {
        if ($input->hasParameterOption('--tenant')) {
            return $this->query()
                ->where('slug', $input->getParameterOption('--tenant'))
                ->first();
        }

        return null;
    }

    /**
     * Static methods
     */
    public static function findByCode($value)
    {
        return self::where('code', $value)->first();
    }
}
