<?php

namespace App\Models\System;

use App\Traits\EncryptModel;
use Illuminate\Database\Eloquent\Model;

class ProgramLevel extends Model
{
    use EncryptModel;

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'program_levels';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /** Relationships */
    public function program()
    {
        return $this->belongsTo(Program::class);
    }
}
