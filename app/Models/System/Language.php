<?php

namespace App\Models\System;

use App\Traits\EncryptModel;
use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'languages';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];


    public function programs()
    {
        return $this->hasMany(Program::class);
    }
}
