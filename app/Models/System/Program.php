<?php

namespace App\Models\System;

use App\Models\Tenant;
use App\Traits\EncryptModel;
use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    use EncryptModel;

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'programs';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['language'];

    /** Relationships */
    public function language()
    {
        return $this->belongsTo(Language::class);
    }

    public function levels()
    {
        return $this->hasMany(ProgramLevel::class);
    }

    public function getNameAttribute($value)
    {
        return $value ?: $this->language->name;
    }
}
