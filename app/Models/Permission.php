<?php

namespace App\Models;

use App\Traits\GlobalModelConnection;
use Caffeinated\Shinobi\Models\Permission as BasePermission;

class Permission extends BasePermission
{
    use GlobalModelConnection;

    public function scopeActionView($query)
    {
        $query
            ->select('name', 'slug')
            ->where([
            [ 'slug', 'not like', '%store%' ],
            [ 'slug', 'not like', '%update%' ],
        ]);
    }
}
