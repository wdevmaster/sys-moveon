<?php

namespace App\Models;

use App\Traits\EncryptModel;
use App\Traits\GlobalModelConnection;
use Illuminate\Database\Eloquent\SoftDeletes;
use Caffeinated\Shinobi\Models\Role as BaseRole;

class Role extends BaseRole
{
    use GlobalModelConnection,
        EncryptModel,
        SoftDeletes;

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::creating(function ($model) {
            $model->slug = \Str::slug($model->name);
        });
    }

    public function scopeActionView($query)
    {
        $query->where('its_global', false);
    }

    public function scopeTeacher($query)
    {
        return $query->where('slug', 'teacher');
    }

    public function scopeStudent($query)
    {
        return $query->where('slug', 'student');
    }

    public function scopeRepresentative($query)
    {
        return $query->where('slug', 'representative');
    }
}
