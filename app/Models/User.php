<?php

namespace App\Models;

use App\Traits;
use App\Models\Tenant;
use Tenancy\Environment;
use App\Traits\GlobalModelConnection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tenancy\Affects\Connections\Support\Traits\OnTenant;
use Caffeinated\Shinobi\Concerns\HasRolesAndPermissions;

class User extends Authenticatable
{
    use Notifiable,
        SoftDeletes,
        Traits\UserProfile,
        Traits\EncryptModel,
        HasRolesAndPermissions,
        Traits\GlobalModelConnection;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = [];

    /** Delete Relationships */
    public function deleteRelationships()
    {
        //
    }

    /** Relationships */
    public function branches()
    {
        return $this->belongsToMany(Tenant\Branch::class);
    }

    public function staff()
    {
        return $this->hasOne(Tenant\Staff::class);
    }

    public function personalData()
    {
        return $this->hasOne(Tenant\PersonalData::class);
    }

    public function languages()
    {
        $db = config('database.connections.tenant.database');
        return $this->belongsToMany(System\Language::class, $db.".language_user")
                    ->using(Tenant\LanguageUser::class);
    }

    public function studentType()
    {
        return $this->belongsToMany(Tenant\StudentType::class);
    }

    public function points()
    {
        return $this->hasMany(Tenant\TeacherPoint::class);
    }

    public function enrollment()
    {
        return $this->hasOne(Tenant\StudentEnrollment::class);
    }

    public function parent()
    {
        return $this->belongsToMany(
            User::class, 'parent_children', 'children_id', 'parent_id'
        );
    }

    public function children()
    {
        return $this->belongsToMany(
            User::class, 'parent_children', 'parent_id', 'children_id'
        );
    }

    public function schedules()
    {
        return $this->hasMany(Tenant\Schedule::class);
    }

    public function coursables()
    {
        return $this->hasMany(Tenant\Coursable::class);
    }

    public function courses()
    {
        return $this->belongsToMany(Tenant\Coursable::class)->withPivot('plan_id');
    }

    /** Scopes */
    public function scopeFindRole($query, $value)
    {
        return $query->whereHas('roles', function (Builder $query) use ($value) {
            $query->where('slug', $value);
        });
    }

    public function scopeTeachers($query, $type)
    {
        return $query->whereHas('roles', function (Builder $query) {
            $query->where('slug', 'teacher');
        })->whereHas('staff', function (Builder $query) use ($type) {
            $query->where('type', $type->value());
        });
    }

    public function scopeStudents($query, $status)
    {
        return $query->whereHas('roles', function (Builder $query) {
            $query->where('slug', 'student');
        })->whereHas('enrollment', function (Builder $query) use ($status) {
            $query->where('status', $status->value());
        });
    }

    public function scopeWhereBranch($query, $branch)
    {
        return $query->whereHas('branches', function($query) use ($branch) {
            $query->where('id', $branch);
        });
    }

    public function scopeWhereStudentType($query, $type)
    {
        return $query->whereHas('studentType', function($query) use ($type) {
            $query->where('id', $type);
        });
    }

    public function scopeWhereScheduleDay($query, $days)
    {
        return $query->whereHas('schedules', function($query) use ($days) {
            $query->whereIn('day', $days);
        });
    }

    public function scopeWhereScheduleTime($query, $times)
    {
        return $query->whereHas('schedules', function($query) use ($times) {
            $query->where('start_time', $times['start_time'])
                  ->where('end_time', $times['end_time']);
        });
    }

    public function scopeWhereEnrollmentType($query, $type)
    {
        return $query->whereHas('enrollment', function($query) use ($type) {
            $query->where('student_type_id', $type);
        });
    }

    public function scopeWhereCourse($query, $value)
    {
        return $query->whereHas('courses', function($query) use ($value) {
            $query->where('coursable_id', $value);
        });
    }

    public function scopeWhereDoesntHaveCourse($query, $value)
    {
        return $query->whereDoesntHave('courses', function($query) use ($value) {
            $query->where('coursable_id', $value);
        });
    }

    public function scopeRepresentatives($query)
    {
        return $query->whereHas('roles', function (Builder $query) {
            $query->where('slug', 'representative');
        });
    }

    public function scopeFindByCi($query, $value)
    {
        return $query->whereHas('roles', function (Builder $query) {
            $query->where('slug', 'representative');
        })->whereHas('personalData', function (Builder $query) use ($value) {
            $query->where('ci', $value);
        });
    }

    public function scopeDoesntHaveCourse($query, $value)
    {
        return $query->whereDoesntHave('coursables', function(Builder $query) use ($value) {
            $query->whereHas('days', function(Builder $query) use ($value) {
                    $query->whereIn('day', $value['days']);
            })
            ->where('schedule_id', $value['schedule_id']);
        });
    }

    /** Attributes */
    public function getRoleAttribute()
    {
        return $this->roles->first();
    }

    public function getBranchAttribute()
    {
        return $this->branches->first();
    }

    public function getAddressAttribute()
    {
        return $this->personalData->address;
    }

    public function getStatusAttribute()
    {
        return $this->staff->status;
    }

    public function getTypeAttribute()
    {
        return $this->staff->type;
    }

    public function getRatingsAttribute()
    {
        if ($point = $this->points->where('type', 'general')->first())
            return $point->value;

        return '0.00';
    }

    public function getUserAttribute()
    {
        if ($this->role->slug != 'student')
            $more = [
                'contract_start' => $this->staff
                    ? $this->staff->contract_start : null,
                'contract_end' => $this->staff
                    ? $this->staff->contract_end : null,
                'ranting' => $this->points->pluck('value', 'type'),
                'student' => $this->studentType->pluck('id'),
            ];
        elseif ($this->enrollment)
            $more = ['student' => $this->enrollment->student_type_id];
        else
            $more = [];

        return (Object) array_merge(
            $this->only('username', 'email', 'id'),
            array_merge(
                [
                    'role' => $this->role->slug,
                    'branches' => $this->branches->pluck('id')->toArray(),
                    'languages'=> $this->languages->pluck('id'),
                ],
                $more
            )
        );
    }

    public function getAcademicAttribute()
    {
        if ($data = $this->personalData->academics->first())
            return (Object) array_merge(
                $data->only('dicipline', 'graduate_year'),
                [
                    'type' => $data->type->value(),
                    'title' => $data->title ? $data->title->value() : null,
                    'institution' => $data->institution->name,
                    'is_graduated' => !$data->is_graduated
                ]
            );
        else
            return null;
    }

    public function getRepresentativeAttribute()
    {
        if ($parent = $this->parent->first())
            return (Object) array_merge(
                $parent->personalData->toArray(),
                [ 'profile_photo' => $parent->profile_photo ]
            );
    }

    public function getCiStudentAttribute()
    {
        $ci = $this->personalData->ci;
        $year = $this->personalData->birthdate_year;
        $n = $this->children->count() + 1;

        return "{$n}{$year}{$ci}";
    }

    public function getProfileIsCompleteAttribute ()
    {
        $boo = true;
        $data = collect($this->personalData->toArray())->except(['profile_photo', 'address']);

        foreach ($data as $key => $val)
            if (!$val)
                $boo = false;

        return $boo;
    }
}
