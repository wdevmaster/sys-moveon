<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

use Tenancy\Hooks\Database\Events\Drivers\Configuring;
use App\Listeners\ConfigureTenantDatabase;

use Tenancy\Affects\Connections\Events\Resolving;
use App\Listeners\ResolveTenantConnection;

use Tenancy\Affects\Connections\Events\Drivers\Configuring as Affects;
use App\Listeners\ConfigureTenantConnection;

use Tenancy\Hooks\Migration\Events\ConfigureMigrations;
use App\Listeners\ConfigureTenantMigrations;

use Tenancy\Hooks\Migration\Events\ConfigureSeeds;
use App\Listeners\ConfigureTenantSeeds;

use Tenancy\Affects\Filesystems\Events\ConfigureDisk;
use App\Listeners\ConfigureTenantDisk;

class EventTenantServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Configuring::class => [
            ConfigureTenantDatabase::class,
        ],

        Resolving::class => [
            ResolveTenantConnection::class,
        ],

        Affects::class => [
            ConfigureTenantConnection::class,
        ],

        ConfigureMigrations::class => [
            ConfigureTenantMigrations::class,
        ],

        ConfigureSeeds::class => [
            ConfigureTenantSeeds::class
        ],

        ConfigureDisk::class => [
            ConfigureTenantDisk::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
