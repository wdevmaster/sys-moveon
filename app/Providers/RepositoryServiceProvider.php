<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\RoleRepositoryInterface',
            'App\Repositories\RoleRepository'
        );

        $this->app->bind(
            'App\Repositories\Tenant\BranchRepositoryInterface',
            'App\Repositories\Tenant\BranchRepository'
        );

        $this->app->bind(
            'App\Repositories\EmployeeRepositoryInterface',
            'App\Repositories\EmployeeRepository'
        );

        $this->app->bind(
            'App\Repositories\Tenant\TeacherRepositoryInterface',
            'App\Repositories\Tenant\TeacherRepository'
        );

        $this->app->bind(
            'App\Repositories\Tenant\StudentRepositoryInterface',
            'App\Repositories\Tenant\StudentRepository'
        );

        $this->app->bind(
            'App\Repositories\Tenant\RepresentativeRepositoryInterface',
            'App\Repositories\Tenant\RepresentativeRepository'
        );

        $this->app->bind(
            'App\Repositories\Tenant\HallRepositoryInterface',
            'App\Repositories\Tenant\HallRepository'
        );

        $this->app->bind(
            'App\Repositories\Tenant\ScheduleRepositoryInterface',
            'App\Repositories\Tenant\ScheduleRepository'
        );

        $this->app->bind(
            'App\Repositories\Tenant\CourseRepositoryInterface',
            'App\Repositories\Tenant\CourseRepository'
        );

        $this->app->bind(
            'App\Repositories\Tenant\PlanRepositoryInterface',
            'App\Repositories\Tenant\PlanRepository'
        );

        $this->app->bind(
            'App\Repositories\Tenant\PaymentRepositoryInterface',
            'App\Repositories\Tenant\PaymentRepository'
        );
    }
}
