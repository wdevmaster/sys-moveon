<?php

namespace App\Providers;

use App\Models\System\Franchise;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Request;
use Tenancy\Identification\Contracts\ResolvesTenants;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->resolving(ResolvesTenants::class, function (ResolvesTenants $resolver){
            $resolver->addModel(Franchise::class);
            return $resolver;
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        if (env('APP_URL_SSL', false)) URL::forceScheme('https');

        if (Request::is('app/*')) {
            $code = explode('/', Request::path())[1];

            View::share('code', $code);
            View::share('franchise', Franchise::findByCode($code));
        }

        View::share('type', Request::is('app/*') ? 'tenant.' : 'system.');


        //dd(config('tenancy'));
    }
}
