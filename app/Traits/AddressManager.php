<?php

namespace App\Traits;

use App\Models\Tenant\City;
use App\Models\Tenant\Address;

trait AddressManager
{
    public function createAddress($data)
    {
        $this->syncCity($data);
        return Address::firstOrCreate($data);
    }

    public function updateAddress(array $data, $model)
    {
        $this->syncCity($data);

        if ($model){
            $model->update($data);
            return $model;
        }

        return Address::firstOrCreate($data);
    }

    public function syncCity($data)
    {
        $data = collect($data);
        $name = $data->get('city_str');

        return City::firstOrCreate(
            ['code' => \Str::slug($name)],
            $data->only('country_str')
                    ->merge([ 'name' => $name ])
                    ->toArray()
        );
    }
}

