<?php

namespace App\Traits;

use Carbon\Carbon;

trait UserManager
{
    public function createUser($data, $model)
    {
        $row = $this->setUpData(
            $data->only('username', 'email', 'password', 'role')
        );

        $model = $model->create($row->toArray());

        $model->branches()->attach($data->get('branches'));

        if ($data->get('role') != 'student' && $data->get('role') != 'representative')
            $model->staff()->create(
                $this->formatAttach($data)
            );
        elseif($data->get('role') == 'student')
            $model->enrollment()->create(array_merge(
                $data->only('status')->toArray(),
                ['student_type_id' => (int) $data->get('student')[0]]
            ));

        $model->assignRoles($data->get('role'));

        $this->attachFilesUser($data->get('files'), $model);

        return $model;
    }

    public function updateUser($data, $model)
    {
        $row = $this->setUpData(
            $data->only('username', 'email', 'password', 'role', 'origin')
        );

        $model->update($row->toArray());

        $model->branches()->sync($data->get('branches'));

        if ($data->get('role') != 'student' && $data->get('role') != 'representative')
            $model->staff()->update(
                $this->formatAttach($data)
            );
        elseif($data->get('role') == 'student') {
            $enrollment = array_merge(
                $data->only('status')->toArray(),
                [
                    'student_type_id' =>  $data->get('student')
                        ? (int) $data->get('student')[0]
                        : $model->enrollment->student_type_id
                ]
            );

            $model->enrollment()->update(
                $enrollment
            );
        }


        $model->syncRoles($data->get('role'));

        $this->attachFilesUser($data->get('files'), $model);

        return $model;
    }

    public function createPersonal($data, $model)
    {
        $address = $this->createAddress($data->get('address'));
        $personalData = $data->get('personalData');

        $data = array_merge(
            $personalData,
            [
                'address_id' => $address->id,
                'birthdate' => $this->formatDate(
                    $personalData['birthdate']
                )
            ]
        );

        return $model->personalData()->create($data);
    }

    public function updatePersonal($data, $model)
    {
        $personalData = $data->get('personalData');
        $address = $this->updateAddress(
            $data->get('address'),
            $model->address
        );

        $model->update(array_merge(
            $personalData,
            [
                'address_id' => $address->id,
                'birthdate' => $this->formatDate(
                    $personalData['birthdate']
                )
            ]
        ));

        return $model;
    }

    public function formatAttach($data)
    {
        return $data->only('status', 'type')
            ->merge(
                $this->formatContract($data)
            )->toArray();
    }

    private function formatContract($data)
    {
        return $data->only('contract_start', 'contract_end')
                    ->map(function ($row) {
                        return $row
                            ? $this->formatDate($row)
                            : null;
                    });
    }

    public function updateOrCreateLog($data, $model)
    {
        $data = json_decode($data, true);
        $model->log()->delete();

        if ($data)
            foreach ($data as $log)
                $model->log()->create($log);
    }

    public function setUpData($data)
    {
        $data = collect($data);

        if (!$data->get('username') && $data->get('role') != 'student')
            $data['username'] = explode('@', $data->get('email'))[0];
        elseif (!$data->get('username'))
            $data['username'] = \Str::random(5);

        if ($data->has('password'))
            $data['password'] = bcrypt($data->get('password'));

        return $data;
    }

    public function getTypeStuden($age)
    {
        dd($age);
    }
}
