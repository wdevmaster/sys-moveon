<?php

namespace App\Traits;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;

trait ModelRepository
{
    use AddressManager,
        UserManager,
        AcademicManager,
        FileUserManager;

    public function decodeId($id)
    {
        $id = $id;
        return is_array($id) ? (isset($id[0]) ? $id[0] : null ): $id;
    }

    public function find($id)
    {
        if (!$id || null == $model = $this->model->find($this->decodeId($id))) {
            throw new ModelNotFoundException(
                __(
                    'The :name was not found',
                    [ 'name' => __(\Str::afterLast(get_class($this->model), '\\')) ]
                )
            );
        }

        return $model;
    }

    public function delete($id)
    {
        $model = $this->find($id);
        $model->update(['is_active' => false]);

        if ($model->trashed()) {
            $model->deleteRelationships();
            return $model->forceDelete();
        }

        return $model->delete();
    }

    public function restore($id)
    {
        $model = $this->find($id);
        $model->update(['is_active' => true]);
        return $model->restore();
    }

    public function formatDate($value)
    {
        if ($value)
            return Carbon::parse(str_replace('/', '-', $value));

        return null;
    }
}
