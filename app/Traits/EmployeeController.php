<?php

namespace App\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Inspector\Laravel\Facades\Inspector;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Repositories\EmployeeRepositoryInterface as EmployeeRepository;

trait EmployeeController
{
    /** @var EmployeeRepositoryInterface */
    private $repository;

    public function __construct() {
        $this->middleware('auth');
        $this->repository = app(EmployeeRepository::class);
        \View::share('pageConfigs', [
            'pageHeader' => false
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($code, $role)
    {
        $rows = $this->repository->all($role);
        return view('app.employee.index', compact('rows', 'role'))->with(
            $this->repository::with()
        );;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($code, $role)
    {
        return view('app.employee.create', compact('role'))->with(
            $this->repository::with()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::connection('tenant')->beginTransaction();

            $this->repository->create($request->except('_token'));
            session(['menu' => null]);

            DB::connection('tenant')->commit();
            return redirect(tenant_route('employees.index', ['role' => $request->get('role')]))
                    ->with('success', __(':name has been created', [
                        'name' => ucfirst(__('Employee') )
                    ]));
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            Inspector::reportException($e);
            Log::debug($e->getMessage());
            return redirect()->back()->withInput()
                    ->with('error', __('Error: :msg', ['msg' => $e->getMessage()]));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($code, $role, $id)
    {
        try {
            $data = $this->repository->find($id);

            return view('app.employee.edit', compact('data', 'role'))->with(
                $this->repository::with()
            );
        } catch (\Exception $e) {
            return redirect(tenant_route('employees.index', ['role' => $role]))
                    ->with('error', __('Error: :msg', ['msg' => $e->getMessage()]));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($code, $role, $id)
    {
        try {
            $data = $this->repository->find($id);

            return view('app.employee.edit', compact('data', 'role'))->with(
                $this->repository::with()
            );
        } catch (\Exception $e) {
            return redirect(tenant_route('employees.index', ['role' => $role]))
                    ->with('error', __('Error: :msg', ['msg' => $e->getMessage()]));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $code, $id)
    {
        try {
            DB::connection('tenant')->beginTransaction();

            $this->repository->update(
                $request->except(['_token', '_method']),
                $id
            );
            session(['menu' => null]);

            DB::connection('tenant')->commit();
            return redirect(tenant_route('employees.index', ['role' => $request->get('role')]))
                    ->with('success', __(':name has been updated', [
                        'name' => ucfirst(__('Employee') )
                    ]));
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            Inspector::reportException($e);
            Log::debug($e->getMessage());
            return redirect()->back()->withInput()
                    ->with('error', __('Error: :msg', ['msg' => $e->getMessage()]));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($code, $id)
    {
        try {
            DB::connection('tenant')->beginTransaction();
                $this->repository->delete($id);
            DB::connection('tenant')->commit();
        } catch (ModelNotFoundException $e) {
            DB::connection('tenant')->rollBack();
            return response()->json(['error' => $e->getMessage()], 404);
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            Inspector::reportException($e);
            Log::debug($e->getMessage());
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }

    public function restore($code, $id)
    {
        try {
            DB::connection('tenant')->beginTransaction();
                $this->repository->restore($id);
            DB::connection('tenant')->commit();
        } catch (ModelNotFoundException $e) {
            DB::connection('tenant')->rollBack();
            return response()->json(['error' => $e->getMessage()], 404);
        } catch (\Exception $e) {
            DB::connection('tenant')->rollBack();
            Inspector::reportException($e);
            Log::debug($e->getMessage());
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }
}
