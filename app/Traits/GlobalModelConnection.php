<?php

namespace App\Traits;

use Tenancy\Environment;

trait GlobalModelConnection
{
    public function getConnectionName()
    {
        if (!request()->is('admin/**') && !request()->server('argv', null))
            return Environment::getTenantConnectionName();

        return $this->connection;
    }
}
