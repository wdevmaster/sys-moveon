<?php

namespace App\Traits;

trait EncryptModel
{
    /**
     * @return string
     */
    public function getHIdAttribute()
    {
        return $this->id;
    }
}
