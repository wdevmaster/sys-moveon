<?php

namespace App\Traits;

use App\Models\Tenant\Address;
use App\Models\Tenant\Institution;

trait AcademicManager
{
    public function updateOrCreateAcademic($model, $data)
    {
        $instt = Institution::firstOrCreate(
            ['slug' => \Str::slug($data->get('institution'))],
            [
                'name' => $data->get('institution'),
                'type' => $data->get('type')
            ]
        );
        //dd($data);
        return $model->academics()->updateOrCreate(
            [ 'type' => $data->get('type') ],
            array_merge(
                $data->only('title', 'dicipline')->toArray(),
                [
                    'institution_id' => $instt->id,
                    'graduate_year' => $this->formatDate(
                        $data->get('graduate_year')
                    )
                ]
            )
        );
    }
}

