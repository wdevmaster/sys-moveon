<?php

namespace App\Traits;

trait FileUserManager
{
    /** Upload files form */
    private function updaloadFiles($files, $model)
    {
        $array = collect([]);
        $disk = request()->is('app/*') ? 'tenant' : 'public';
        $dir = base64_encode($model->id);

        if ($files->isNotEmpty())
            foreach($files as $type => $file) {
                $fileName = "$type-$model->id.{$file->getClientOriginalExtension()}";

                if ($path = \Storage::disk($disk)->putFileAs($dir, $file, $fileName)) {
                    $array->push([
                        'type' => $type,
                        'name' => $file->getClientOriginalName(),
                        'path' => $path
                    ]);
                }
            }

        return $array->isNotEmpty() ? $array : null;
    }

    public function attachFilesUser($files, $model)
    {
        if ($files = $this->updaloadFiles(collect($files), $model)) {
            if ($avatar = $files->where('type', 'avatar')->first())
                $model->personalData()->update([
                    'profile_photo' => $avatar['path']
                ]);
        }
    }
 }
