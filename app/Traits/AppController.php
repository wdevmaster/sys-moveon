<?php

namespace App\Traits;

trait AppController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        \View::share('pageConfigs', [
            'pageHeader' => false
        ]);
    }

    public function __invoke()
    {
        return view('app.dashboard');
    }
}
