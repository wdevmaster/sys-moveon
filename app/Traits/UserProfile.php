<?php

namespace App\Traits;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

trait UserProfile
{
    /**
     * Scopes
     */
    public function scopeNotAuth($query)
    {
        return $query->where('id', '!=', \Auth::id());
    }

    public function getFullnameAttribute()
    {
        return $this->personalData
            ? $this->personalData->fullname
            : $this->username;
    }

    public function getProfilePhotoAttribute()
    {
        return $this->checkProfilePhotoUrl();
    }

    protected function getAvatarAttribute()
    {
        $disk = \Storage::disk(request()->is('app/*') ? 'tenant' : 'public');
        return $this->personalData
            ? (
                $this->personalData->profile_photo
                    ? str_replace(
                        storage_path('app/'),
                        '',
                        $disk->path($this->personalData->profile_photo)
                    )
                    : null
            ) : null;
    }

    protected function checkProfilePhotoUrl()
    {
        return $this->avatar
            ? asset($this->avatar)
            : $this->defaultProfilePhotoUrl();
    }

    /**
     * Get the default profile photo URL if no profile photo has been uploaded.
     *
     * @return string
     */
    protected function defaultProfilePhotoUrl()
    {
        return 'https://ui-avatars.com/api/?name='.urlencode($this->fullname).'&color=28C76F&background=BEEED3';
    }
}
