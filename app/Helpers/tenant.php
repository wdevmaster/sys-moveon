<?php

if (! function_exists('tenant_route')) {
    function tenant_route($name, $parameters = [], $absolute = true)
    {
        $type = request()->is('app/*') ? 'tenant.' : 'system.';
        if (request()->is('app/*')) {
            if (!is_array($parameters)) {
                $key = \Str::singular(explode('.', $name)[0]);
                $value = $parameters;
                $parameters = [];
                $parameters[$key] = $value;
            }

            $parameters = array_merge(
                [ 'code' => explode('/', request()->path())[1] ],
                $parameters
            );
        }

        return app('url')->route("{$type}{$name}", $parameters, $absolute);
    }
}
