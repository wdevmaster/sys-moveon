<?php // Code within app\Helpers\Helper.php

namespace App\Helpers;

use Config;
use App\Enums;
use App\Models\Role;
use Illuminate\Support\Facades\Auth;
class Helper
{
    public static function applClasses()
    {
        // default data array
        $DefaultData = [
            'mainLayoutType' => 'vertical',
            'theme' => 'light',
            'sidebarCollapsed' => false,
            'navbarColor' => '',
            'horizontalMenuType' => 'floating',
            'verticalMenuNavbarType' => 'floating',
            'footerType' => 'static', //footer
            'bodyClass' => '',
            'navbarClass' => config('custom.custom.navbarClass'),
            'pageHeader' => true,
            'contentLayout' => 'default',
            'blankPage' => false,
            'direction' => env('MIX_CONTENT_DIRECTION', 'ltr'),
        ];

        // if any key missing of array from custom.php file it will be merge and set a default value from dataDefault array and store in data variable
        $data = array_merge($DefaultData, config('custom.custom'));

        // All options available in the template
        $allOptions = [
            'mainLayoutType' => array('vertical', 'horizontal'),
            'theme' => array('light' => 'light', 'dark' => 'dark-layout', 'semi-dark' => 'semi-dark-layout'),
            'sidebarCollapsed' => array(true, false),
            'navbarColor' => array('bg-primary', 'bg-info', 'bg-warning', 'bg-success', 'bg-danger', 'bg-dark'),
            'horizontalMenuType' => array('floating' => 'navbar-floating', 'static' => 'navbar-static', 'sticky' => 'navbar-sticky'),
            'horizontalMenuClass' => array('static' => 'menu-static', 'sticky' => 'fixed-top', 'floating' => 'floating-nav'),
            'verticalMenuNavbarType' => array('floating' => 'navbar-floating', 'static' => 'navbar-static', 'sticky' => 'navbar-sticky', 'hidden' => 'navbar-hidden'),
            'navbarClass' => array('floating' => 'floating-nav', 'static' => 'static-top', 'sticky' => 'fixed-top', 'hidden' => 'd-none'),
            'footerType' => array('static' => 'footer-static', 'sticky' => 'fixed-footer', 'hidden' => 'footer-hidden'),
            'pageHeader' => array(true, false),
            'contentLayout' => array('default', 'content-left-sidebar', 'content-right-sidebar', 'content-detached-left-sidebar', 'content-detached-right-sidebar'),
            'blankPage' => array(false, true),
            'sidebarPositionClass' => array('content-left-sidebar' => 'sidebar-left', 'content-right-sidebar' => 'sidebar-right', 'content-detached-left-sidebar' => 'sidebar-detached sidebar-left', 'content-detached-right-sidebar' => 'sidebar-detached sidebar-right', 'default' => 'default-sidebar-position'),
            'contentsidebarClass' => array('content-left-sidebar' => 'content-right', 'content-right-sidebar' => 'content-left', 'content-detached-left-sidebar' => 'content-detached content-right', 'content-detached-right-sidebar' => 'content-detached content-left', 'default' => 'default-sidebar'),
            'direction' => array('ltr', 'rtl'),
        ];

        //if mainLayoutType value empty or not match with default options in custom.php config file then set a default value
        foreach ($allOptions as $key => $value) {
            if (array_key_exists($key, $DefaultData)) {
                if (gettype($DefaultData[$key]) === gettype($data[$key])) {
                    // data key should be string
                    if (is_string($data[$key])) {
                        // data key should not be empty
                        if (isset($data[$key]) && $data[$key] !== null) {
                            // data key should not be exist inside allOptions array's sub array
                            if (!array_key_exists($data[$key], $value)) {
                                // ensure that passed value should be match with any of allOptions array value
                                $result = array_search($data[$key], $value, 'strict');
                                if (empty($result) && $result !== 0) {
                                    $data[$key] = $DefaultData[$key];
                                }
                            }
                        } else {
                            // if data key not set or
                            $data[$key] = $DefaultData[$key];
                        }
                    }
                } else {
                    $data[$key] = $DefaultData[$key];
                }
            }
        }

        //layout classes
        $layoutClasses = [
            'theme' => $data['theme'],
            'layoutTheme' => $allOptions['theme'][$data['theme']],
            'sidebarCollapsed' => $data['sidebarCollapsed'],
            'verticalMenuNavbarType' => $allOptions['verticalMenuNavbarType'][$data['verticalMenuNavbarType']],
            'navbarClass' => $allOptions['navbarClass'][$data['navbarClass']],
            'navbarColor' => $data['navbarColor'],
            'horizontalMenuType' => $allOptions['horizontalMenuType'][$data['horizontalMenuType']],
            'horizontalMenuClass' => $allOptions['horizontalMenuClass'][$data['horizontalMenuType']],
            'footerType' => $allOptions['footerType'][$data['footerType']],
            'sidebarClass' => 'menu-expanded',
            'bodyClass' => $data['bodyClass'],
            'pageHeader' => $data['pageHeader'],
            'blankPage' => $data['blankPage'],
            'blankPageClass' => '',
            'contentLayout' => $data['contentLayout'],
            'sidebarPositionClass' => $allOptions['sidebarPositionClass'][$data['contentLayout']],
            'contentsidebarClass' => $allOptions['contentsidebarClass'][$data['contentLayout']],
            'mainLayoutType' => $data['mainLayoutType'],
            'direction' => $data['direction'],
        ];

        // sidebar Collapsed
        if ($layoutClasses['sidebarCollapsed'] == 'true') {
            $layoutClasses['sidebarClass'] = "menu-collapsed";
        }

        // blank page class
        if ($layoutClasses['blankPage'] == 'true') {
            $layoutClasses['blankPageClass'] = "blank-page";
        }

        return $layoutClasses;
    }

    public static function updatePageConfig($pageConfigs)
    {
        $demo = 'custom';
        if (isset($pageConfigs)) {
            if (count($pageConfigs) > 0) {
                foreach ($pageConfigs as $config => $val) {
                    Config::set('custom.' . $demo . '.' . $config, $val);
                }
            }
        }
    }

    public static function getMenu()
    {
        $role = Auth::user()->role;
        $roles = ['teacher', 'student', 'representative'];

        $menuArry = collect(require_once(
            base_path('resources/views/elements/menu.php')
        ));

        $key = request()->is('admin*') ? 'system' : 'tenant';

        if ($key == 'tenant' && in_array($role->slug, $roles))
            $key = 'main';

        if (!session('menu')) {
            $menu = self::formatCollect(
                $menuArry->get($key)['options']
            );

            $menu = self::replaceCode($menu);

            if ($key != 'main') {
                $menu = self::setUpEmployee($menu);
                $menu = self::setUpBranch($menu);
                $menu = self::setMenuTeacher($menu);
                $menu = self::setMenuStudent($menu);
            }

            $menu = json_decode(
                collect(['options' => $menu])->toJson()
            );

            if (env('APP_ENV') == 'production')
                session(['menu' => $menu]);
        } else
            $menu = session('menu');

        return $menu;
    }

    private static function formatCollect($array)
    {
        return collect($array)->map(function($row) {
            $row = collect($row);
            if ($submenu = $row->get('submenu'))
                $row['submenu'] = self::formatCollect($submenu);

            return $row;
        });
    }

    private static function replaceCode($array)
    {
        if (request()->is('app/*')) {
            $code = explode('/',request()->path())[1];
            return $array->map(function($row) use ($code) {
                if ($url = $row->get('url'))
                    $row['url'] = str_replace(':code:', $code, $url);

                if ($submenu = $row->get('submenu'))
                    $row['submenu'] = self::replaceCode($submenu);

                return $row;
            });
        }

        return $array;
    }

    public static function setUpEmployee($array)
    {
        $roles = Role::actionView()->get();

        return  $array->map(function ($row) use ($roles) {
            if ($row->get('slug') != 'employees')
                return $row;
            elseif ($roles->isNotEmpty()) {

                foreach ($roles as $role) {
                    $url = tenant_route('employees.index', ['role' => $role->slug]);
                    $row['submenu'] = [
                        [
                            'url' =>  str_replace(url('/').'/', '', $url),
                            'name' => $role->name,
                            'icon' => 'feather icon-circle',
                        ]
                    ];
                }

                return $row;
            }
        });
    }

    private static function setUpBranch($array)
    {
        if (request()->is('app/*')) {
            $code = explode('/',request()->path())[1];
            $franchise = \App\Models\System\Franchise::findByCode($code);

            return  $array->map(function ($row) use ($franchise) {
                $row = collect($row);
                if ($row->get('slug') != 'general')
                    return $row;
                else {
                    $row = $row->merge([
                        'submenu' => $row->get('submenu')->map(function($sub) use ($franchise) {
                            if ($sub->get('slug') == 'branches') {

                                if (!\Auth::user()->role->special) {
                                    $branch = \Auth::user()->branch;
                                    $url = tenant_route('branches.show', $branch->hid);
                                    $sub['name'] = __('Branch');
                                } elseif (!$franchise->is_multibranch) {
                                    $sub['name'] = __('Branch');
                                    $url = tenant_route(
                                        'branches.edit',
                                        \App\Models\Tenant\Branch::first()->hid
                                    );
                                } else {
                                    $url = $sub['url'];
                                }

                                $sub['url'] = str_replace(url('/').'/', '', $url);
                            }

                            return $sub;
                        })
                    ]);

                    return $row;
                }
            });
        }

        return $array;
    }

    public static function setMenuTeacher($menu)
    {
        return $menu->map(function ($row){
            if (isset($row['slug']) && $row['slug'] == 'teachers') {
                $submenu = [];
                foreach (Enums\TeacherType::choices() as $type => $label) {
                    $submenu[] = [
                        'url' => str_replace(
                            url('/').'/',
                            '',
                            tenant_route('teachers.index', ['type' => $type])
                        ),
                        'name' => $label,
                        'icon' => 'feather icon-circle'
                    ];
                }
                $row['submenu'] = $submenu;
            }
            return $row;
        });
    }

    public static function setMenuStudent($menu)
    {
        return $menu->map(function ($row){
            if (isset($row['slug']) && $row['slug'] == 'students') {
                $submenu = [];
                foreach (Enums\StudentStatus::choices() as $status => $label) {
                    $submenu[] = [
                        'url' => str_replace(
                            url('/').'/',
                            '',
                            tenant_route('students.index', ['status' => $status])
                        ),
                        'name' => $label,
                        'icon' => 'feather icon-circle'
                    ];
                }
                $row['submenu'] = $submenu;
            }
            return $row;
        });
    }
}
