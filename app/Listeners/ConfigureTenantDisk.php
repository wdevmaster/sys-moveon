<?php

namespace App\Listeners;

use Tenancy\Affects\Filesystems\Events\ConfigureDisk;

class ConfigureTenantDisk
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(ConfigureDisk $event)
    {
        if($tenant = $event->event->tenant) {
            $event->config = [
                'driver' => 'local',
                'root' => storage_path('app/tenant/' . base64_encode($tenant->getTenantKey())),
            ];
        }
    }
}
