<?php

namespace App\Listeners;

use Tenancy\Hooks\Migration\Events\ConfigureSeeds;

class ConfigureTenantSeeds
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(ConfigureSeeds $event)
    {
        $event->seed(\TenantRoleSeeder::class);
        $event->seed(\TenantPermissionSeeder::class);
        $event->seed(\StudentTypeTableSeeder::class);
    }
}
