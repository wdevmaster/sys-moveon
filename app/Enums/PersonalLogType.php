<?php

namespace App\Enums;

use Konekt\Enum\Enum;

class PersonalLogType extends Enum
{
    const __DEFAULT = self::LOG;

    const LOG       = 'log';
    const RECOMM    = 'recomm';
    const OBSERV    = 'observ';
    const CONDIT    = 'condit';
    const ALLERG    = 'allerg';


    protected static $labels = [];

    protected static function boot()
    {
        static::$labels = [
            self::LOG       => __('Log'),
            self::RECOMM    => __('Recommendation'),
            self::OBSERV    => __('Observation'),
            self::CONDIT    => __('Condition'),
            self::ALLERG    => __('Allergy'),

        ];
    }
}
