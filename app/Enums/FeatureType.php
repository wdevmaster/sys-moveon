<?php

namespace App\Enums;

use Konekt\Enum\Enum;

class FeatureType extends Enum
{
    const __DEFAULT = self::INSCRIPTION;

    const INSCRIPTION = 'inscription';
    const DISCOUNT = 'discount';
    const ADVANCEMENT = 'advancement';

    protected static $labels = [];

    protected static function boot()
    {
        static::$labels = [
            self::INSCRIPTION => __('Inscription'),
            self::DISCOUNT => __('Discount'),
            self::ADVANCEMENT => __('Advancement'),
        ];
    }
}
