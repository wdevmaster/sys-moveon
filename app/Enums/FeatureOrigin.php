<?php

namespace App\Enums;

use Konekt\Enum\Enum;

class FeatureOrigin extends Enum
{
    const DUES = 'dues';
    const BASE = 'base';

    protected static $labels = [];

    protected static function boot()
    {
        static::$labels = [
            self::DUES => __('Dues'),
            self::BASE => __('Base'),
        ];
    }
}
