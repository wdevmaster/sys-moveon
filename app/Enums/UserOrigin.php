<?php

namespace App\Enums;

use Konekt\Enum\Enum;

class UserOrigin extends Enum
{
    const __DEFAULT = self::WEB;

    const WEB       = 'web';
    const FACEBOOK  = 'facebook';
    const GOOGLE    = 'google';
    const INSTAGRAM = 'instagram';
    const TWITTER   = 'twitter';
    const FRIEND    = 'friend';

    protected static $labels = [];

    protected static function boot()
    {
        static::$labels = [
            self::WEB       => 'Web',
            self::FACEBOOK  => 'Facebook',
            self::GOOGLE    => 'Google',
            self::INSTAGRAM => 'Instagram',
            self::TWITTER   => 'Twitter',
            self::FRIEND    => 'Friend'
        ];
    }
}
