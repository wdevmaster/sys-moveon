<?php

namespace App\Enums;

use Konekt\Enum\Enum;

class TeacherPointType extends Enum
{
    const __DEFAULT = self::GENERAL;

    const GENERAL   = 'general';
    const EXAM              = 'exam';
    const MINICLASS         = 'mini_class';
    const INTERVIEW         = 'interview';
    const JOURNEYS          = 'journeys';

    protected static $labels = [];

    protected static function boot()
    {
        static::$labels = [
            self::GENERAL   => __('General'),
            self::EXAM          => __('Exam'),
            self::MINICLASS     => __('Mini class'),
            self::INTERVIEW     => __('Interview'),
            self::JOURNEYS      => __('Journeys'),
        ];
    }
}

