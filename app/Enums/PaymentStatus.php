<?php

namespace App\Enums;

use Konekt\Enum\Enum;

class PaymentStatus extends Enum
{
    const __DEFAULT = self::ONHOLD;

    const ONHOLD        = 'on_hold ';
    const INPROCESS     = 'in_process';
    const REJECTED      = 'rejected';
    const APPROVED      = 'approved';

    protected static $labels = [];

    protected static function boot()
    {
        static::$labels = [
            self::ONHOLD        => __('On hold'),
            self::INPROCESS     => __('In process'),
            self::REJECTED      => __('Rejected'),
            self::APPROVED      => __('Approved')
        ];
    }
}
