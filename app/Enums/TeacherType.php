<?php

namespace App\Enums;

use Konekt\Enum\Enum;

class TeacherType extends Enum
{
    const __DEFAULT = self::POSTULANT;

    const ACTIVE    = 'active';
    const POSTULANT = 'postulant';
    const RETIRED   = 'retired';

    protected static $labels = [];

    protected static function boot()
    {
        static::$labels = [
            self::ACTIVE    => __('Active'),
            self::POSTULANT => __('Postulant'),
            self::RETIRED   => __('Retired')
        ];
    }
}
