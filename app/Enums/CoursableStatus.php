<?php

namespace App\Enums;

use Konekt\Enum\Enum;

class CoursableStatus extends Enum
{
    const __DEFAULT = self::WAITI;

    const WAITI = 'waiting';
    const INREG = 'registration';
    const INPRO = 'procress';
    const FINIS = 'finished';

    protected static $labels = [];

    protected static function boot()
    {
        static::$labels = [
            self::WAITI => __('Waiting for the plan '),
            self::INREG => __('In registrations'),
            self::INPRO => __('In process'),
            self::FINIS => __('Finished'),
        ];
    }
}
