<?php

namespace App\Enums;

use Konekt\Enum\Enum;

class PlanInterval extends Enum
{
    const __DEFAULT = self::MONTH;

    const WEEK  = 'week';
    const MONTH = 'month';

    protected static $labels = [];

    protected static function boot()
    {
        static::$labels = [
            self::WEEK  => __('Week'),
            self::MONTH => __('Month'),
        ];
    }
}
