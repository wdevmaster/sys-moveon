<?php

namespace App\Enums;

use Konekt\Enum\Enum;

class PaymentMethod extends Enum
{
    const __DEFAULT = self::CASH;

    const CASH          = 'cash';
    const ZELLE         = 'zelle';
    const WIRETRANSFER  = 'wire_transfer';
    const APPTRANSFER   = 'app_transfer';

    protected static $labels = [];

    protected static function boot()
    {
        static::$labels = [
            self::CASH          => __('Cash'),
            self::ZELLE         => __('Zelle'),
            self::WIRETRANSFER  => __('Wire Transfer'),
            self::APPTRANSFER   => __('App Transfer')
        ];
    }
}
