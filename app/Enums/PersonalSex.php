<?php

namespace App\Enums;

use Konekt\Enum\Enum;

class PersonalSex extends Enum
{
    const MALE      = 'male';
    const FEMALE    = 'female';

    protected static $labels = [];

    protected static function boot()
    {
        static::$labels = [
            self::MALE      => __('Male'),
            self::FEMALE    => __('Female'),
        ];
    }
}
