<?php

namespace App\Enums;

use Konekt\Enum\Enum;

class AcademicTitle extends Enum
{
    const BACHELOR                      = 'bachelor';
    const ACADEMIC                      = 'academic';
    const BACHELOR_LANGUAGES            = 'bachelor_languages';
    const BACHELOR_LANGUAGES_EDUCATION  = 'bachelor_languages_education';
    const GRADUATES_OTHER_AREAS         = 'graduates_other_areas';
    const POSTGRADUATE_LANGUAGES        = 'postgraduate_languages';
    const POSTGRADUATE_OTHER_AREAS      = 'postgraduate_other_areas';
    const DOCTORATE_LANGUAGES           = 'doctorate_languages';
    const DOCTORATE_OTHER_AREAS         = 'doctorate_other_areas';

    protected static $labels = [];

    protected static function boot()
    {
        static::$labels = [
            self::BACHELOR                      => __('Bachelor'),
            self::ACADEMIC                      => __('Academic'),
            self::BACHELOR_LANGUAGES            => __('Bachelor in modern languages'),
            self::BACHELOR_LANGUAGES_EDUCATION  => __('Bachelor of modern languages education'),
            self::GRADUATES_OTHER_AREAS         => __('Graduates in other areas'),
            self::POSTGRADUATE_LANGUAGES        => __('Postgraduate in languages'),
            self::POSTGRADUATE_OTHER_AREAS      => __('Postgraduate courses in other areas'),
            self::DOCTORATE_LANGUAGES           => __('Doctorate in languages'),
            self::DOCTORATE_OTHER_AREAS         => __('Doctorate in other areas'),
        ];
    }
}
