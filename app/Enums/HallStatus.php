<?php

namespace App\Enums;

use Konekt\Enum\Enum;

class HallStatus extends Enum
{
    const __DEFAULT = self::AVAILABLE;

    const AVAILABLE = 'available';
    const DISABLE   = 'disable';

    protected static $labels = [];

    protected static function boot()
    {
        static::$labels = [
            self::AVAILABLE => __('Available'),
            self::DISABLE   => __('Disable'),
        ];
    }
}
