<?php

namespace App\Enums;

use Konekt\Enum\Enum;

class TeacherStatus extends Enum
{
    const __DEFAULT = self::ONHOLD;

    const ONHOLD            = 'on_hold';
    const EXAM              = 'exam';
    const MINICLASS         = 'mini_class';
    const INTERVIEW         = 'interview';
    const JOURNEYS          = 'journeys';
    const REJECTED          = 'rejected';
    const APPROVED          = 'approved';

    protected static $labels = [];

    protected static function boot()
    {
        static::$labels = [
            self::ONHOLD        => __('On hold'),
            self::EXAM          => __('Exam'),
            self::MINICLASS     => __('Mini class'),
            self::INTERVIEW     => __('Interview'),
            self::JOURNEYS      => __('Journeys'),
            self::REJECTED      => __('Rejected'),
            self::APPROVED      => __('Approved'),
        ];
    }
}
