<?php

namespace App\Enums;

use Konekt\Enum\Enum;

class ScheduleType extends Enum
{
    const __DEFAULT = self::LAP;

    const LAP   = 'lapse';
    const TEA   = 'teacher';

    protected static $labels = [];

    protected static function boot()
    {
        static::$labels = [
            self::LAP   => __('Lapse'),
            self::TEA   => __('Teacher'),
        ];
    }
}
