<?php

namespace App\Enums;

use Konekt\Enum\Enum;

class PersonalLogGroup extends Enum
{
    const __DEFAULT = self::LOG;

    const LOG       = 'log';
    const RECOMM    = 'recomm';
    const HEALTH    = 'health';

    protected static $labels = [];

    protected static function boot()
    {
        static::$labels = [
            self::LOG       => __('log'),
            self::RECOMM    => __('recomm'),
            self::HEALTH    => __('health')
        ];
    }
}
