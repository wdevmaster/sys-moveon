<?php

namespace App\Enums;

use Konekt\Enum\Enum;

class StudentStatus extends Enum
{
    const __DEFAULT = self::ACTIVE;

    const ACTIVE    = 'active';
    const PREENR    = 'preenrolled';
    const GRADUAT   = 'graduate';
    const RETIRED   = 'retired';

    protected static $labels = [];

    protected static function boot()
    {
        static::$labels = [
            self::ACTIVE    => __('Active'),
            self::PREENR    => __('Pre-enrolled'),
            self::GRADUAT   => __('Graduate'),
            self::RETIRED   => __('Retired')
        ];
    }
}
