<?php

namespace App\Enums;

use Konekt\Enum\Enum;

class InstitutionType extends Enum
{
    const SCHOOL        = 'school';
    const HIGHSCHOOL    = 'highschool';
    const UNIVERSITY    = 'university';

    protected static $labels = [];

    protected static function boot()
    {
        static::$labels = [
            self::SCHOOL        => __('School'),
            self::HIGHSCHOOL    => __('High school'),
            self::UNIVERSITY    => __('University'),
        ];
    }
}
