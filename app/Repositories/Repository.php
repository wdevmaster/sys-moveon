<?php

namespace App\Repositories;

class Repository
{
    protected $code;

    public function __construct() {
        if (request()->is('app/*'))
            $this->code = explode('/',request()->path())[1];
        else
            $this->code = null;
    }
}
