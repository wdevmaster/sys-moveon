<?php

namespace App\Repositories;

use App\Repositories\RepositoryInterface;

interface EmployeeRepositoryInterface extends RepositoryInterface
{
    public function all($role);

    public function manager(array $data);
}
