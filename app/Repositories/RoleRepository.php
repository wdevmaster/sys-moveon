<?php

namespace App\Repositories;

use App\Models\Role;
use App\Models\Permission;
use App\Traits\ModelRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class RoleRepository extends Repository implements RoleRepositoryInterface
{
    use ModelRepository;

    protected $model;

    public function __construct(Role $role) {
        parent::__construct();
        $this->model = $role;
    }

    public function all()
    {
        return $this->model->withCount('users')->get();
    }

    public function create(array $data)
    {
        return $this->model->create($data);
    }

    public function update(array $data, $id)
    {
        $role = $this->find($id);
        return $role->update($data) ? $role : null;
    }

    public function createWithPermissions(array $data, array $permissions)
    {
        $role = $this->create($data);
        return $role->givePermissionTo(array_keys($permissions));
    }

    public function updateWithPermissions($id, array $data, array $permissions)
    {
        $role = $this->update($data, $id);
        return $role->syncPermissions(array_keys($permissions));
    }

    public static function with()
    {
        return [
            'permissions' => Permission::actionView()->get()->groupBy(function ($item, $key) {
                return explode('.', $item->slug)[0];
            })
        ];
    }
}
