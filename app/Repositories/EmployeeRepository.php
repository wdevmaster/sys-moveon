<?php

namespace App\Repositories;

use App\Models\User;
use App\Models\Role;
use App\Models\Country;
use App\Enums\PersonalSex;
use App\Models\Tenant\Branch;
use App\Traits\ModelRepository;
use App\Models\System\Franchise;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class EmployeeRepository extends Repository implements EmployeeRepositoryInterface
{
    use ModelRepository;

    protected $model;

    public function __construct(User $user) {
        parent::__construct();
        $this->model = $user;
    }

    public function all($role)
    {
        $query = $this->model->findRole($role);
        $queryCopy = $query;

        return [
            'active' => $query->get(),
            'trash' => $queryCopy->onlyTrashed()->get()
        ];
    }

    public function create(array $data)
    {
        $data = collect($data);
        if (!collect($data->get('user'))->has('password'))
            $data = $data->merge([ 'user' => array_merge(
                $data->get('user'),
                ['password' => 'password']
            )]);

        $model = $this->createUser(
            collect($data->get('user')),
            $this->model
        );

        $this->createPersonal(
            $data->only('address', 'personalData'), $model
        );

        return $model;
    }

    public function update(array $data, $id)
    {
        $model = $this->find($id);
        $data = collect($data);
        $model->load('personalData');

        $this->updateUser(collect(
            $data->get('user')), $model
        );

        $this->updatePersonal(
            $data->only('address', 'personalData'),
            $model->personalData
        );

        return $model;
    }

    public function find($id)
    {
        if (!$id || null == $model = $this->model->withTrashed()->find($this->decodeId($id))) {
            throw new ModelNotFoundException(
                __(
                    'The :name was not found',
                    [ 'name' => __(\Str::afterLast(get_class($this->model), '\\')) ]
                )
            );
        }

        return $model;
    }

    public function manager(array $data)
    {

    }

    public static function with()
    {
        if (request()->is('app/*')) {
            $code = explode('/',request()->path())[1];
            $franchise = Franchise::findByCode($code);
        }

        return [
            'countries' => Country::pluck('name', 'code'),
            'roles' => Role::actionView()->get(),
            'branches' => \Auth::user()->branches,
            'sexs' => PersonalSex::choices(),
            'franchise' => $franchise ?? null
        ];
    }
}
