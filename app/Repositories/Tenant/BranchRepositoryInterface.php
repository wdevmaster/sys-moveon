<?php

namespace App\Repositories\Tenant;

use App\Repositories\RepositoryInterface;

interface BranchRepositoryInterface extends RepositoryInterface
{
    public function all();
}
