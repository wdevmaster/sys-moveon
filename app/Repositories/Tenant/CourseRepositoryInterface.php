<?php

namespace App\Repositories\Tenant;

use App\Repositories\RepositoryInterface;

interface CourseRepositoryInterface extends RepositoryInterface
{
    public function all();

    public function restore($id);

    public function manager(array $data, $id);
}
