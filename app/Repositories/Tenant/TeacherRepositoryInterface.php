<?php

namespace App\Repositories\Tenant;

use App\Repositories\RepositoryInterface;

interface TeacherRepositoryInterface extends RepositoryInterface
{
    public function all($type);

    public function restore($id);

    public function manager(array $data, $id);
}
