<?php

namespace App\Repositories\Tenant;

use App\Repositories\RepositoryInterface;

interface PlanRepositoryInterface extends RepositoryInterface
{
    public function all();
}
