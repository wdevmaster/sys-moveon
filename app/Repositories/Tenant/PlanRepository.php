<?php

namespace App\Repositories\Tenant;

use App\Enums;
use App\Models\Tenant;
use App\Traits\ModelRepository;
use App\Repositories\Repository;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PlanRepository extends Repository implements PlanRepositoryInterface
{
    use ModelRepository;

    protected $model;

    public function __construct(Tenant\Plan $plan) {
        parent::__construct();
        $this->model = $plan;
    }

    public function all()
    {
        $query = $this->model->query();

        return [
            'active' => $query->get(),
            'trash' => $query->onlyTrashed()->get()
        ];
    }

    public function create(array $data)
    {
        $data = collect($data);
        $plan = $this->model->create(array_merge(
            $data->get('plan'),
            ['is_default' => $data->get('plan')['is_default']??false]
        ));

        if ($data->get('features'))
            $plan->features()->createMany($data->get('features'));

        $plan->branches()->attach($data->get('branches'));
        $plan->load('features');

        return $plan;
    }

    public function update(array $data, $id)
    {
        $data = collect($data);
        $plan = $this->find($id);
        $plan->update(array_merge(
            $data->get('plan'),
            ['is_default' => $data->get('plan')['is_default']??false]
        ));

        $plan->features()->delete();
        if ($data->get('features'))
            $plan->features()->createMany($data->get('features'));

        if (\Auth::user()->branches->count() > 1)
            $plan->branches()->sync($data->get('branches'));

        $plan->load('features');
        return $plan;
    }

    public function find($id)
    {
        if (!$id || null == $model = $this->model->withTrashed()->find($this->decodeId($id))) {
            throw new ModelNotFoundException(
                __(
                    'The :name was not found',
                    [ 'name' => __('Teacher') ]
                )
            );
        }

        return $model;
    }

    public function delete($id)
    {
        $model = $this->find($id);
        if ($model->trashed()) {
            return $model->forceDelete();
        }

        return $model->delete();
    }

    public static function with()
    {
        return [
            'branches' => \Auth::user()->branches,
            'intervals' => Enums\PlanInterval::choices(),
            'featureTypes' => Enums\FeatureType::choices(),
            'featureOrigin' => Enums\FeatureOrigin::choices()
        ];
    }
}
