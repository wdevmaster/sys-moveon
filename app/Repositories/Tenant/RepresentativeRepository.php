<?php

namespace App\Repositories\Tenant;

use App\Models\User;
use App\Models\Role;
use App\Models\Tenant;
use App\Models\Country;
use App\Enums\PersonalSex;
use App\Traits\ModelRepository;
use App\Models\System\Franchise;
use App\Repositories\Repository;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class RepresentativeRepository extends Repository implements RepresentativeRepositoryInterface
{
    use ModelRepository;

    protected $model;

    public function __construct(User $user) {
        parent::__construct();
        $this->model = $user;
    }

    public function all()
    {
        $query = $this->model->representatives();
        $queryCopy = $query;

        return [
            'active' => $query->get(),
            'trash' => $queryCopy->onlyTrashed()->get()
        ];
    }

    public function create(array $data)
    {
        $data = collect($data);
        if (!collect($data->get('user'))->has('password'))
            $data = $data->merge([ 'user' => array_merge(
                $data->get('user'),
                ['password' => 'password']
            )]);

        $model = $this->createUser(
            collect($data->get('user')),
            $this->model
        );

        $this->createPersonal(
            $data->only('address', 'personalData'), $model
        );

        return $model;
    }

    public function update(array $data, $id)
    {
        $model = $this->find($id);
        $data = collect($data);
        $model->load('personalData');

        $this->updateUser(collect(
            $data->get('user')), $model
        );

        $this->updatePersonal(
            $data->only('address', 'personalData'),
            $model->personalData
        );

        return $model;
    }

    public function find($id)
    {
        if (!$id || null == $model = $this->model->withTrashed()->find($this->decodeId($id))) {
            throw new ModelNotFoundException(
                __(
                    'The :name was not found',
                    [ 'name' => __(\Str::afterLast(get_class($this->model), '\\')) ]
                )
            );
        }

        return $model;
    }

    public function get(array $data)
    {
        $data = collect($data);
        if (!$data->has('ci') && !$data->get('ci'))
                throw new ModelNotFoundException(
                    __('The :attribute field is required.', [ 'attribute' => __('ci') ])
                );

        if (!$row = $this->model->findByCi($data->get('ci'))->first())
            return [];

        return [
            'user' => $row->user,
            'personalData' => collect($row->personalData->toArray())->except('address'),
            'address' => $row->address
        ];
    }

    public function manager(array $data, $id)
    {
        $model = $this->find($id);
        $data = collect($data);

        if ($data->get('model') == 'role') {
            $model->assignRoles('student');

            $model->enrollment()->create([
                'created_at' => date("Y-m-d H:i:s"),
                'student_type_id' => $this->getTypeStuden(
                    $model->personalData->age
                )
            ]);
        }

        return $model;
    }

    public static function with()
    {
        if (request()->is('app/*')) {
            $code = explode('/',request()->path())[1];
            $franchise = Franchise::findByCode($code);
        }

        return [
            'countries' => Country::pluck('name', 'code'),
            'roles' => Role::representative()->get(),
            'branches' => \Auth::user()->branches,
            'sexs' => PersonalSex::choices(),
            'franchise' => $franchise ?? null,
            'role' => 'representative'
        ];
    }

    private function getTypeStuden($age)
    {
        foreach (Tenant\StudentType::get() as $row)
            if ($age >= $row->age_from && $age < $row->age_up)
                break;

        return $row->id;
    }
}
