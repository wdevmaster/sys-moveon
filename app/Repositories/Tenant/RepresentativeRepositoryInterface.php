<?php

namespace App\Repositories\Tenant;

use App\Repositories\RepositoryInterface;

interface RepresentativeRepositoryInterface extends RepositoryInterface
{
    public function all();
}
