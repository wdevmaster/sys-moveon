<?php

namespace App\Repositories\Tenant;

use App\Enums;
use App\Models;
use Carbon\Carbon;
use App\Models\System;
use App\Models\Tenant;
use App\Traits\ModelRepository;
use App\Repositories\Repository;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CourseRepository extends Repository implements CourseRepositoryInterface
{
    use ModelRepository;

    public $model;

    public function __construct(Tenant\Course $model) {
        $this->model = $model;
    }

    public function all()
    {
        return $this->model->get();
    }

    public function getStudent(array $data)
    {
        $data = collect($data);

        if ($data->get('course_id')) {
            $course = $this->find($data->get('course_id'));
            $coursables = $course->coursables->where('id', $data->get('coursable_id'))->first();
        } elseif($data->get('coursable_id')) {
            $course = $this->model->coursable($data->get('coursable_id'))->first();
            $coursables = $course->coursables->where('id', $data->get('coursable_id'))->first();
        }

        if (isset($coursables))
            return $coursables->students;

        return collect([]);
    }

    public function create(array $data)
    {
        $data = collect($data);

        $course = $this->model->firstOrCreate(
            $data->only([
                'branch_id',
                'name',
                'language_id',
                'student_type_id',
                'level_id'
            ])->toArray(),
        );

        $coursables = $course->coursables()->create(
            $data->except([
                'days',
                'branch_id',
                'name',
                'language_id',
                'student_type_id',
                'level_id'
            ])->toArray()
        );

        if ($data->get('plan_id'))
            $coursables->update([
                'status' => Enums\CoursableStatus::INREG()
            ]);

        foreach (array_keys($data->get('days')) as $key => $day)
            $arry[] = [ 'day' =>  $day];

        $coursables->days()->createMany($arry);
        $coursables->load('days');
        $course->load('coursables');

        return $course;
    }

    public function update(array $data, $id)
    {
        $data = collect($data);
        $course = $this->find($id);
        $coursables = $course->coursables->where('id', $data->get('coursable_id'))->first();

        $coursables->update(
            $data->except(['_token', '_method', 'days', 'coursable_id', 'name'])->toArray()
        );

        foreach (array_keys($data->get('days')) as $key => $day)
            $arry[] = [ 'day' =>  $day];

        $coursables->days()->delete();
        $coursables->days()->createMany($arry);
        $coursables->load('days');
        $course->load('coursables');

        return $course;
    }

    public function delete($id)
    {
        $model = $this->find($id);
        return $model->coursables->last()->delete();
    }

    public function manager(array $data, $id)
    {
        # code...
    }

    public static function with()
    {
        $branches = \Auth::user()->branches()->with('programs.levels', 'schedules')->get();

        return [
            'branches'  => $branches,
            'studentType'  => Tenant\StudentType::pluck('name', 'id'),
            'json' => $branches,
            'plans' => Tenant\Plan::get()->pluck('fullname', 'id'),
            'intervals' => Enums\PlanInterval::choices(),
            'featureTypes' => Enums\FeatureType::choices(),
            'featureOrigin' => Enums\FeatureOrigin::choices()
        ];
    }
}
