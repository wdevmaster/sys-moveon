<?php

namespace App\Repositories\Tenant;

use App\Models;
use App\Models\Tenant;
use App\Traits\ModelRepository;
use App\Repositories\Repository;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class HallRepository extends Repository implements HallRepositoryInterface
{
    use ModelRepository;

    protected $model;

    public function __construct(Tenant\Hall $branch) {
        parent::__construct();
        $this->model = $branch;
    }

    public function all()
    {
        return $this->model->all();
    }

    public function sync(array $data)
    {
        $data = collect($data);

        return $this->model->updateOrCreate(
            $data->only(['id'])->toArray(),
            $data->except(['id'])->toArray()
        );
    }

    public function delete($id)
    {
        $model = $this->find($id);

        if ($model->trashed()) {
            $model->deleteRelationships();
            return $model->forceDelete();
        }

        return $model->delete();
    }

    public function manager(array $data, $id)
    {
        $model = $this->find($id);
        $data = collect($data);

        $model->update(
            $data->only('status')->toArray()
        );

        return $model;
    }

    public static function with()
    {
        return [
            'branches' => \Auth::user()->branches->where('online', false)
        ];
    }
}
