<?php

namespace App\Repositories\Tenant;

use App\Enums;
use App\Models;
use Carbon\Carbon;
use App\Models\Tenant;
use App\Traits\ModelRepository;
use App\Repositories\Repository;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class TeacherRepository extends Repository implements TeacherRepositoryInterface
{
    use ModelRepository;

    public $model;

    public function __construct(Models\User $user) {
        parent::__construct();
        $this->model = $user;
    }

    public function all($type)
    {
        $query = $this->model->teachers($type);
        $queryCopy = $query;

        return [
            'active' => $query->get(),
            'trash' => $queryCopy->onlyTrashed()->get()
        ];
    }

    public function allWith(array $data)
    {
        $data = collect($data);
        if (!$data->get('branch_id'))
            throw new ModelNotFoundException(
                __('The :name was not found', [ 'name' => __('Branch') ])
            );

        $query = $this->model->teachers(Enums\TeacherType::ACTIVE());

        if ($branch = $data->get('branch_id'))
            $query->whereBranch($branch);

        if ($type = $data->get('student_type_id'))
            $query->whereStudentType($type);

        if ($days = $data->get('days'))
            $query->whereScheduleDay($days);

        if (
            $data->get('schedule_id') &&
            $schedule = Tenant\Schedule::find($data->get('schedule_id'))
        ) {
            $query->whereScheduleTime(
                $schedule->only('start_time', 'end_time')
            );
        }

        if ($data->get('course'))
            $query->doesntHaveCourse(
                $data->only(['days', 'schedule_id'])->toArray()
            );

        return $query->get()->pluck('fullname', 'id');
    }

    public function create(array $data)
    {
        if ($data['type'] == 'postulant')
            $data['user']['is_active'] = false;

        $data = collect($data);
        if (!collect($data->get('user'))->has('password'))
            $data = $data->merge([ 'user' => array_merge(
                $data->get('user'),
                ['password' => 'password']
            )]);

        $model = $this->createUser(
            collect($data->get('user'))->merge($data->only('type')),
            $this->model
        );

        $personal = $this->createPersonal(
            $data->only('address', 'personalData'), $model
        );

        $this->updateOrCreateAcademic(
            $personal,
            collect($data->get('academic'))
        );

        $model->languages()->attach(
            collect($data->get('user'))->get('languages')
        );

        $model->studentType()->attach(
            collect($data->get('user'))->get('student')
        );

        $this->updateOrCreateLog(
            collect($data->get('personalData'))->get('logs'),
            $personal
        );

        if ($ranting = collect($data->get('user'))->get('ranting'))
            foreach ($ranting as $key => $value) {
                $model->points()->create([
                    'type' => $key,
                    'value' => (float) $value
                ]);
            }

        return $model;
    }

    public function update(array $data, $id)
    {
        $model = $this->find($id);
        $data = collect($data);

        $this->updateUser(
            collect($data->get('user'))->merge($data->only('type')),
            $model
        );

        $personal = $this->updatePersonal(
            $data->only('address', 'personalData'),
            $model->personalData
        );

        $this->updateOrCreateAcademic(
            $personal,
            collect($data->get('academic'))
        );

        $model->languages()->sync(
            collect($data->get('user'))->get('languages')
        );

        $model->studentType()->sync(
            collect($data->get('user'))->get('student')
        );

        $this->updateOrCreateLog(
            collect($data->get('personalData'))->get('logs'),
            $personal
        );

        if ($ranting = collect($data->get('user'))->get('ranting'))
            foreach ($ranting as $key => $value) {
                $model->points()->where('type', $key)->update([
                    'value' => (float) $value
                ]);
            }

        return $model;
    }

    public function manager(array $data, $id)
    {
        $model = $this->find($id);
        $data = collect($data);

        if ($data->get('model') == 'user')
            $model->update([
                'is_active' => $data->get('is_active')
            ]);

        $row = $data->only('type', 'status');

        if ($row->get('type') == 'retired') {
            $model->update([
                'is_active' => false
            ]);

            $row = collect(array_merge(
                $row->toArray(),
                ['contract_end' => Carbon::now()]
            ));
        } elseif ($row->get('type') == 'active') {
            $model->update([
                'is_active' => true
            ]);

            $row = collect(array_merge(
                $row->toArray(),
                [
                    'contract_start' => Carbon::now(),
                    'contract_end' => null
                ]
            ));
        }

        if ($row->isNotEmpty())
            $model->staff()->update($row->toArray());

        return $model;
    }

    public function delete($id)
    {
        $model = $this->find($id);
        $model->update(['is_active' => false]);

        if (
            $model->trashed() ||
            $model->type->is_retired
        ) {
            $model->deleteRelationships();
            return $model->forceDelete();
        }

        return $model->delete();
    }

    public function find($id)
    {
        if (!$id || null == $model = $this->model->withTrashed()->find($this->decodeId($id))) {
            throw new ModelNotFoundException(
                __(
                    'The :name was not found',
                    [ 'name' => __('Teacher') ]
                )
            );
        }

        return $model;
    }

    public static function with()
    {
        $branch = \Auth::user()->branches;
        foreach (Enums\PersonalLogType::choices() as $key => $value) {
            if ($key == 'recomm' || $key == 'observ')
                $logTypes['recomm'][$key] = $value;
            elseif($key != 'log')
                $logTypes['health'][$key] = $value;
        }
        $languages = $branch->first()->programs()->with('language')->get();

        return [
            'countries' => Models\Country::pluck('name', 'code'),
            'roles' => Models\Role::teacher()->get(),
            'branches' => $branch,
            'sexs' => Enums\PersonalSex::choices(),
            'languages' => $languages->pluck('language.name', 'language.id'),
            'inst_types' => Enums\InstitutionType::choices(),
            'titles' => Enums\AcademicTitle::choices(),
            'langLogType' => Enums\PersonalLogType::choices(),
            'logTypes' => $logTypes,
            'institutions' => Models\Tenant\Institution::select('name', 'type')->get(),
            'titleType' => Enums\AcademicTitle::choices(),
            'studentType' => Models\Tenant\StudentType::pluck('name', 'id')
        ];
    }
}
