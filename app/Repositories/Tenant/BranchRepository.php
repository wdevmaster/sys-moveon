<?php

namespace App\Repositories\Tenant;

use App\Models\Country;
use App\Models\Tenant\Branch;
use App\Models\System\Program;
use App\Traits\ModelRepository;
use App\Repositories\Repository;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class BranchRepository extends Repository implements BranchRepositoryInterface
{
    use ModelRepository;

    protected $model;

    public function __construct(Branch $branch) {
        parent::__construct();
        $this->model = $branch;
    }

    public function all()
    {
        return $this->model->withTrashed()->get();
    }

    public function create(array $data)
    {
        $data = collect($data);
        $address = $this->createAddress($data->get('address'));
        $branch = $this->model->create(
            $data->only('name')->merge([
                'address_id' => isset($address) ? $address->id : null
            ])->toArray()
        );

        $branch->programs()->attach(
            array_keys($data['programs']),
        );

        return $branch;
    }

    public function update(array $data, $id)
    {
        $data = collect($data);
        $branch = $this->find($id);
        $this->updateAddress($data->get('address'), $branch->address);
        $branch->programs()->sync(
            array_keys($data['programs']),
        );

        return $branch->update([
            'name' => $data['name'],
        ]);
    }

    public function delete($id)
    {
        $model = $this->find($id);

        if ($model->trashed()) {
            $model->deleteRelationships();
            return $model->forceDelete();
        }

        return $model->delete();
    }

    public function find($id)
    {
        if (!$id || null == $model = $this->model->withTrashed()->find($this->decodeId($id))) {
            throw new ModelNotFoundException(
                __(
                    'The :name was not found',
                    [ 'name' => __(\Str::afterLast(get_class($this->model), '\\')) ]
                )
            );
        }

        return $model;
    }

    public function restore($id)
    {
        $model = $this->find($id);

        return $model->restore();
    }

    public static function with()
    {
        return [
            'countries' => Country::pluck('name', 'code'),
            'programs' => Program::get()
        ];
    }
}
