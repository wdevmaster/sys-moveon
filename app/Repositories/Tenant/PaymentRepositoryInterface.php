<?php

namespace App\Repositories\Tenant;

use App\Repositories\RepositoryInterface;

interface PaymentRepositoryInterface extends RepositoryInterface
{
    public function all();
}
