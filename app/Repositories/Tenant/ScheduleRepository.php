<?php

namespace App\Repositories\Tenant;

use App\Enums;
use App\Models;
use App\Models\Tenant;
use App\Traits\ModelRepository;
use App\Repositories\Repository;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ScheduleRepository extends Repository implements ScheduleRepositoryInterface
{
    use ModelRepository;

    protected $model;

    public function __construct(Tenant\Schedule $schedule) {
        parent::__construct();
        $this->model = $schedule;
    }

    public function all(string $type)
    {
        return $this->model
                     ->where('type', $type)
                     ->orderBy('branch_id')
                     ->orderBy('start_time')
                     ->get();
    }

    public function getLapses()
    {
        return $this->model->lapse()->get()
                    ->groupBy('branch_id')->first();
    }

    public function allWithinHours(array $data)
    {
        $response = [];
        if (!key_exists('b', $data))
            throw new ModelNotFoundException(
                __('The :name was not found', [ 'name' => __('teacher') ])
            );

        $schedule = [];
        $rows = $this->model->teacher()
                    ->branchHas($data['b'])
                    ->orderBy('day')
                    ->orderBy('start_time')
                    ->get();

        if ($rows->isNotEmpty()) {
            foreach ($rows->groupBy('day') as $key => $rows) {
                foreach ($rows as $item) {
                    $schedule[$key][$item->start_time->format('H')][] = $item->user_id;
                }
            }

            $rows = collect($schedule);
        }

        return $rows;
    }

    public function sync(array $data)
    {
        $data = collect($data);
        $type = $data->get('type');

        if ($data->get('user_id'))
            $this->model->where('user_id', $data->get('user_id'))->delete();

        if ($type->is_lap)
            collect($data->get('data'))->groupBy('branch_id')->keys()->map(function ($item, $key){
                $this->model->where('branch_id', $item)->delete();
            });

        foreach ($data->get('data') as $row)
            $rowNew[] = $this->model->create(array_merge(
                $row,
                [ 'type' => $type->value() ]
            ));

        if (collect($data->get('data'))->isNotEmpty())
            return collect($rowNew);
    }

    public function delete($id)
    {
        $model = $this->find($id);
        return $model->delete();
    }

    public static function with() {
        return [
            'branches' => \Auth::user()->branches,
        ];
    }

    private function formatData(array $data)
    {
        return collect(json_decode($data['data'], true));
    }

    public function format(array $data, array $merge)
    {
        $arry = [];
        foreach ($data as $d => $rows) {
            foreach ($this->model->find(array_keys($rows)) as $key => $row) {
                $arry[] = array_merge(
                    [
                        'day' => $d,
                        'start_time' => $row->start_time->format('H:i:s'),
                        'end_time' => $row->end_time->format('H:i:s')
                    ],
                    $merge
                );
            }
        }

        return $arry;
    }
}
