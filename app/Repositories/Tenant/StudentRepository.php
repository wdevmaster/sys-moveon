<?php

namespace App\Repositories\Tenant;

use App\Enums;
use App\Models;
use Carbon\Carbon;
use App\Traits\ModelRepository;
use App\Repositories\Repository;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class StudentRepository extends Repository implements StudentRepositoryInterface
{
    use ModelRepository;

    protected $model;

    public function __construct(Models\User $user) {
        parent::__construct();
        $this->model = $user;
    }

    public function all($status)
    {
        $query = $this->model->students($status);
        $queryCopy = $query;

        return [
            'active' => $query->get(),
            'trash' => $queryCopy->onlyTrashed()->get()
        ];
    }

    public function allWith(array $data)
    {
        $data = collect($data);
        $query = $this->model->students(Enums\StudentStatus::ACTIVE());

        if ($branch = $data->get('branch_id'))
            $query->whereBranch($branch);

        if ($type = $data->get('student_type_id'))
            $query->whereEnrollmentType($type);

        if ($coursable = $data->get('coursable_id'))
            $query->whereDoesntHaveCourse($coursable);

        return $query->get();
    }

    public function create(array $data)
    {
        $data = collect($data);
        if (!collect($data->get('user'))->has('password'))
            $data = $data->merge([ 'user' => array_merge(
                $data->get('user'),
                ['password' => 'password']
            )]);

        $model = $this->createUser(
            collect($data->get('user'))->merge($data->only('status')),
            $this->model
        );

        if ($data->get('representative'))
            $model->parent()->attach(
                $data->get('representative')
            );

        $personal = $this->createPersonal(
            $data->only('address', 'personalData'), $model
        );

        $this->updateOrCreateAcademic(
            $personal,
            collect($data->get('academic'))
        );

        $model->languages()->attach(
            collect($data->get('user'))->get('languages')
        );

        $this->updateOrCreateLog(
            collect($data->get('personalData'))->get('logs'),
            $personal
        );

        return $model;
    }

    public function update(array $data, $id)
    {
        $model = $this->find($id);
        $data = collect($data);

        $this->updateUser(
            collect($data->get('user'))->merge($data->only('status')),
            $model
        );

        $personal = $this->updatePersonal(
            $data->only('address', 'personalData'),
            $model->personalData
        );

        if ($academic = $data->get('academic'))
            $this->updateOrCreateAcademic(
                $personal,
                collect($academic)
            );

        if ($languages = collect($data->get('user'))->get('languages'))
            $model->languages()->sync(
                $languages
            );

        if ($logs = collect($data->get('personalData'))->get('logs'))
            $this->updateOrCreateLog(
                $logs,
                $personal
            );

        return $model;
    }

    public function manager(array $data, $id)
    {
        $model = $this->find($id);
        $data = collect($data);

        if ($data->get('model') == 'user')
            $model->update([
                'is_active' => $data->get('is_active')
            ]);

        $row = $data->only('status');

        if (
            $row->get('status') == 'graduate' ||
            $row->get('status') == 'retired'
        ) {
            $model->update([
                'is_active' => false
            ]);
        } elseif ($row->get('status') == 'active') {
            $model->update([
                'is_active' => true
            ]);
        }

        if ($row->isNotEmpty())
            $model->enrollment()->update($row->toArray());

        return $model;
    }

    public function delete($id)
    {
        $model = $this->find($id);
        if (!$model->hasRole('representative')) {
            $model->update(['is_active' => false]);

            if (
                $model->trashed() ||
                $model->enrollment->is_retired
            ) {
                $model->deleteRelationships();
                return $model->forceDelete();
            }

            return $model->delete();
        }

        $model->removeRoles('student');
    }

    public function find($id)
    {
        if (!$id || null == $model = $this->model->withTrashed()->find($this->decodeId($id))) {
            throw new ModelNotFoundException(
                __(
                    'The :name was not found',
                    [ 'name' => __('Student') ]
                )
            );
        }

        return $model;
    }

    public static function with()
    {
        $branch = \Auth::user()->branches;
        foreach (Enums\PersonalLogType::choices() as $key => $value) {
            if ($key == 'recomm' || $key == 'observ')
                $logTypes['recomm'][$key] = $value;
            elseif($key != 'log')
                $logTypes['health'][$key] = $value;
        }
        $languages = $branch->first()->programs()->with('language')->get();

        return [
            'countries' => Models\Country::pluck('name', 'code'),
            'roles' => Models\Role::teacher()->get(),
            'branches' => $branch,
            'sexs' => Enums\PersonalSex::choices(),
            'languages' => $languages->pluck('language.name', 'language.id'),
            'inst_types' => Enums\InstitutionType::choices(),
            'titles' => Enums\AcademicTitle::choices(),
            'langLogType' => Enums\PersonalLogType::choices(),
            'logTypes' => $logTypes,
            'institutions' => Models\Tenant\Institution::select('name', 'type')->get(),
            'titleType' => Enums\AcademicTitle::choices(),
            'studentType' => Models\Tenant\StudentType::pluck('name', 'id')
        ];
    }
}
