<?php

namespace App\Repositories\Tenant;

use App\Repositories\RepositoryInterface;

interface HallRepositoryInterface
{
    public function all();

    public function sync(array $data);

    public function delete($id);

    public function manager(array $data, $id);

    public function find($id);

    public static function with();
}
