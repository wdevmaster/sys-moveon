<?php

namespace App\Repositories\Tenant;

use App\Enums;
use App\Models;
use App\Models\Tenant;
use App\Traits\ModelRepository;
use App\Repositories\Repository;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PaymentRepository extends Repository implements PaymentRepositoryInterface
{
    use ModelRepository;

    public $model;

    public function __construct(Tenant\Payment $model) {
        parent::__construct();
        $this->model = $model;
    }

    public function all()
    {
        $rows = $this->model->orderBy('created_at', 'DESC')->get();
        return $rows->mapToGroups(function ($item, $key) {
            return [
               $item->code => $item
            ];
        });
    }

    public function getPlanDues(array $data)
    {
        $data = collect($data);
        $response = [];

        $plan = Tenant\Plan::find($data->get('plan_id'));
        $response['plan_id'] = $plan->id;
        $response['plan'] = $plan->fullname;

        if ($coursable = Tenant\Coursable::find($data->get('coursable_id'))) {
            if ($plan->interval_unit->is_month)
                $response['dues_total'] = (int) round($coursable->duration_week / 4, 2);
            else
                $response['dues_total'] = $coursable->duration_week;
        }

        $d_dues = $this->model->where('user_id', $data->get('user_id'))
                             ->where('coursable_id', $data->get('coursable_id'))
                             ->groupBy('dues')
                             ->selectRaw('sum(amount) as amount_dues, dues')
                             ->get();

        $dues = [];
        $d = $response['dues_total'];
        if ($plan->features->isNotEmpty())
            foreach ($plan->features as $feature) {
                if ($feature->type->is_discount) {
                    $dues[] = $feature->value;
                } elseif ($feature->type->is_inscription) {
                    $amount = $feature->value;
                    if ($feature->is_percentage) {
                        $amount = $coursable->price * ($feature->value/100);
                        $response['dues_total'] = round($response['dues_total'] - ($response['dues_total'] * ($feature->value/100)));
                        $d = $response['dues_total'];
                    }
                    $dues[] = $amount;
                    $d--;
                } elseif ($feature->type->is_discount) {
                    $price_due = $coursable->price / $coursable->duration_week;
                    $amount = $feature->value;
                    if ($feature->is_percentage)
                        $amount = $price_due * ($feature->value/100);


                    for ($i=0; $i < $d; $i++) {
                        $dues[] = $price_due - $amount;
                    }
                }
            }

        if (count($dues) < $response['dues_total']) {
            $price_due = round($coursable->price / $response['dues_total']);

            for ($i=0; $i < $response['dues_total']; $i++)
                $dues[] = $price_due;
        }

        $response['dues_count'] = 0;
        if ($d_dues->isNotEmpty())
            foreach ($d_dues as $row) {
                $dues[$row->dues] -= $row->amount_dues;

                if ($dues[$row->dues] <= 0)
                    $response['dues_count']++;
            }

        $response['total_paid'] = array_sum($dues);
        $response['dues'] = $dues;

        return $response;
    }

    public function create(array $data)
    {
        $data = collect($data);
        $planable = $this->getPlanDues($data->toArray());
        $user = Models\User::find($data->get('user_id'));

        if ($data->get('bank'))
            $bank = Tenant\Bank::firstOrCreate([
                'name' => $data->get('bank'),
                'type' => $data->get('payment_method')
            ]);

        $amount = $data->get('amount');
        $code =date('ymd');
        $dat = array_merge(
            $data->only('user_id', 'plan_id', 'coursable_id', 'payment_method')->toArray(),
            [
                'code' => "{$data->get('user_id')}-{$data->get('coursable_id')}-$code",
                'voucher_path' => $this->uploadVoucher(
                    $data->get('voucher_path'),
                    \Str::slug($user->fullname)
                ),
                'bank_id' => isset($bank) ? $bank->id : null
            ]
        );
        foreach ($planable['dues'] as $k => $value) {

            if (
                count($planable['dues']) > $planable['dues_total'] &&
                $k == 0
            ) {
                $dat['is_inscription'] = true;
            } else {
                $dat['is_inscription'] = false;
            }

            if ($value == 0)
                continue;

            if ($amount < $value || $amount == $value) {
                $this->model->create(array_merge(
                    $dat,
                    [ 'dues' => $k, 'amount' => $amount ]
                ));
                break;
            } elseif(($amount -= $value) > 0) {
                $this->model->create(array_merge(
                    $dat,
                    [ 'dues' => $k, 'amount' => $value]
                ));
            }

            if ($amount <= 0)
                break;
        }
    }

    public function update(array $data, $id)
    {
        # code...
    }

    public function manager(array $data, $id)
    {
        $data = collect($data);
        if ($data->get('status'))
            $this->model->where('code', $id)->update([
                'status' => $data->get('status')
            ]);
    }

    public static function with()
    {
        $plans = Tenant\Plan::get();
        return [
            'branches' => \Auth::user()->branches,
            'plans' => $plans->pluck('fullname', 'id'),
            'fullplans' => $plans,
            'paymentMethods' => Enums\PaymentMethod::choices(),
            'banks' => Tenant\Bank::select('name', 'type')->get(),
            'status' => Enums\PaymentStatus::values(),
        ];
    }

    private function uploadVoucher($file, $username)
    {
        $disk = request()->is('app/*') ? 'tenant' : 'public';
        $dir = base64_encode('payments');
        $date = date("Y-m-d_H-i-s");
        $fileName = "{$date}_{$username}.{$file->getClientOriginalExtension()}";

        return \Storage::disk($disk)->putFileAs($dir, $file, $fileName);
    }
}
