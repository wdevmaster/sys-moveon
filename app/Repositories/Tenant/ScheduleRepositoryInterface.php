<?php

namespace App\Repositories\Tenant;

use App\Repositories\RepositoryInterface;

interface ScheduleRepositoryInterface
{
    public function all(string $type);

    public function sync(array $data);

    public function allWithinHours(array $data);

    public function delete($id);

    public function find($id);

    public static function with();
}
