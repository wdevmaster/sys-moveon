<?php

namespace App\Repositories;

interface RoleRepositoryInterface extends RepositoryInterface
{
    public function all();

    public function restore($id);

    public function createWithPermissions(array $data, array $permissions);

    public function updateWithPermissions($id, array $data, array $permissions);
}
