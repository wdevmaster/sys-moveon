function funcAddLog(dom)
{
    var group = dom.data('group')
    var type = $(`#${group}-type`).children("option:selected").val()
    var body = $(`#${group}-body`).val()

    validate($(`#${group}-type`), type)
    validate($(`#${group}-body`), body)

    if (type && body) {
        logs.push({
            type: type,
            group: group,
            body: body
        })
    }
    $(`#${group}-type`).val('')
    $(`#${group}-body`).val('')

    renderLogs(group)
}

function renderLogs(group = null) {

  $(`.${group}-render-log div`).remove()

  $.each(logs, function(i,log) {
      if (log.group == group)
          $(`.${log.group}-render-log`).append(`
            <div class="list-group-item list-group-item-action">
              <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-0">${langLogType[log.type]}</h5>
                <small>
                  <a
                    href="#"
                    class="log-remove ${disabled}"
                    data-key="${i}"
                    data-group="${group}"
                    data-toggle="tooltip"
                    data-original-title="{{__('Remove')}}"
                  >
                      <i class="feather icon-trash-2 danger"></i>
                  </a>
                </small>
              </div>
              <p class="mb-0">${log.body}</p>
            </div>
          `)
  })

  $('input#logs').val(JSON.stringify(logs))
}

$(document).on('click', '.log-remove', function (e) {
  e.preventDefault()
  const key = $(this).data('key')
  const group = $(this).data('group')
  let arry = []

  $.each(logs, function(i,log) {
      if (i != key)
          arry.push(log)
  })
  logs = arry

  renderLogs(group)
})

function validate(dom, dat) {
  if (!dat)
      dom.parent().addClass('has-error')
  else
      dom.parent().removeClass('has-error')
}
