$('a.btn-dropdown').click(function() {
  let data = $(this).data()
  data['_token'] = token

  if ($(`#row-${data.id}`))
    $(`#row-${data.id}`).toggleClass('table-info')

  if (typeof data.type !== 'undefined')
    redirectUrl = redirectUrl.replace(':type', data.type);
  else
    redirectUrl = redirectUrl.replace(':type', type);

  sendData(data, redirectUrl);
});


function sendData(data, redirectUrl) {
  Swal.fire({
    title: lang.are_you_sure,
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#2ecc71',
    confirmButtonText: lang.yes_sure,
    cancelButtonText: lang.no_cancel,
    showLoaderOnConfirm: true,
    inputAttributes: {
      autocapitalize: 'off'
    },
    preConfirm: () => {
      return fetch(managerUrl, {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        method: "PUT",
        body: JSON.stringify(data)
      }).catch(error => {
          Swal.showValidationMessage(
          `Request failed: ${error}`
          )
      })
    },
    allowOutsideClick: () => !Swal.isLoading()
  }).then((result) => {
    if (result.value && result.value.ok) {
        Swal.fire({
          title: lang.updated,
          type:'success',
          confirmButtonText: lang.ok,
        }).then(() => {
          window.location.href = redirectUrl
        })
    } else {
        if ($(`#row-${data.id}`))
            $(`#row-${data.id}`).toggleClass('table-info')

        if (result.value)
            Swal.fire({
              position: 'top-end',
              type: 'error',
              title: lang.oops,
              text: result.value.statusText,
              showConfirmButton: false,
              timer: 2500
            })
    }
  })
}
