<?php

use Illuminate\Database\Migrations\Migration;
use App\Traits\CreatePermissionUserTable as CreatePermissionUser;

class CreatePermissionUserTable extends Migration
{
    use CreatePermissionUser;
}
