<?php

use Illuminate\Database\Migrations\Migration;
use App\Traits\CreatePermissionRoleTable as CreatePermissionRole;

class CreatePermissionRoleTable extends Migration
{
    use CreatePermissionRole;
}
