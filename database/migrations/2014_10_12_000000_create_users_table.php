<?php

use App\Traits\CreateUsersTable as CreateUsers;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    use CreateUsers;
}
