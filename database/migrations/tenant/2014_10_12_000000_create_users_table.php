<?php

use App\Traits\CreateUsersTable as CreateUsers;
use Illuminate\Database\Migrations\Migration;

class TenantCreateUsersTable extends Migration
{
    use CreateUsers;
}
