<?php

use Illuminate\Database\Migrations\Migration;
use App\Traits\CreatePermissionRoleTable as CreatePermissionRole;

class TenantCreatePermissionRoleTable extends Migration
{
    use CreatePermissionRole;
}
