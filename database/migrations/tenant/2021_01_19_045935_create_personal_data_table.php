<?php

use App\Enums\PersonalSex;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonalDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personal_data', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('address_id')->unsigned()->nullable();
            $table->string('ci')->unique()->nullable();
            $table->string('rif')->nullable();
            $table->string('first_name', 100);
            $table->string('last_name', 100);
            $table->date('birthdate')->nullable();
            $table->enum('sex', PersonalSex::values())->nullable();
            $table->string('phone', 100)->nullable();
            $table->string('emergency_phone', 100)->nullable();
            $table->string('profile_photo')->nullable();

            $table->unique('user_id', 'ci');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('address_id')->references('id')->on('addresses')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personal_data');
    }
}
