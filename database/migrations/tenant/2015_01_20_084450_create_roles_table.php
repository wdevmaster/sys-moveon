<?php

use Illuminate\Database\Migrations\Migration;
use App\Traits\CreateRolesTable as CreateRoles;

class TenantCreateRolesTable extends Migration
{
    use CreateRoles;
}
