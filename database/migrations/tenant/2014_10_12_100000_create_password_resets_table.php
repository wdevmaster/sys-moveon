<?php

use App\Traits\CreatePasswordResetsTable as CreatePasswordResets;
use Illuminate\Database\Migrations\Migration;

class TenantCreatePasswordResetsTable extends Migration
{
    use CreatePasswordResets;
}
