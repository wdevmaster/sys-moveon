<?php

use Illuminate\Database\Migrations\Migration;
use App\Traits\CreatePermissionUserTable as CreatePermissionUser;

class TenantCreatePermissionUserTable extends Migration
{
    use CreatePermissionUser;
}
