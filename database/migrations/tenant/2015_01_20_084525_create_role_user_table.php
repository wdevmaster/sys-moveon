<?php

use Illuminate\Database\Migrations\Migration;
use App\Traits\CreateRoleUserTable as CreateRoleUser;

class TenantCreateRoleUserTable extends Migration
{
    use CreateRoleUser;
}
