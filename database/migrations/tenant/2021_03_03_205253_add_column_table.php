<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('coursables', function (Blueprint $table) {
            $table->float('price', 8, 2)->nullable();
            $table->unsignedBigInteger('plan_id')->nullable();
            $table->smallInteger('duration_week')->nullable();
            $table->foreign('plan_id')->references('id')->on('plans');

        });

        Schema::table('coursable_user', function (Blueprint $table) {
            $table->unsignedBigInteger('plan_id')->nullable();
            $table->foreign('plan_id')->references('id')->on('plans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
