<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoursableDaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coursable_days', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('coursable_id')->index();
            $table->integer('day')->nullable();

            $table->foreign('coursable_id')->references('id')->on('coursables')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coursable_days');
    }
}
