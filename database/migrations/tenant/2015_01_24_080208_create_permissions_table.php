<?php

use Illuminate\Database\Migrations\Migration;
use App\Traits\CreatePermissionsTable as CreatePermissions;

class TenantCreatePermissionsTable extends Migration
{
    use CreatePermissions;
}
