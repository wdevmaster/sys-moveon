<?php

use App\Enums\InstitutionType;
use App\Enums\AcademicTitle;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonalAcademicDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personal_academic_data', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('personal_data_id')->unsigned();
            $table->bigInteger('institution_id')->unsigned();
            $table->enum('type', InstitutionType::values());
            $table->enum('title', AcademicTitle::values())->nullable();
            $table->string('dicipline', 100)->nullable();
            $table->date('graduate_year')->nullable();

            $table->foreign('personal_data_id')->references('id')->on('personal_data');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personal_academic_data');
    }
}
