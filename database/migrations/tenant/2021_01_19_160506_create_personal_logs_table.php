<?php

use App\Enums\PersonalLogType;
use App\Enums\PersonalLogGroup;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonalLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personal_logs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('personal_data_id');
            $table->enum('type', PersonalLogType::values())
                    ->default(PersonalLogType::defaultValue());
            $table->enum('group', PersonalLogGroup::values())
                    ->default(PersonalLogGroup::defaultValue());
            $table->text('body');
            $table->timestamps();

            $table->foreign('personal_data_id')->references('id')->on('personal_data');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personal_logs');
    }
}
