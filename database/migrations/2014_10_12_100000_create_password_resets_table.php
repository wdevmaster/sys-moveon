<?php

use App\Traits\CreatePasswordResetsTable as CreatePasswordResets;
use Illuminate\Database\Migrations\Migration;

class CreatePasswordResetsTable extends Migration
{
    use CreatePasswordResets;
}
