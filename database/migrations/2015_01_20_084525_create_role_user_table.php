<?php

use Illuminate\Database\Migrations\Migration;
use App\Traits\CreateRoleUserTable as CreateRoleUser;

class CreateRoleUserTable extends Migration
{
    use CreateRoleUser;
}
