<?php

use Illuminate\Database\Migrations\Migration;
use App\Traits\CreatePermissionsTable as CreatePermissions;

class CreatePermissionsTable extends Migration
{
    use CreatePermissions;
}
