<?php

use Illuminate\Database\Migrations\Migration;
use App\Traits\CreateRolesTable as CreateRoles;

class CreateRolesTable extends Migration
{
    use CreateRoles;
}
