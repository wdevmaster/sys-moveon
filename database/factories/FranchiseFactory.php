<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\System\Franchise;
use Faker\Generator as Faker;

$factory->define(Franchise::class, function (Faker $faker) {
    return [
        'code' => $faker->numberBetween(1000, 9000),
        'name' => $faker->company,
        'locked' => false,
        'is_main' => false,
        'is_multibranch' => false,
    ];
});
