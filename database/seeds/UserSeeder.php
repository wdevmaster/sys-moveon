<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $root = User::create([
            'username' => '@dmin',
            'email' => 'admin@root.com',
            'password' => bcrypt('secret'),
        ]);
    }
}
