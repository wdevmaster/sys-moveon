<?php

use Illuminate\Database\Seeder;
use App\Models\Tenant\StudentType;

class StudentTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rows = [
        	[
        		'name'      => 'Kids',
        		'age_from'  => 4,
        		'age_up'    => 7,
        	],
        	[
        		'name'      => 'Junior',
        		'age_from'  => 8,
        		'age_up'    => 12,
        	],
        	[
        		'name'      => 'Teens',
        		'age_from'  => 12,
        		'age_up'    => 16,
        	],
        	[
        		'name'      => 'Pro',
        		'age_from'  => 16,
        	],
        ];

        foreach ($rows as $row) {
            StudentType::create($row);
        }
    }
}
