<?php

use App\Models\Role;
use Illuminate\Database\Seeder;

class TenantRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rows = [
            [
                'name'      => 'Admin',
                'special'   => 'all-access',
                'its_global'=> true
            ],
            [
                'name'      => 'Teacher',
                'its_global'=> true
            ],
            [
                'name'      =>  'Student',
                'its_global'=> true
            ],
            [
                'name'      =>  'Representative',
                'its_global'=> true
            ]
        ];

        foreach ($rows as $row) {
            $role = Role::create([
                'name' => $row['name'],
                'its_global' => isset($row['its_global']),
                'special' => isset($row['special'])
                    ? $row['special']
                    : null,
            ]);
        }
    }
}
