<?php

use App\Enums;
use App\Models\Tenant;
use Illuminate\Database\Seeder;

class TenantScheduleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rows = [
            [
                'start_time'    => '08:30:00',
                'end_time'      => '10:00:00',
            ],
            [
                'start_time'    => '10:30:00',
                'end_time'      => '12:00:00',
            ],
            [
                'start_time'    => '13:00:00',
                'end_time'      => '14:30:00',
            ],
            [
                'start_time'    => '15:00:00',
                'end_time'      => '16:30:00',
            ],
            [
                'start_time'    => '17:00:00',
                'end_time'      => '18:30:00',
            ],
        ];

        foreach (Tenant\Branch::get() as $branch) {
            foreach ($rows as $row) {
                Tenant\Schedule::create(array_merge($row, [
                    'branch_id' => $branch->id,
                    'type' => Enums\ScheduleType::LAP()
                ]));
            }
        }
    }
}
