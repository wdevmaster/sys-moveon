<?php

use App\Models\Country;
use Illuminate\Database\Seeder;

class CountriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries = [
            [
              "flag"=> "https=>//restcountries.eu/data/afg.svg",
              "name"=> "Afghanistan",
              "alpha2Code"=> "AF",
              "nativeName"=> "افغانستان"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/ala.svg",
              "name"=> "Åland Islands",
              "alpha2Code"=> "AX",
              "nativeName"=> "Åland"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/alb.svg",
              "name"=> "Albania",
              "alpha2Code"=> "AL",
              "nativeName"=> "Shqipëria"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/dza.svg",
              "name"=> "Algeria",
              "alpha2Code"=> "DZ",
              "nativeName"=> "الجزائر"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/asm.svg",
              "name"=> "American Samoa",
              "alpha2Code"=> "AS",
              "nativeName"=> "American Samoa"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/and.svg",
              "name"=> "Andorra",
              "alpha2Code"=> "AD",
              "nativeName"=> "Andorra"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/ago.svg",
              "name"=> "Angola",
              "alpha2Code"=> "AO",
              "nativeName"=> "Angola"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/aia.svg",
              "name"=> "Anguilla",
              "alpha2Code"=> "AI",
              "nativeName"=> "Anguilla"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/ata.svg",
              "name"=> "Antarctica",
              "alpha2Code"=> "AQ",
              "nativeName"=> "Antarctica"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/atg.svg",
              "name"=> "Antigua and Barbuda",
              "alpha2Code"=> "AG",
              "nativeName"=> "Antigua and Barbuda"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/arg.svg",
              "name"=> "Argentina",
              "alpha2Code"=> "AR",
              "nativeName"=> "Argentina"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/arm.svg",
              "name"=> "Armenia",
              "alpha2Code"=> "AM",
              "nativeName"=> "Հայաստան"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/abw.svg",
              "name"=> "Aruba",
              "alpha2Code"=> "AW",
              "nativeName"=> "Aruba"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/aus.svg",
              "name"=> "Australia",
              "alpha2Code"=> "AU",
              "nativeName"=> "Australia"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/aut.svg",
              "name"=> "Austria",
              "alpha2Code"=> "AT",
              "nativeName"=> "Österreich"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/aze.svg",
              "name"=> "Azerbaijan",
              "alpha2Code"=> "AZ",
              "nativeName"=> "Azərbaycan"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/bhs.svg",
              "name"=> "Bahamas",
              "alpha2Code"=> "BS",
              "nativeName"=> "Bahamas"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/bhr.svg",
              "name"=> "Bahrain",
              "alpha2Code"=> "BH",
              "nativeName"=> "‏البحرين"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/bgd.svg",
              "name"=> "Bangladesh",
              "alpha2Code"=> "BD",
              "nativeName"=> "Bangladesh"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/brb.svg",
              "name"=> "Barbados",
              "alpha2Code"=> "BB",
              "nativeName"=> "Barbados"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/blr.svg",
              "name"=> "Belarus",
              "alpha2Code"=> "BY",
              "nativeName"=> "Белару́сь"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/bel.svg",
              "name"=> "Belgium",
              "alpha2Code"=> "BE",
              "nativeName"=> "België"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/blz.svg",
              "name"=> "Belize",
              "alpha2Code"=> "BZ",
              "nativeName"=> "Belize"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/ben.svg",
              "name"=> "Benin",
              "alpha2Code"=> "BJ",
              "nativeName"=> "Bénin"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/bmu.svg",
              "name"=> "Bermuda",
              "alpha2Code"=> "BM",
              "nativeName"=> "Bermuda"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/btn.svg",
              "name"=> "Bhutan",
              "alpha2Code"=> "BT",
              "nativeName"=> "ʼbrug-yul"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/bol.svg",
              "name"=> "Bolivia (Plurinational State of)",
              "alpha2Code"=> "BO",
              "nativeName"=> "Bolivia"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/bes.svg",
              "name"=> "Bonaire, Sint Eustatius and Saba",
              "alpha2Code"=> "BQ",
              "nativeName"=> "Bonaire"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/bih.svg",
              "name"=> "Bosnia and Herzegovina",
              "alpha2Code"=> "BA",
              "nativeName"=> "Bosna i Hercegovina"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/bwa.svg",
              "name"=> "Botswana",
              "alpha2Code"=> "BW",
              "nativeName"=> "Botswana"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/bvt.svg",
              "name"=> "Bouvet Island",
              "alpha2Code"=> "BV",
              "nativeName"=> "Bouvetøya"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/bra.svg",
              "name"=> "Brazil",
              "alpha2Code"=> "BR",
              "nativeName"=> "Brasil"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/iot.svg",
              "name"=> "British Indian Ocean Territory",
              "alpha2Code"=> "IO",
              "nativeName"=> "British Indian Ocean Territory"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/umi.svg",
              "name"=> "United States Minor Outlying Islands",
              "alpha2Code"=> "UM",
              "nativeName"=> "United States Minor Outlying Islands"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/vgb.svg",
              "name"=> "Virgin Islands (British)",
              "alpha2Code"=> "VG",
              "nativeName"=> "British Virgin Islands"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/vir.svg",
              "name"=> "Virgin Islands (U.S.)",
              "alpha2Code"=> "VI",
              "nativeName"=> "Virgin Islands of the United States"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/brn.svg",
              "name"=> "Brunei Darussalam",
              "alpha2Code"=> "BN",
              "nativeName"=> "Negara Brunei Darussalam"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/bgr.svg",
              "name"=> "Bulgaria",
              "alpha2Code"=> "BG",
              "nativeName"=> "България"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/bfa.svg",
              "name"=> "Burkina Faso",
              "alpha2Code"=> "BF",
              "nativeName"=> "Burkina Faso"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/bdi.svg",
              "name"=> "Burundi",
              "alpha2Code"=> "BI",
              "nativeName"=> "Burundi"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/khm.svg",
              "name"=> "Cambodia",
              "alpha2Code"=> "KH",
              "nativeName"=> "Kâmpŭchéa"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/cmr.svg",
              "name"=> "Cameroon",
              "alpha2Code"=> "CM",
              "nativeName"=> "Cameroon"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/can.svg",
              "name"=> "Canada",
              "alpha2Code"=> "CA",
              "nativeName"=> "Canada"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/cpv.svg",
              "name"=> "Cabo Verde",
              "alpha2Code"=> "CV",
              "nativeName"=> "Cabo Verde"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/cym.svg",
              "name"=> "Cayman Islands",
              "alpha2Code"=> "KY",
              "nativeName"=> "Cayman Islands"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/caf.svg",
              "name"=> "Central African Republic",
              "alpha2Code"=> "CF",
              "nativeName"=> "Ködörösêse tî Bêafrîka"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/tcd.svg",
              "name"=> "Chad",
              "alpha2Code"=> "TD",
              "nativeName"=> "Tchad"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/chl.svg",
              "name"=> "Chile",
              "alpha2Code"=> "CL",
              "nativeName"=> "Chile"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/chn.svg",
              "name"=> "China",
              "alpha2Code"=> "CN",
              "nativeName"=> "中国"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/cxr.svg",
              "name"=> "Christmas Island",
              "alpha2Code"=> "CX",
              "nativeName"=> "Christmas Island"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/cck.svg",
              "name"=> "Cocos (Keeling) Islands",
              "alpha2Code"=> "CC",
              "nativeName"=> "Cocos (Keeling) Islands"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/col.svg",
              "name"=> "Colombia",
              "alpha2Code"=> "CO",
              "nativeName"=> "Colombia"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/com.svg",
              "name"=> "Comoros",
              "alpha2Code"=> "KM",
              "nativeName"=> "Komori"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/cog.svg",
              "name"=> "Congo",
              "alpha2Code"=> "CG",
              "nativeName"=> "République du Congo"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/cod.svg",
              "name"=> "Congo (Democratic Republic of the)",
              "alpha2Code"=> "CD",
              "nativeName"=> "République démocratique du Congo"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/cok.svg",
              "name"=> "Cook Islands",
              "alpha2Code"=> "CK",
              "nativeName"=> "Cook Islands"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/cri.svg",
              "name"=> "Costa Rica",
              "alpha2Code"=> "CR",
              "nativeName"=> "Costa Rica"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/hrv.svg",
              "name"=> "Croatia",
              "alpha2Code"=> "HR",
              "nativeName"=> "Hrvatska"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/cub.svg",
              "name"=> "Cuba",
              "alpha2Code"=> "CU",
              "nativeName"=> "Cuba"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/cuw.svg",
              "name"=> "Curaçao",
              "alpha2Code"=> "CW",
              "nativeName"=> "Curaçao"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/cyp.svg",
              "name"=> "Cyprus",
              "alpha2Code"=> "CY",
              "nativeName"=> "Κύπρος"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/cze.svg",
              "name"=> "Czech Republic",
              "alpha2Code"=> "CZ",
              "nativeName"=> "Česká republika"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/dnk.svg",
              "name"=> "Denmark",
              "alpha2Code"=> "DK",
              "nativeName"=> "Danmark"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/dji.svg",
              "name"=> "Djibouti",
              "alpha2Code"=> "DJ",
              "nativeName"=> "Djibouti"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/dma.svg",
              "name"=> "Dominica",
              "alpha2Code"=> "DM",
              "nativeName"=> "Dominica"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/dom.svg",
              "name"=> "Dominican Republic",
              "alpha2Code"=> "DO",
              "nativeName"=> "República Dominicana"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/ecu.svg",
              "name"=> "Ecuador",
              "alpha2Code"=> "EC",
              "nativeName"=> "Ecuador"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/egy.svg",
              "name"=> "Egypt",
              "alpha2Code"=> "EG",
              "nativeName"=> "مصر‎"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/slv.svg",
              "name"=> "El Salvador",
              "alpha2Code"=> "SV",
              "nativeName"=> "El Salvador"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/gnq.svg",
              "name"=> "Equatorial Guinea",
              "alpha2Code"=> "GQ",
              "nativeName"=> "Guinea Ecuatorial"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/eri.svg",
              "name"=> "Eritrea",
              "alpha2Code"=> "ER",
              "nativeName"=> "ኤርትራ"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/est.svg",
              "name"=> "Estonia",
              "alpha2Code"=> "EE",
              "nativeName"=> "Eesti"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/eth.svg",
              "name"=> "Ethiopia",
              "alpha2Code"=> "ET",
              "nativeName"=> "ኢትዮጵያ"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/flk.svg",
              "name"=> "Falkland Islands (Malvinas)",
              "alpha2Code"=> "FK",
              "nativeName"=> "Falkland Islands"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/fro.svg",
              "name"=> "Faroe Islands",
              "alpha2Code"=> "FO",
              "nativeName"=> "Føroyar"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/fji.svg",
              "name"=> "Fiji",
              "alpha2Code"=> "FJ",
              "nativeName"=> "Fiji"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/fin.svg",
              "name"=> "Finland",
              "alpha2Code"=> "FI",
              "nativeName"=> "Suomi"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/fra.svg",
              "name"=> "France",
              "alpha2Code"=> "FR",
              "nativeName"=> "France"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/guf.svg",
              "name"=> "French Guiana",
              "alpha2Code"=> "GF",
              "nativeName"=> "Guyane française"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/pyf.svg",
              "name"=> "French Polynesia",
              "alpha2Code"=> "PF",
              "nativeName"=> "Polynésie française"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/atf.svg",
              "name"=> "French Southern Territories",
              "alpha2Code"=> "TF",
              "nativeName"=> "Territoire des Terres australes et antarctiques françaises"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/gab.svg",
              "name"=> "Gabon",
              "alpha2Code"=> "GA",
              "nativeName"=> "Gabon"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/gmb.svg",
              "name"=> "Gambia",
              "alpha2Code"=> "GM",
              "nativeName"=> "Gambia"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/geo.svg",
              "name"=> "Georgia",
              "alpha2Code"=> "GE",
              "nativeName"=> "საქართველო"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/deu.svg",
              "name"=> "Germany",
              "alpha2Code"=> "DE",
              "nativeName"=> "Deutschland"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/gha.svg",
              "name"=> "Ghana",
              "alpha2Code"=> "GH",
              "nativeName"=> "Ghana"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/gib.svg",
              "name"=> "Gibraltar",
              "alpha2Code"=> "GI",
              "nativeName"=> "Gibraltar"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/grc.svg",
              "name"=> "Greece",
              "alpha2Code"=> "GR",
              "nativeName"=> "Ελλάδα"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/grl.svg",
              "name"=> "Greenland",
              "alpha2Code"=> "GL",
              "nativeName"=> "Kalaallit Nunaat"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/grd.svg",
              "name"=> "Grenada",
              "alpha2Code"=> "GD",
              "nativeName"=> "Grenada"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/glp.svg",
              "name"=> "Guadeloupe",
              "alpha2Code"=> "GP",
              "nativeName"=> "Guadeloupe"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/gum.svg",
              "name"=> "Guam",
              "alpha2Code"=> "GU",
              "nativeName"=> "Guam"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/gtm.svg",
              "name"=> "Guatemala",
              "alpha2Code"=> "GT",
              "nativeName"=> "Guatemala"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/ggy.svg",
              "name"=> "Guernsey",
              "alpha2Code"=> "GG",
              "nativeName"=> "Guernsey"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/gin.svg",
              "name"=> "Guinea",
              "alpha2Code"=> "GN",
              "nativeName"=> "Guinée"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/gnb.svg",
              "name"=> "Guinea-Bissau",
              "alpha2Code"=> "GW",
              "nativeName"=> "Guiné-Bissau"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/guy.svg",
              "name"=> "Guyana",
              "alpha2Code"=> "GY",
              "nativeName"=> "Guyana"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/hti.svg",
              "name"=> "Haiti",
              "alpha2Code"=> "HT",
              "nativeName"=> "Haïti"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/hmd.svg",
              "name"=> "Heard Island and McDonald Islands",
              "alpha2Code"=> "HM",
              "nativeName"=> "Heard Island and McDonald Islands"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/vat.svg",
              "name"=> "Holy See",
              "alpha2Code"=> "VA",
              "nativeName"=> "Sancta Sedes"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/hnd.svg",
              "name"=> "Honduras",
              "alpha2Code"=> "HN",
              "nativeName"=> "Honduras"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/hkg.svg",
              "name"=> "Hong Kong",
              "alpha2Code"=> "HK",
              "nativeName"=> "香港"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/hun.svg",
              "name"=> "Hungary",
              "alpha2Code"=> "HU",
              "nativeName"=> "Magyarország"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/isl.svg",
              "name"=> "Iceland",
              "alpha2Code"=> "IS",
              "nativeName"=> "Ísland"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/ind.svg",
              "name"=> "India",
              "alpha2Code"=> "IN",
              "nativeName"=> "भारत"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/idn.svg",
              "name"=> "Indonesia",
              "alpha2Code"=> "ID",
              "nativeName"=> "Indonesia"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/civ.svg",
              "name"=> "Côte d'Ivoire",
              "alpha2Code"=> "CI",
              "nativeName"=> "Côte d'Ivoire"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/irn.svg",
              "name"=> "Iran (Islamic Republic of)",
              "alpha2Code"=> "IR",
              "nativeName"=> "ایران"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/irq.svg",
              "name"=> "Iraq",
              "alpha2Code"=> "IQ",
              "nativeName"=> "العراق"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/irl.svg",
              "name"=> "Ireland",
              "alpha2Code"=> "IE",
              "nativeName"=> "Éire"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/imn.svg",
              "name"=> "Isle of Man",
              "alpha2Code"=> "IM",
              "nativeName"=> "Isle of Man"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/isr.svg",
              "name"=> "Israel",
              "alpha2Code"=> "IL",
              "nativeName"=> "יִשְׂרָאֵל"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/ita.svg",
              "name"=> "Italy",
              "alpha2Code"=> "IT",
              "nativeName"=> "Italia"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/jam.svg",
              "name"=> "Jamaica",
              "alpha2Code"=> "JM",
              "nativeName"=> "Jamaica"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/jpn.svg",
              "name"=> "Japan",
              "alpha2Code"=> "JP",
              "nativeName"=> "日本"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/jey.svg",
              "name"=> "Jersey",
              "alpha2Code"=> "JE",
              "nativeName"=> "Jersey"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/jor.svg",
              "name"=> "Jordan",
              "alpha2Code"=> "JO",
              "nativeName"=> "الأردن"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/kaz.svg",
              "name"=> "Kazakhstan",
              "alpha2Code"=> "KZ",
              "nativeName"=> "Қазақстан"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/ken.svg",
              "name"=> "Kenya",
              "alpha2Code"=> "KE",
              "nativeName"=> "Kenya"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/kir.svg",
              "name"=> "Kiribati",
              "alpha2Code"=> "KI",
              "nativeName"=> "Kiribati"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/kwt.svg",
              "name"=> "Kuwait",
              "alpha2Code"=> "KW",
              "nativeName"=> "الكويت"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/kgz.svg",
              "name"=> "Kyrgyzstan",
              "alpha2Code"=> "KG",
              "nativeName"=> "Кыргызстан"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/lao.svg",
              "name"=> "Lao People's Democratic Republic",
              "alpha2Code"=> "LA",
              "nativeName"=> "ສປປລາວ"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/lva.svg",
              "name"=> "Latvia",
              "alpha2Code"=> "LV",
              "nativeName"=> "Latvija"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/lbn.svg",
              "name"=> "Lebanon",
              "alpha2Code"=> "LB",
              "nativeName"=> "لبنان"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/lso.svg",
              "name"=> "Lesotho",
              "alpha2Code"=> "LS",
              "nativeName"=> "Lesotho"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/lbr.svg",
              "name"=> "Liberia",
              "alpha2Code"=> "LR",
              "nativeName"=> "Liberia"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/lby.svg",
              "name"=> "Libya",
              "alpha2Code"=> "LY",
              "nativeName"=> "‏ليبيا"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/lie.svg",
              "name"=> "Liechtenstein",
              "alpha2Code"=> "LI",
              "nativeName"=> "Liechtenstein"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/ltu.svg",
              "name"=> "Lithuania",
              "alpha2Code"=> "LT",
              "nativeName"=> "Lietuva"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/lux.svg",
              "name"=> "Luxembourg",
              "alpha2Code"=> "LU",
              "nativeName"=> "Luxembourg"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/mac.svg",
              "name"=> "Macao",
              "alpha2Code"=> "MO",
              "nativeName"=> "澳門"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/mkd.svg",
              "name"=> "Macedonia (the former Yugoslav Republic of)",
              "alpha2Code"=> "MK",
              "nativeName"=> "Македонија"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/mdg.svg",
              "name"=> "Madagascar",
              "alpha2Code"=> "MG",
              "nativeName"=> "Madagasikara"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/mwi.svg",
              "name"=> "Malawi",
              "alpha2Code"=> "MW",
              "nativeName"=> "Malawi"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/mys.svg",
              "name"=> "Malaysia",
              "alpha2Code"=> "MY",
              "nativeName"=> "Malaysia"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/mdv.svg",
              "name"=> "Maldives",
              "alpha2Code"=> "MV",
              "nativeName"=> "Maldives"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/mli.svg",
              "name"=> "Mali",
              "alpha2Code"=> "ML",
              "nativeName"=> "Mali"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/mlt.svg",
              "name"=> "Malta",
              "alpha2Code"=> "MT",
              "nativeName"=> "Malta"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/mhl.svg",
              "name"=> "Marshall Islands",
              "alpha2Code"=> "MH",
              "nativeName"=> "M̧ajeļ"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/mtq.svg",
              "name"=> "Martinique",
              "alpha2Code"=> "MQ",
              "nativeName"=> "Martinique"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/mrt.svg",
              "name"=> "Mauritania",
              "alpha2Code"=> "MR",
              "nativeName"=> "موريتانيا"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/mus.svg",
              "name"=> "Mauritius",
              "alpha2Code"=> "MU",
              "nativeName"=> "Maurice"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/myt.svg",
              "name"=> "Mayotte",
              "alpha2Code"=> "YT",
              "nativeName"=> "Mayotte"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/mex.svg",
              "name"=> "Mexico",
              "alpha2Code"=> "MX",
              "nativeName"=> "México"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/fsm.svg",
              "name"=> "Micronesia (Federated States of)",
              "alpha2Code"=> "FM",
              "nativeName"=> "Micronesia"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/mda.svg",
              "name"=> "Moldova (Republic of)",
              "alpha2Code"=> "MD",
              "nativeName"=> "Moldova"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/mco.svg",
              "name"=> "Monaco",
              "alpha2Code"=> "MC",
              "nativeName"=> "Monaco"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/mng.svg",
              "name"=> "Mongolia",
              "alpha2Code"=> "MN",
              "nativeName"=> "Монгол улс"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/mne.svg",
              "name"=> "Montenegro",
              "alpha2Code"=> "ME",
              "nativeName"=> "Црна Гора"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/msr.svg",
              "name"=> "Montserrat",
              "alpha2Code"=> "MS",
              "nativeName"=> "Montserrat"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/mar.svg",
              "name"=> "Morocco",
              "alpha2Code"=> "MA",
              "nativeName"=> "المغرب"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/moz.svg",
              "name"=> "Mozambique",
              "alpha2Code"=> "MZ",
              "nativeName"=> "Moçambique"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/mmr.svg",
              "name"=> "Myanmar",
              "alpha2Code"=> "MM",
              "nativeName"=> "Myanma"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/nam.svg",
              "name"=> "Namibia",
              "alpha2Code"=> "NA",
              "nativeName"=> "Namibia"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/nru.svg",
              "name"=> "Nauru",
              "alpha2Code"=> "NR",
              "nativeName"=> "Nauru"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/npl.svg",
              "name"=> "Nepal",
              "alpha2Code"=> "NP",
              "nativeName"=> "नेपाल"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/nld.svg",
              "name"=> "Netherlands",
              "alpha2Code"=> "NL",
              "nativeName"=> "Nederland"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/ncl.svg",
              "name"=> "New Caledonia",
              "alpha2Code"=> "NC",
              "nativeName"=> "Nouvelle-Calédonie"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/nzl.svg",
              "name"=> "New Zealand",
              "alpha2Code"=> "NZ",
              "nativeName"=> "New Zealand"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/nic.svg",
              "name"=> "Nicaragua",
              "alpha2Code"=> "NI",
              "nativeName"=> "Nicaragua"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/ner.svg",
              "name"=> "Niger",
              "alpha2Code"=> "NE",
              "nativeName"=> "Niger"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/nga.svg",
              "name"=> "Nigeria",
              "alpha2Code"=> "NG",
              "nativeName"=> "Nigeria"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/niu.svg",
              "name"=> "Niue",
              "alpha2Code"=> "NU",
              "nativeName"=> "Niuē"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/nfk.svg",
              "name"=> "Norfolk Island",
              "alpha2Code"=> "NF",
              "nativeName"=> "Norfolk Island"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/prk.svg",
              "name"=> "Korea (Democratic People's Republic of)",
              "alpha2Code"=> "KP",
              "nativeName"=> "북한"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/mnp.svg",
              "name"=> "Northern Mariana Islands",
              "alpha2Code"=> "MP",
              "nativeName"=> "Northern Mariana Islands"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/nor.svg",
              "name"=> "Norway",
              "alpha2Code"=> "NO",
              "nativeName"=> "Norge"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/omn.svg",
              "name"=> "Oman",
              "alpha2Code"=> "OM",
              "nativeName"=> "عمان"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/pak.svg",
              "name"=> "Pakistan",
              "alpha2Code"=> "PK",
              "nativeName"=> "Pakistan"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/plw.svg",
              "name"=> "Palau",
              "alpha2Code"=> "PW",
              "nativeName"=> "Palau"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/pse.svg",
              "name"=> "Palestine, State of",
              "alpha2Code"=> "PS",
              "nativeName"=> "فلسطين"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/pan.svg",
              "name"=> "Panama",
              "alpha2Code"=> "PA",
              "nativeName"=> "Panamá"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/png.svg",
              "name"=> "Papua New Guinea",
              "alpha2Code"=> "PG",
              "nativeName"=> "Papua Niugini"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/pry.svg",
              "name"=> "Paraguay",
              "alpha2Code"=> "PY",
              "nativeName"=> "Paraguay"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/per.svg",
              "name"=> "Peru",
              "alpha2Code"=> "PE",
              "nativeName"=> "Perú"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/phl.svg",
              "name"=> "Philippines",
              "alpha2Code"=> "PH",
              "nativeName"=> "Pilipinas"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/pcn.svg",
              "name"=> "Pitcairn",
              "alpha2Code"=> "PN",
              "nativeName"=> "Pitcairn Islands"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/pol.svg",
              "name"=> "Poland",
              "alpha2Code"=> "PL",
              "nativeName"=> "Polska"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/prt.svg",
              "name"=> "Portugal",
              "alpha2Code"=> "PT",
              "nativeName"=> "Portugal"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/pri.svg",
              "name"=> "Puerto Rico",
              "alpha2Code"=> "PR",
              "nativeName"=> "Puerto Rico"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/qat.svg",
              "name"=> "Qatar",
              "alpha2Code"=> "QA",
              "nativeName"=> "قطر"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/kos.svg",
              "name"=> "Republic of Kosovo",
              "alpha2Code"=> "XK",
              "nativeName"=> "Republika e Kosovës"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/reu.svg",
              "name"=> "Réunion",
              "alpha2Code"=> "RE",
              "nativeName"=> "La Réunion"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/rou.svg",
              "name"=> "Romania",
              "alpha2Code"=> "RO",
              "nativeName"=> "România"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/rus.svg",
              "name"=> "Russian Federation",
              "alpha2Code"=> "RU",
              "nativeName"=> "Россия"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/rwa.svg",
              "name"=> "Rwanda",
              "alpha2Code"=> "RW",
              "nativeName"=> "Rwanda"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/blm.svg",
              "name"=> "Saint Barthélemy",
              "alpha2Code"=> "BL",
              "nativeName"=> "Saint-Barthélemy"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/shn.svg",
              "name"=> "Saint Helena, Ascension and Tristan da Cunha",
              "alpha2Code"=> "SH",
              "nativeName"=> "Saint Helena"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/kna.svg",
              "name"=> "Saint Kitts and Nevis",
              "alpha2Code"=> "KN",
              "nativeName"=> "Saint Kitts and Nevis"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/lca.svg",
              "name"=> "Saint Lucia",
              "alpha2Code"=> "LC",
              "nativeName"=> "Saint Lucia"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/maf.svg",
              "name"=> "Saint Martin (French part)",
              "alpha2Code"=> "MF",
              "nativeName"=> "Saint-Martin"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/spm.svg",
              "name"=> "Saint Pierre and Miquelon",
              "alpha2Code"=> "PM",
              "nativeName"=> "Saint-Pierre-et-Miquelon"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/vct.svg",
              "name"=> "Saint Vincent and the Grenadines",
              "alpha2Code"=> "VC",
              "nativeName"=> "Saint Vincent and the Grenadines"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/wsm.svg",
              "name"=> "Samoa",
              "alpha2Code"=> "WS",
              "nativeName"=> "Samoa"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/smr.svg",
              "name"=> "San Marino",
              "alpha2Code"=> "SM",
              "nativeName"=> "San Marino"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/stp.svg",
              "name"=> "Sao Tome and Principe",
              "alpha2Code"=> "ST",
              "nativeName"=> "São Tomé e Príncipe"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/sau.svg",
              "name"=> "Saudi Arabia",
              "alpha2Code"=> "SA",
              "nativeName"=> "العربية السعودية"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/sen.svg",
              "name"=> "Senegal",
              "alpha2Code"=> "SN",
              "nativeName"=> "Sénégal"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/srb.svg",
              "name"=> "Serbia",
              "alpha2Code"=> "RS",
              "nativeName"=> "Србија"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/syc.svg",
              "name"=> "Seychelles",
              "alpha2Code"=> "SC",
              "nativeName"=> "Seychelles"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/sle.svg",
              "name"=> "Sierra Leone",
              "alpha2Code"=> "SL",
              "nativeName"=> "Sierra Leone"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/sgp.svg",
              "name"=> "Singapore",
              "alpha2Code"=> "SG",
              "nativeName"=> "Singapore"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/sxm.svg",
              "name"=> "Sint Maarten (Dutch part)",
              "alpha2Code"=> "SX",
              "nativeName"=> "Sint Maarten"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/svk.svg",
              "name"=> "Slovakia",
              "alpha2Code"=> "SK",
              "nativeName"=> "Slovensko"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/svn.svg",
              "name"=> "Slovenia",
              "alpha2Code"=> "SI",
              "nativeName"=> "Slovenija"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/slb.svg",
              "name"=> "Solomon Islands",
              "alpha2Code"=> "SB",
              "nativeName"=> "Solomon Islands"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/som.svg",
              "name"=> "Somalia",
              "alpha2Code"=> "SO",
              "nativeName"=> "Soomaaliya"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/zaf.svg",
              "name"=> "South Africa",
              "alpha2Code"=> "ZA",
              "nativeName"=> "South Africa"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/sgs.svg",
              "name"=> "South Georgia and the South Sandwich Islands",
              "alpha2Code"=> "GS",
              "nativeName"=> "South Georgia"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/kor.svg",
              "name"=> "Korea (Republic of)",
              "alpha2Code"=> "KR",
              "nativeName"=> "대한민국"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/ssd.svg",
              "name"=> "South Sudan",
              "alpha2Code"=> "SS",
              "nativeName"=> "South Sudan"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/esp.svg",
              "name"=> "Spain",
              "alpha2Code"=> "ES",
              "nativeName"=> "España"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/lka.svg",
              "name"=> "Sri Lanka",
              "alpha2Code"=> "LK",
              "nativeName"=> "śrī laṃkāva"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/sdn.svg",
              "name"=> "Sudan",
              "alpha2Code"=> "SD",
              "nativeName"=> "السودان"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/sur.svg",
              "name"=> "Suriname",
              "alpha2Code"=> "SR",
              "nativeName"=> "Suriname"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/sjm.svg",
              "name"=> "Svalbard and Jan Mayen",
              "alpha2Code"=> "SJ",
              "nativeName"=> "Svalbard og Jan Mayen"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/swz.svg",
              "name"=> "Swaziland",
              "alpha2Code"=> "SZ",
              "nativeName"=> "Swaziland"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/swe.svg",
              "name"=> "Sweden",
              "alpha2Code"=> "SE",
              "nativeName"=> "Sverige"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/che.svg",
              "name"=> "Switzerland",
              "alpha2Code"=> "CH",
              "nativeName"=> "Schweiz"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/syr.svg",
              "name"=> "Syrian Arab Republic",
              "alpha2Code"=> "SY",
              "nativeName"=> "سوريا"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/twn.svg",
              "name"=> "Taiwan",
              "alpha2Code"=> "TW",
              "nativeName"=> "臺灣"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/tjk.svg",
              "name"=> "Tajikistan",
              "alpha2Code"=> "TJ",
              "nativeName"=> "Тоҷикистон"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/tza.svg",
              "name"=> "Tanzania, United Republic of",
              "alpha2Code"=> "TZ",
              "nativeName"=> "Tanzania"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/tha.svg",
              "name"=> "Thailand",
              "alpha2Code"=> "TH",
              "nativeName"=> "ประเทศไทย"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/tls.svg",
              "name"=> "Timor-Leste",
              "alpha2Code"=> "TL",
              "nativeName"=> "Timor-Leste"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/tgo.svg",
              "name"=> "Togo",
              "alpha2Code"=> "TG",
              "nativeName"=> "Togo"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/tkl.svg",
              "name"=> "Tokelau",
              "alpha2Code"=> "TK",
              "nativeName"=> "Tokelau"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/ton.svg",
              "name"=> "Tonga",
              "alpha2Code"=> "TO",
              "nativeName"=> "Tonga"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/tto.svg",
              "name"=> "Trinidad and Tobago",
              "alpha2Code"=> "TT",
              "nativeName"=> "Trinidad and Tobago"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/tun.svg",
              "name"=> "Tunisia",
              "alpha2Code"=> "TN",
              "nativeName"=> "تونس"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/tur.svg",
              "name"=> "Turkey",
              "alpha2Code"=> "TR",
              "nativeName"=> "Türkiye"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/tkm.svg",
              "name"=> "Turkmenistan",
              "alpha2Code"=> "TM",
              "nativeName"=> "Türkmenistan"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/tca.svg",
              "name"=> "Turks and Caicos Islands",
              "alpha2Code"=> "TC",
              "nativeName"=> "Turks and Caicos Islands"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/tuv.svg",
              "name"=> "Tuvalu",
              "alpha2Code"=> "TV",
              "nativeName"=> "Tuvalu"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/uga.svg",
              "name"=> "Uganda",
              "alpha2Code"=> "UG",
              "nativeName"=> "Uganda"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/ukr.svg",
              "name"=> "Ukraine",
              "alpha2Code"=> "UA",
              "nativeName"=> "Україна"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/are.svg",
              "name"=> "United Arab Emirates",
              "alpha2Code"=> "AE",
              "nativeName"=> "دولة الإمارات العربية المتحدة"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/gbr.svg",
              "name"=> "United Kingdom of Great Britain and Northern Ireland",
              "alpha2Code"=> "GB",
              "nativeName"=> "United Kingdom"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/usa.svg",
              "name"=> "United States of America",
              "alpha2Code"=> "US",
              "nativeName"=> "United States"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/ury.svg",
              "name"=> "Uruguay",
              "alpha2Code"=> "UY",
              "nativeName"=> "Uruguay"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/uzb.svg",
              "name"=> "Uzbekistan",
              "alpha2Code"=> "UZ",
              "nativeName"=> "O‘zbekiston"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/vut.svg",
              "name"=> "Vanuatu",
              "alpha2Code"=> "VU",
              "nativeName"=> "Vanuatu"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/ven.svg",
              "name"=> "Venezuela (Bolivarian Republic of)",
              "alpha2Code"=> "VE",
              "nativeName"=> "Venezuela"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/vnm.svg",
              "name"=> "Viet Nam",
              "alpha2Code"=> "VN",
              "nativeName"=> "Việt Nam"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/wlf.svg",
              "name"=> "Wallis and Futuna",
              "alpha2Code"=> "WF",
              "nativeName"=> "Wallis et Futuna"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/esh.svg",
              "name"=> "Western Sahara",
              "alpha2Code"=> "EH",
              "nativeName"=> "الصحراء الغربية"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/yem.svg",
              "name"=> "Yemen",
              "alpha2Code"=> "YE",
              "nativeName"=> "اليَمَن"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/zmb.svg",
              "name"=> "Zambia",
              "alpha2Code"=> "ZM",
              "nativeName"=> "Zambia"
            ],
            [
              "flag"=> "https=>//restcountries.eu/data/zwe.svg",
              "name"=> "Zimbabwe",
              "alpha2Code"=> "ZW",
              "nativeName"=> "Zimbabwe"
            ]
        ];

        foreach ($countries as $country) {
            Country::create([
                'code' => strtolower($country['alpha2Code']),
                'name' => $country['name'],
                'native_name' => $country['nativeName'],
                'url_flag' => $country['flag'],
            ]);
        }
    }
}
