<?php

use Illuminate\Database\Seeder;

use App\Models\System;

class ProgramSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rows = [
            [ 'name' => 'Aleman' ],
            [ 'name' => 'Español' ],
            [ 'name' => 'Frances' ],
            [ 'name' => 'Ingles' ],
            [ 'name' => 'Italiano' ],
        ];
        $levels = ['I', 'II', 'III', 'IV', 'V'];



        foreach ($rows as $row) {
            $lang = System\Language::create([
                'name' => $row['name'],
                'slug' => \Str::slug($row['name'])
            ]);
            $program = $lang->programs()->create();

            for ($i=0; $i < 5; $i++)
                System\ProgramLevel::create([
                    'program_id' => $program->id,
                    'name' => $levels[$i]
                ]);
        }
    }
}
