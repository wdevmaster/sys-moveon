<?php

use App\Models\Permission;
use Illuminate\Database\Seeder;

class TenantPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rows = [
            'roles' => [
                'index',
                'create',
                'store',
                'edit',
                'update',
                'destroy'
            ],
            'employees' => [
                'index',
                'create',
                'store',
                'edit',
                'update',
                'destroy',
                'manager'
            ],
            'representatives' => [
                'index',
                'create',
                'store',
                'edit',
                'update',
                'destroy'
            ],
            'students' => [
                'index',
                'create',
                'store',
                'edit',
                'update',
                'destroy',
                'manager'
            ],
            'halls' => [
                'index',
                'create',
                'store',
                'edit',
                'update',
                'destroy',
                'manager'
            ],
            'lapses' => [
                'index',
                'update',
                'destroy',
            ]
        ];

        foreach ($rows as $key => $actions) {

            foreach ($actions as $action) {
                Permission::create([
                    'name' => "{$key} {$action}",
                    'slug' => "{$key}.{$action}"
                ]);
            }
        }
    }
}

