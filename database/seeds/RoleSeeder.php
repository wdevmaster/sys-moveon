<?php

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rows = [
            [
                'name'      => 'Root',
                'special'   => 'all-access',
            ],
            [
                'name'      => 'Admin',
            ]
        ];

        foreach ($rows as $row) {
            $role = Role::create([
                'name' => $row['name'],
                'special' => isset($row['special'])
                    ? $row['special']
                    : null
            ]);
        }
    }
}
