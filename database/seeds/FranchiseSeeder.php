<?php

use App\Models\User;
use App\Models\Role;
use App\Models\Tenant\City;
use App\Models\Tenant\Branch;
use App\Models\System\Program;
use App\Models\Tenant\Address;
use Illuminate\Database\Seeder;
use App\Models\System\Franchise;

class FranchiseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rows = [
            [
                'code' => 1010,
                'name' => 'Move On Academy C.A',
                'email' => 'tapias@email.com',
                'is_main' => true,
                'is_multibranch' => true,
                'users' => [
                    [
                        'username' => 'm0@',
                        'role' => 'admin',
                        'branches' => '*'
                    ]
                ],
                'branches' => [
                    [
                        'name' => 'Tapias',
                        'address' => [
                            'country_str' => 've',
                            'city_str' => 'Merida',
                            'address' => 'Avenida Andres bello. Centro comercial las tapias - piso 3. Local 41'
                        ],
                        'programs' => '*'
                    ],
                    [
                        'name' => 'Metroatletik',
                        'address' => [
                            'country_str' => 've',
                            'city_str' => 'Merida',
                            'address' => 'Av principal Zumba'
                        ],
                        'programs' => '*'
                    ],
                    [
                        'name' => 'E-MOA',
                        'is_online' => true,
                        'programs' => '*'
                    ]
                ]
            ],
            [
                'code' => 1212,
                'name' => 'Vigia - Move On Academy',
                'email' => 'moaelvigia@moveonacademy.com',
                'is_main' => true,
                'users' => [
                    [
                        'username' => 'l3tsg02021',
                        'role' => 'admin',
                        'branches' => '*'
                    ]
                ],
                'branches' => [
                    [
                        'name' => 'El Vigia',
                        'address' => [
                            'country_str' => 've',
                            'city_str' => 'Vigia',
                            'address' => 'Av principal Zumba'
                        ],
                        'programs' => '*'
                    ]
                ]
            ]
        ];

        foreach ($rows as $row) {
            $franchise = Franchise::create([
                'code' => $row['code'],
                'name' => $row['name'],
                'email' => $row['email'],
                'locked' => isset($row['locked']) ? $row['locked'] : false,
                'is_main' => isset($row['is_main']) ? $row['is_main'] : false,
                'is_multibranch' => isset($row['is_multibranch']) ? $row['is_multibranch'] : false,
            ]);

            \Tenancy::setTenant($franchise);

            if (isset($row['branches'])) {
                foreach ($row['branches'] as $data) {

                    $newAddress = null;
                    if (isset($data['address']) && $address = collect($data['address'])) {
                        $city = City::firstOrCreate(
                            ['code' => \Str::slug($address->get('city_str'))],
                            $address->only('country_str')
                                    ->merge([ 'name' => $address->get('city_str') ])
                                    ->toArray()
                        );

                        $newAddress = Address::create(
                            $address->only('country_str', 'address')
                                    ->merge([ 'city_str' => $city->code ])
                                    ->toArray()
                        );
                    }
                    $branch = Branch::create([
                        'name' => $data['name'],
                        'address_id' => isset($newAddress) ? $newAddress->id : null,
                        'is_online' => isset($data['is_online']),
                    ]);

                    if (isset($data['programs']) && $programs = $data['programs']) {
                        if (is_string($programs))
                            $programs = Program::pluck('id');

                        $branch->programs()->attach($programs);
                    }
                }
            }

            foreach ($row['users'] as $user) {
                $newUser = User::on('tenant')->create([
                    'username' => $user['username'],
                    'email' => isset($user['email']) ? $user['email'] : $row['email'],
                    'password' => bcrypt('secret')
                ]);

                $role = Role::on('tenant')->where('slug', $user['role'])->first();
                $newUser->roles()->attach($role->id);

                if (isset($user['branches']) && $branches = $user['branches']) {
                    if (is_string($branches))
                        $branches = Branch::pluck('id');

                    $newUser->branches()->attach(
                        $branches
                    );
                }
            }

            $this->call(TenantScheduleSeeder::class);
        }
    }
}
