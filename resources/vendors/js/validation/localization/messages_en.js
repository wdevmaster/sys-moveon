(function( factory ) {
	if ( typeof define === "function" && define.amd ) {
		define( ["jquery", "../jquery.validate"], factory );
	} else if (typeof module === "object" && module.exports) {
		module.exports = factory( require( "jquery" ) );
	} else {
		factory( jQuery );
	}
}(function( $ ) {

  /*
  * Translated default messages for the jQuery validation plugin.
  * Locale: ES (Spanish; Español)
  */
  $.extend( $.validator.messages, {
      required: "This field is required.",
      remote: "Please complete this field",
      email: "Please enter a valid email address.",
      url: "Please enter a valid URL.",
      date: "Please enter a valid date.",
      dateISO: "Please enter a valid (ISO) date.",
      number: "Please enter a valid number.",
      digits: "Please enter digits only.",
      creditcard: "Please enter a valid card number.",
      equalTo: "Please enter the same value again.",
      extension: "Please enter a value with an accepted extension.",
      maxlength: $ .validator.format ("Please enter no more than {0} characters."),
      minlength: $ .validator.format ("Please do not enter less than {0} characters."),
      rangelength: $ .validator.format ("Please enter a value between {0} and {1} characters."),
      range: $ .validator.format ("Please enter a value between {0} and {1}."),
      max: $ .validator.format ("Please enter a value less than or equal to {0}."),
      min: $ .validator.format ("Please enter a value greater than or equal to {0}."),
      nifES: "Please, write a valid NIF.",
      nieES: "Please write a valid NIE.",
      cifES: "Please enter a valid VAT number."
    });
    return $;
  })
);
