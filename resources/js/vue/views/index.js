import Vue from "vue";

var requireContext = require.context('./', true, /.*\.vue$/);
var Views = requireContext.keys().map(file => requireContext(file));

// Components that are registered globaly.
Views.forEach(View => {
  Vue.component(View.default.name, View.default)
});
