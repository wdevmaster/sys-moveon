import Vue from "vue";
import "~/views";

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: "#app",
})
