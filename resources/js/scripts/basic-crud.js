$('.table').DataTable(configTable);

$('.btn-delete').click(function(e) {
  let id = $(this).data('id');
  $(`#row-${id}`).toggleClass('table-primary');

  Swal.fire({
    title: lang.are_you_sure,
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#2ecc71',
    confirmButtonText: lang.yes_sure,
    cancelButtonText: lang.no_cancel,
    showLoaderOnConfirm: true,
    preConfirm: () => {
      return fetch(routeDelete.replace(':id', id), {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        method: "DELETE",
        body: JSON.stringify({
            '_token': csrfToken
        })
      }).catch(error => {
        Swal.showValidationMessage(
          `Request failed: ${error}`
        )
      })
    },
    allowOutsideClick: () => !Swal.isLoading()
  }).then((result) => {
    if (result.value && result.value.ok) {
      Swal.fire({
          title: lang.updated,
          type:'success',
          confirmButtonText: lang.ok,
      }).then(() => {
          window.location.href = routeIndex
      })
    } else {
      $('#row-'+id).toggleClass('table-primary')
      if ($(`#row-${id}`))
        $(`#row-${id}`).toggleClass('table-info')

      if (result.value)
        Swal.fire({
            position: 'top-end',
            type: 'error',
            title: lang.oops,
            text: result.value.statusText,
            showConfirmButton: false,
            timer: 2500
        })
    }
  });
});

$('.btn-restore').click(function(e) {
  let id = $(this).data('id')
  $(`#row-${id}`).toggleClass('table-primary')

  Swal.fire({
    title: lang.are_you_sure,
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#2ecc71',
    confirmButtonText: lang.yes_sure,
    cancelButtonText: lang.no_cancel,
    showLoaderOnConfirm: true,
    preConfirm: () => {
      return fetch(routeRestore.replace(':id', id), {
          headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
          },
          method: "PUT",
          body: JSON.stringify({
              '_token': csrfToken
          })
      }).catch(error => {
          Swal.showValidationMessage(
          `Request failed: ${error}`
          )
      })
    },
    allowOutsideClick: () => !Swal.isLoading()
  }).then((result) => {
    if (result.value && result.value.ok) {
      Swal.fire({
        title: lang.updated,
        type:'success',
        confirmButtonText: lang.ok,
      }).then(() => {
          window.location.href = routeIndex
      })
    } else {
      $('#row-'+id).toggleClass('table-primary')
      if ($(`#row-${id}`))
        $(`#row-${id}`).toggleClass('table-info')

      if (result.value)
        Swal.fire({
          position: 'top-end',
          type: 'error',
          title: lang.oops,
          text: result.value.statusText,
          showConfirmButton: false,
          timer: 2500
        })
    }
  })
})
