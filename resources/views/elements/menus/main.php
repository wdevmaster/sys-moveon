<?php

return [
    'options' => [
        [
            'url' => 'app/:code:/dashboard',
            'name' => __('Dashboard'),
            'icon' => 'feather icon-home'
        ],
    ]
];
