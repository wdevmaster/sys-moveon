<?php

return [
    'options' => [
        [
            'url' => 'app/:code:/dashboard',
            'name' => __('Dashboard'),
            'icon' => 'feather icon-home'
        ],

        [
            'url' => 'app/:code:/courses',
            'name' => __('Courses'),
            'icon' => 'feather icon-book',
            'permissions' => [
                'courses.index',
                'courses.create',
                'courses.edit',
                'courses.show',
                'courses.destroy'
            ],
        ],

        [
            'url' => 'app/:code:/payments',
            'name' => __('Payments'),
            'icon' => 'feather icon-layers',
            'permissions' => [
                'payments.index',
                'payments.create',
                'payments.edit',
                'payments.show',
                'payments.destroy'
            ],
        ],

        [
            'url' => '#',
            'name' => __('Schedules'),
            'icon' => 'feather icon-calendar',
            'submenu' => [
                [
                    'url' => 'app/:code:/schedules/teachers/table',
                    'name' => __('Teachers'),
                    'icon' => 'feather icon-circle',
                ],
                [
                    'url' => 'app/:code:/schedules/course/table',
                    'name' => __('Courses'),
                    'icon' => 'feather icon-circle',
                ]
            ],
            'permissions' => [
                'schedules.show',
            ],
        ],

        [
            'name' => __('Teachers'),
            'icon' => 'feather icon-bookmark',
            'slug' => 'teachers',
            'permissions' => [
                'teachers.index',
                'teachers.create',
                'teachers.edit',
                'teachers.show',
                'teachers.destroy'
            ],
            'submenu' => []
        ],

        [
            'name' => __('Student'),
            'icon' => 'feather icon-users',
            'slug' => 'students',
            'permissions' => [
                'students.index',
                'students.create',
                'students.edit',
                'students.show',
                'students.destroy'
            ],
            'submenu' => []
        ],

        [
            'url' => 'app/:code:/representatives',
            'name' => __('Representatives'),
            'icon' => 'feather icon-square',
            'permissions' => [
                'representatives.index',
                'representatives.create',
                'representatives.edit',
                'representatives.show',
                'representatives.destroy'
            ],
        ],

        [
            'navheader' => __('Settings'),
            'icon' => ''
        ],
        [
            'url' => '#',
            'name' => __('Employees'),
            'slug' => 'employees',
            'icon' => 'feather icon-slack',
            'submenu' => [],
            'permissions' => [
                'employees.index',
                'employees.create',
                'employees.edit',
                'employees.destroy'
            ]
        ],
        [
            'url' => '#',
            'name' => __('General'),
            'slug' => 'general',
            'icon' => 'feather icon-settings',
            'submenu' => [
                [
                    'url' => 'app/:code:/settings/branches',
                    'name' => __('Branches'),
                    'slug' => 'branches',
                    'icon' => 'feather icon-circle',
                    'permissions' => [
                        'branches.index',
                        'branches.create',
                        'branches.edit',
                        'branches.show',
                        'branches.destroy'
                    ]
                ],
                [
                    'url' => 'app/:code:/settings/halls',
                    'name' => __('Halls'),
                    'slug' => 'halls',
                    'icon' => 'feather icon-circle',
                    'permissions' => [
                        'halls.index',
                        'halls.create',
                        'halls.edit',
                        'halls.show',
                        'halls.destroy'
                    ]
                ],
                [
                    'url' => 'app/:code:/settings/lapses',
                    'name' => __('Lapses'),
                    'slug' => 'lapses',
                    'icon' => 'feather icon-circle',
                    'permissions' => [
                        'lapses.index',
                        'lapses.destroy'
                    ]
                ],
                [
                    'url' => 'app/:code:/settings/plans',
                    'name' => __('Payment plans'),
                    'slug' => 'plans',
                    'icon' => 'feather icon-circle',
                    'permissions' => [
                        'plans.index',
                        'plans.create',
                        'plans.edit',
                        'plans.show',
                        'plans.destroy'
                    ]
                ],
                [
                    'url' => 'app/:code:/settings/roles',
                    'name' => __('Roles and Permissions'),
                    'icon' => 'feather icon-circle',
                    'permissions' => [
                        'roles.index',
                        'roles.create',
                        'roles.edit',
                        'roles.destroy'
                    ]
                ],
            ]
        ]
    ],
];
