@if (request()->is('app/*/teachers*'))
  {{ Form::hidden('user[role]', 'teacher') }}
@elseif (request()->is('app/*/students*'))
  {{ Form::hidden('user[role]', 'student') }}
@else
<div class="col-sm-6 form-group">
  <label>{{ __('Role') }}</label>
  @if (request()->is('app/*/employees/*/create'))
    {{ Form::hidden('user[role]', $role) }}
  @endif
  {{
    Form::select(
      'user[role]',
      $roles->pluck('name', 'slug'),
      $role ?? null,
      [
        'class' => 'form-control'.($errors->has('role') ? ' is-invalid' : '' ),
        'placeholder' => __('Select :name', [ 'name' => __('Role') ]),
        request()->is('app/*/employees/*/create') ? 'disabled' : '',
        'required'
      ]
    )
  }}

  @error('role')
    <div class="invalid-feedback">
      <strong>{{ $message }}</strong>
    </div>
  @enderror
</div>
@endif
