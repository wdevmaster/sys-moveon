<div
  class="
    form-group
    {{
      request()->is('app/*/teachers/postulant/*') ||
      (request()->is('app/*/students/*') &&
      !request()->is('app/*/students/*/edit')) ||
      request()->is('app/*/representatives/*')
        ? 'col-sm-12'
        : ''
    }}
  "
>
  <label>{{ __('E-mail') }}</label>
  {{
    Form::email(
      'user[email]',
      old('user.email'),
      [
        'id' => 'user-name',
        'class' => 'form-control'.($errors->has('email') ? ' is-invalid' : '' ),
        'required',
      ]
    )
  }}
  @error('email')
    <div class="invalid-feedback">
      <strong>{{ $message }}</strong>
    </div>
  @enderror
</div>
