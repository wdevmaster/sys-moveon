<div class="col-sm-6 form-group">
  <label>{{ __('Contract data') }}</label>
  {{
    Form::text(
      'user[contract_start]',
      null,
      [
        'id' => 'contract_start',
        'class' => 'form-control pickadate',
        'placeholder' => __('Start'),
        'required',
        'autocomplete' => 'off'
      ]
    )
  }}
</div>

<div class="col-sm-6 form-group">
  <label></label>
  {{
    Form::text(
      'user[contract_end]',
      null,
      [
        'id' => 'contract_end',
        'class' => 'form-control pickadate',
        'placeholder' => __('End'),
        'autocomplete' => 'off',
        'disabled'
      ]
    )
  }}
</div>
