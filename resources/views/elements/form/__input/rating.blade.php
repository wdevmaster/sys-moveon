<div class="col-sm-12">
  <div class="table-responsive border rounded px-1 mb-1">
    <h6 class="border-bottom py-1 mx-1 mb-0 font-medium-2">
      Pts.
    </h6>
    <div class="form-group">
      <label>{{ __('General') }}</label>
      {{
        Form::text(
          'user[ranting][general]',
          old('user[ranting][general]'),
          [
            'class' => 'form-control input-ranting',
            'placeholder' => '00.00',
            'required'
          ]
        )
      }}
    </div>
  </div>
</div>

@section('page-script')
  @parent
  <!-- page__input-rating.js -->
  <script>
    $('.input-ranting').mask("000.00", {reverse: true});
  </script>
@endsection
