@if (!request()->is('app/*/teachers/postulant*'))
  <div class="form-group {{ request()->is('app/teachers*') ? 'col-sm-12' : '' }}">
    <label>{{ __('Username') }}</label>
    {{
      Form::text(
        'user[username]',
        old('user.username'),
        [
          'id' => 'user-name',
          'class' => 'form-control'.($errors->has('username') ? ' is-invalid' : '' ),
        ]
      )
    }}
    @error('username')
      <div class="invalid-feedback">
        <strong>{{ $message }}</strong>
      </div>
    @enderror
  </div>
@endif
