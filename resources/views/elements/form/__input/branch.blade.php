<div class="form-group {{ isset($col) ? $col : 'col-sm-6'}}">

  @if (
    request()->is('app/*/teachers/active*') &&
    $franchise->is_multibranch
  )
    <label>{{ __('Branches') }}</label>
    {{
      Form::select(
        'user[branches][]',
        $branches->pluck('name', 'id'),
        old('user.branches'),
        [
          'class' => 'form-control select2',
          'required',
          'multiple'
        ]
      )
    }}
  @else
    <label>{{ __('Branch') }}</label>
    @if (!$franchise->is_multibranch)
      {{ Form::hidden('user[branches][]', $branches->first()->id) }}
    @endif
    {{
      Form::select(
        'user[branches][]',
        $branches->pluck('name', 'id'),
        old('user.branches') ?? (!$franchise->is_multibranch ? $branches->first()->id : null),
        [
          'class' => 'form-control'.($errors->has('branch') ? ' is-invalid' : '' ),
          'placeholder' => __('Select :name', [ 'name' => __('Branch') ]),
          'required',
          !$franchise->is_multibranch ? 'disabled' : ''
        ]
      )
    }}
  @endif
</div>

@section('vendor-style')
  @parent
  <!-- page__input-branch.css -->
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/select/select2.min.css')) }}">
@endsection

@section('vendor-script')
  @parent
  <!-- vendor__input-branch.js -->
  <script src="{{ asset(mix('vendors/js/select/select2.full.min.js')) }}"></script>
@endsection

@section('page-script')
  @parent
  <!-- page__input-branch.js -->
  <script>
    $(".select2").select2({
      dropdownAutoWidth: true,
      width: '100%'
    });
  </script>
@endsection
