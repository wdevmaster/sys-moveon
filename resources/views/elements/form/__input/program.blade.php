<div class="form-group col-sm-12">
  <label>{{ __('Languages') }}</label>

  {{
    Form::select(
      'user[languages][]',
      $languages,
      old('user.languages'),
      [
        'class' => 'form-control select2',
        'required',
        'multiple'
      ]
    )
  }}
</div>

@section('vendor-style')
  @parent
  <!-- page__input-branch.css -->
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/select/select2.min.css')) }}">
@endsection

@section('vendor-script')
  @parent
  <!-- vendor__input-branch.js -->
  <script src="{{ asset(mix('vendors/js/select/select2.full.min.js')) }}"></script>
@endsection

@section('page-script')
  @parent
  <!-- page__input-branch.js -->
  <script>
    $(".select2").select2({
      dropdownAutoWidth: true,
      width: '100%'
    });
  </script>
@endsection
