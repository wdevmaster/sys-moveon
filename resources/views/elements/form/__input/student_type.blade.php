<div
  class="
    form-group
    {{
      request()->is('app/*/teachers*') ||
      request()->is('app/*/students*')
        ? 'col-sm-12'
        : 'col-sm-6'
    }}
  "
>
  <label>{{ __('Student types') }}</label>
  {{
    Form::select(
      'user[student][]',
      $studentType,
      old('user.student'),
      [
        'class' => 'form-control '.( !request()->is('app/*/students*') ? 'select2' : ''),
        'required',
        !request()->is('app/*/students*') ? 'multiple' : ''
      ]
    )
  }}
</div>

@section('vendor-style')
  @parent
  <!-- page__input-branch.css -->
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/select/select2.min.css')) }}">
@endsection

@section('vendor-script')
  @parent
  <!-- vendor__input-branch.js -->
  <script src="{{ asset(mix('vendors/js/select/select2.full.min.js')) }}"></script>
@endsection

@section('page-script')
  @parent
  <!-- page__input-branch.js -->
  <script>
    $(".select2").select2({
      dropdownAutoWidth: true,
      width: '100%'
    });
  </script>
@endsection
