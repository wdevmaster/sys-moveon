<div class="col-12 col-sm-2">
  <a class="mr-2 my-25 avatar" href="#">
    <img
      src="{{
        isset($data)
          ? $data->profile_photo
          : asset('images/portrait/small/avatar-default.svg')
      }}"
      alt="users avatar"
      id="render-avatar"
      class="users-avatar-shadow rounded"
      height="128"
      width="128"
    >
    <input
      type="file"
      class="avatar-file-input"
      name="user[files][avatar]"
      id="avatar"
    >
  </a>
</div>

@section('vendor-style')
  @parent
  <!-- avatar.css -->
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/datepicker/bootstrap-datepicker.min.css')) }}">
  <style>
    .avatar {
      width: 100%;
      overflow: hidden;
      position: relative;
      padding: 0;
    }

    .avatar .avatar-file-input {
      position: absolute;
      margin: 0;
      opacity: 0;
      top: 0;
      left: 0;
      height: 100%;
      height: 100%;
      cursor: pointer;
    }
  </style>
@endsection
@section('page-script')
  @parent
  <!-- avatar.js -->
  <script>
    $('#avatar').change(function () {
      var preview = document.getElementById('render-avatar')
      var file    = document.getElementById("avatar").files[0]
      var reader  = new FileReader();

      reader.onload = () => {
          preview.src = reader.result;
      }

      if (file.type.match('image'))
          reader.readAsDataURL(file);
      else {
          preview.val('')
      }
    })
  </script>
@endsection
