<div class="form-group {{ request()->is('app/teachers*') ? 'col-sm-12' : '' }}">
  <label>{{ __('Password') }}</label>
  {{
    Form::password(
      'user[password]',
      [
        'id' => 'user-password',
        'class' => 'form-control'.($errors->has('password') ? ' is-invalid' : '' ),
      ]
    )
  }}
  @error('password')
    <div class="invalid-feedback">
      <strong>{{ $message }}</strong>
    </div>
  @enderror
</div>
