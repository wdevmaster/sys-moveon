<div class="row">
  <div class="{{ isset($column) ? ( $group1 ?? 'col-md-3') : 'col-12' }}">
    <div class="form-group">
      <div class="controls">
      <label for="country_str">{{ __('Country') }}</label>
      {{
          Form::select(
            'address[country_str]',
            $countries,
            old('address.country_str'),
            [
                'id' => 'country_str',
                'class' => 'form-control',
                'placeholder' => __('Select :name', [ 'name' => __('country') ]),
                'required'
            ]
          )
      }}
      </div>
    </div>
  </div>

  <div class="{{ isset($column) ? ( $group2 ?? 'col-md-3') : 'col-12' }}">
    <div class="form-group">
      <div class="controls">
        <label for="city_str">{{ __('City') }}</label>
        {{
          Form::text(
            'address[city_str]',
            old('address.city_str'),
            [
                'id' => 'city_str',
                'class' => 'form-control',
                'required'
            ]
          )
        }}
      </div>
    </div>
  </div>
  <div class="{{ isset($column) ? ( $group3 ?? 'col-md-6') : 'col-12' }}">
    <div class="form-group">
      <div class="controls">
        <label for="address">{{ __('Address') }}</label>
        {{
          Form::text(
            'address[address]',
            old('address.address'),
            [
              'id' => 'address',
              'class' => 'form-control',
              'required'
            ]
          )
        }}
      </div>
    </div>
  </div>
</div>
