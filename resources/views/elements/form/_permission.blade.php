
<div class="table-responsive border rounded px-1 ">
  <div class="default-collapse collapse-bordered">
    <div class="card collapse-header">
      <div
        id="headingCollapse1"
        class="card-header"
        data-toggle="collapse"
        role="button"
        data-target="#collapse1"
        aria-expanded="false"
        aria-controls="collapse1"
      >
        <span class="lead collapse-title">
          <i class="feather icon-lock mr-50"></i>
          {{ $title }}
        </span>
      </div>
      <div
        id="collapse1"
        role="tabpanel"
        aria-labelledby="headingCollapse1"
        class="collapse {{ isset($show) ? 'show' : '' }}"
      >
        <div class="card-content">
          <div class="card-body">

            <table class="table table-borderless">
              <thead>
                <tr>
                  <th>{{ __('Module') }}</th>
                  <th>{{ __('All') }}</th>
                  <th>{{ __('Create') }}</th>
                  <th>{{ __('Edit') }}</th>
                  <th>{{ __('Read') }}</th>
                  <th>{{ __('Delete') }}</th>
                  <th>{{ __('Manager') }}</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($permissions as $module => $permission)
                  <tr>
                    <td>{{ __($module) }}</td>
                    <td>
                      <div class="custom-control custom-checkbox">
                        <input
                          type="checkbox"
                          id="all-{{ $module }}"
                          data-group="{{ $module }}"
                          class="custom-control-input all-permissions"
                        >
                        <label class="custom-control-label" for="all-{{ $module }}"></label>
                      </div>
                    </td>
                    @forelse ($permission as $item)
                      <td>
                        <div class="custom-control custom-checkbox">
                          {{
                            Form::checkbox(
                              "permissions[{$item->slug}]",
                              true,
                              isset($data) ? $data->hasPermissionTo($item->slug) : false,
                              [
                                'id' => $item->slug,
                                'class' => "custom-control-input {$module} check-permissions",
                                'data-group' => $module
                              ]
                            )
                          }}
                          <label class="custom-control-label" for="{{ $item->slug }}"></label>
                        </div>
                      </td>
                    @empty
                      <td></td>
                    @endforelse
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@section('page-script')
  @parent
  <!-- Page js files -->
  <script>
    $('.all-permissions').click(function () {
      let group = $(this).data('group');
      mapCheckbox($(`.${group}`), $(this).is(':checked'))
    });

    $('.check-permissions').click(function () {
      let group = $(this).data('group');

      if (!$(this).is(':checked'))
        $(`#all-${group}`).prop('checked', false)
    })

    function mapCheckbox(array, checked) {
      $.each(array, function (key, item) {
        $(item).prop('checked', checked)
      })
    }
  </script>
@endsection

