<div class="row">
  @if(!request()->is('app/register'))
    <div class="col-12 col-sm-2">
      <a class="mr-2 my-25 avatar" href="#">
        <img
          src="{{
            isset($data)
              ? $data->representative->profile_photo
              : asset('images/portrait/small/avatar-default.svg')
          }}"
          alt="users avatar"
          id="render-avatar"
          class="users-avatar-shadow rounded"
          height="128"
          width="128"
        >
      </a>
    </div>
  @endif
  <div class="{{ !request()->is('app/register') ? 'col-sm-10' : 'col-sm-12' }}">
    <div class="row">
      <div class="col-sm-6 form-group">
        <div class="controls">
          <label>{{ __('CI') }}</label>
          {{
            Form::text(
              'representative[ci]',
              old('representative.ci'),
              [
                'id' => 'ci',
                'class' => 'form-control'
              ]
            )
          }}
        </div>
      </div>
      <div class="col-sm-6 form-group">
        <div class="controls">
          <label>{{ __('RIF') }}</label>
          {{
            Form::text(
              'representative[rif]',
              old('representative.rif'),
              [
                'id' => 'rif',
                'class' => 'form-control'
              ]
            )
          }}
        </div>
      </div>

      <div class="col-sm-6 form-group">
        <div class="controls">
          <label>{{ __('Firstname') }}</label>
          {{
            Form::text(
              'representative[first_name]',
              old('representative.first_name'),
              [
                  'id' => 'firstname',
                  'class' => 'form-control',
                  'required'
              ]
            )
          }}
        </div>
      </div>

      <div class="col-sm-6 form-group">
        <div class="controls">
          <label>{{ __('Lastname') }}</label>
          {{
            Form::text(
              'representative[last_name]',
              old('representative.last_name'),
              [
                  'id' => 'lastname',
                  'class' => 'form-control',
                  'required'
              ]
            )
          }}
        </div>
      </div>

      <div class="col-sm-6 form-group">
        <div class="controls">
          <label>{{ __('Phone') }}</label>
          {{
            Form::text(
              'representative[phone]',
              old('representative.phone'),
              [
                'id' => 'phone',
                'class' => 'form-control phone',
                'placeholder' => '(00) 0000-0000',
                'required'
              ]
            )
          }}
        </div>
      </div>
      <div class="col-sm-6 form-group">
        <div class="controls">
          <label>{{ __('Emergency phone') }}</label>
          {{
            Form::text(
              'representative[emergency_phone]',
              old('representative.emergency_phone'),
              [
                'id' => 'emergency_phone',
                'class' => 'form-control phone',
                'placeholder' => '(00) 0000-0000',
                'required'
              ]
            )
          }}
        </div>
      </div>
    </div>
  </div>
</div>
