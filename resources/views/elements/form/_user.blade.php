<div class="row">
  @includeWhen(
    !request()->is('app/*/teachers*'),
    'elements.form.__input.avatar'
  )
  <div class="col-12 col-sm-{{ !request()->is('app/*/teachers*') ? '10' : '12' }}">
    <div class="row">
      @if (!request()->is('app/*/teachers/postulant*'))
        <div class="col-12 col-sm-6">
          <h5 class="mb-1 mt-2 mt-sm-0">
            <i class="feather icon-user mr-25"></i>
            {{ __('Account Information') }}
          </h5>

          @includeWhen(
            !request()->is('app/*/teachers/postulant*'),
            'elements.form.__input.username'
          )

          @includeWhen(
            !request()->is('app/*/teachers/postulant*'),
            'elements.form.__input.email'
          )

          @includeWhen(
            !request()->is('app/*/teachers/postulant*'),
            'elements.form.__input.password'
          )

        </div>
      @endif

      <div class="col-12 col-sm-6">
        <h5 class="mb-1 mt-2 mt-sm-0">
          <i class="feather icon-map-pin mr-25"></i>
          {{ __('Account settings') }}
        </h5>
        <div class="row">

          @includeWhen(request()->is(
            'app/*/teachers*'),
            'elements.form.__input.rating'
          )

          @include('elements.form.__input.role')

          @include('elements.form.__input.branch', ['col' => 'col-12'])

          @includeWhen(
            request()->is('app/*/teachers*')
            , 'elements.form.__input.program'
          )

          @includeWhen(
            request()->is('app/*/teachers*'),
            'elements.form.__input.student_type'
          )

          @includeWhen(
            !request()->is('app/*/teachers/postulant*') &&
            !request()->is('app/*/teachers/student*'),
            'elements.form.__input.contract'
          )

        </div>
      </div>

      @if (request()->is('app/*/teachers/postulant*'))
        <div class="col-12 col-sm-6">
          <h5 class="mb-1 mt-2 mt-sm-0">
            <i class="feather icon-message-square mr-25"></i>
            {{ __('Recommendations and observations') }}
          </h5>
          <x-form-log type="recomm" :select="$logTypes['recomm']"/>
        </div>
      @endif

    </div>
  </div>
  <hr>
  <!--
  <div class="col-12 py-3">
    @includeWhen(
      request()->is('app/employees*'),
      'elements.form._permission',
      ['title' => __('Additional permissions')]
    )
  </div>
  -->
</div>

@section('vendor-script')
  @parent
  <!-- vendor_user.js -->
  <script src="{{ asset(mix('vendors/js/datepicker/bootstrap-datepicker.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/datepicker/locales/'.App::getLocale().'.min.js')) }}"></script>
@endsection
@section('page-script')
  @parent
  <!-- page_user.js -->
  <script>
    $('.pickadate').datepicker({
      language: "{{ App::getLocale() }}",
      format: 'dd/mm/yyyy'
    });
  </script>
@endsection
