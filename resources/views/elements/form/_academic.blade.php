<div class="row">
  <div class="col-12">
    <div class="form-group">
      <div class="controls">
      <label for="country_str">{{ __('Level') }}</label>
      {{
          Form::select(
            'academic[type]',
            $inst_types,
            old('academic.type'),
            [
                'id' => 'institution-type',
                'class' => 'form-control',
                'placeholder' => __('Select :name', [ 'name' => '']),
                'required'
            ]
          )
      }}
      </div>
    </div>
  </div>

  <div class="col-12">
    <div class="form-group">
      <div class="controls" style="position: relative;">
        <label for="city_str">{{ __('Institución') }}</label>
        {{
          Form::text(
            'academic[institution]',
            old('academic.institution'),
            [
              'id' => 'institution',
              'class' => 'form-control',
              'autocomplete' => 'off',
              'required'
            ]
          )
        }}
      </div>
    </div>
  </div>
  <div class="col-12">
    <div class="form-group">
      <label class="ui-radio">
        {{
          Form::checkbox(
            "academic[is_graduated]",
            1,
            isset($data)
              ? ($data->academic ? $data->academic->is_graduated : false)
              : (old('academic.is_graduated') ?? true),
            [ 'id' => 'is_graduated']
          )
        }}
        <span class="input-span"></span> {{ __('I am currently studying') }}
      </label>
    </div>
  </div>
  <div
    id="show-academic"
    class="
      row col-12
      {{ isset($data) && $data->academic && !$data->academic->is_graduated ? '' : 'd-none' }}
    "
  >
    <div class="col-6">
      <div class="form-group">
        <label class="form-lable">{{ __('Title') }}</label>
        {{
          Form::select(
            "academic[title]",
            $titleType,
            old('academic.title'),
            [
              'id' => 'title',
              'class' => 'form-control',
              'placeholder' => __('Select :name', [ 'name' => __('Title') ]),
            ]
          )
        }}
      </div>
    </div>
    <div class="col-6">
      <div class="form-group">
        <label class="form-lable">{{ __('Draduate date') }}</label>
        {{
          Form::text(
            'academic[graduate_year]',
            old('academic.graduate_year'),
            [
              'id' => 'graduate_year',
              'class' => 'form-control pickadate',
              'autocomplete' => 'off',
              'placeholder' => '__/__/____',
            ]
          )
        }}
      </div>
    </div>
  </div>
</div>
@section('vendor-style')
  @parent
  <!-- vendor_academic.css -->
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/autocomplete/autocomplete.css')) }}">
@endsection
@section('vendor-script')
  @parent
  <!-- vendor_academic.js -->
  <script src="{{ asset(mix('vendors/js/autocomplete/autocomplete.js')) }}"></script>
@endsection
@section('page-script')
  @parent
  <!-- page_academic.js -->
  <script>
    $('#institution-type').change(function() {
        $('#institution').val('');
        let val = $(this).val();
        let institutions = filterInstitution(val);

        autocomplete(document.getElementById("institution"), institutions);
    })

    function filterInstitution(type) {
        let arry = [];
        let data = JSON.parse(`{!! $institutions->toJson() !!}`);
        data.filter(item => item.type == type).forEach(item => arry.push(item.name));

        return arry;
    }

    $('#is_graduated').change(function () {

      if(!$(this).is(':checked'))
          $('#show-academic').removeClass('d-none');
      else {
        $('select#title').prop('selectedIndex',0);
        $('input#graduate_year').val('');
        $('#show-academic').addClass('d-none');
      }
    })
  </script>
@endsection

