<div class="row">
  @if(request()->is('app/*/representatives*'))
  <div class="col-sm-6 form-group">
    <div class="controls">
      <label>{{ __('CI') }}</label>
      {{
        Form::text(
          'personalData[ci]',
          old('personalData.ci'),
          [
            'id' => 'ci',
            'class' => 'form-control'
          ]
        )
      }}
    </div>
  </div>
  <div class="col-sm-6 form-group">
    <div class="controls">
      <label>{{ __('RIF') }}</label>
      {{
        Form::text(
          'personalData[rif]',
          old('personalData.rif'),
          [
            'id' => 'rif',
            'class' => 'form-control'
          ]
        )
      }}
    </div>
  </div>
  @endif
  <div class="col-sm-6 form-group">
    <div class="controls">
      <label>{{ __('Birthdate') }}</label>
      {{
        Form::text(
          'personalData[birthdate]',
          old('personalData.birthdate'),
          [
            'id' => 'birthdate',
            'class' => 'form-control pickadate',
            'autocomplete' => 'off',
            'placeholder' => '__/__/____',
            'required'
          ]
        )
      }}
    </div>
  </div>
  <div class="col-sm-6 form-group">
    <div class="controls">
      <label>{{ __('Gender') }}</label>
      <ul class="list-unstyled mb-0">
        @foreach ($sexs as $key => $val)
        <li class="d-inline-block mr-2">
          <fieldset>
           <div class="vs-radio-con">
              {{  Form::radio("personalData[sex]", $key) }}
              <span class="vs-radio">
                <span class="vs-radio--border"></span>
                <span class="vs-radio--circle"></span>
              </span>
              <span class="">{{ $val }}</span>
            </div>
          </fieldset>
        </li>
        @endforeach
      </ul>
    </div>
  </div>

  <div class="col-sm-6 form-group">
    <div class="controls">
      <label>{{ __('Firstname') }}</label>
      {{
        Form::text(
          'personalData[first_name]',
          old('personalData.first_name'),
          [
              'id' => 'firstname',
              'class' => 'form-control',
              'required'
          ]
        )
      }}
    </div>
  </div>

  <div class="col-sm-6 form-group">
    <div class="controls">
      <label>{{ __('Lastname') }}</label>
      {{
        Form::text(
          'personalData[last_name]',
          old('personalData.last_name'),
          [
              'id' => 'lastname',
              'class' => 'form-control',
              'required'
          ]
        )
      }}
    </div>
  </div>
  @if(!request()->is('app/*/representatives*'))
  <div class="col-sm-6 form-group input-fci d-none">
    <div class="controls">
      <label>{{ __('CI') }}</label>
      {{
        Form::text(
          'personalData[ci]',
          isset($ci) ? $ci : old('personalData.ci'),
          [
            'id' => 'ci',
            'class' => 'form-control'
          ]
        )
      }}
    </div>
  </div>
  <div class="col-sm-6 form-group input-fci d-none">
    <div class="controls">
      <label>{{ __('RIF') }}</label>
      {{
        Form::text(
          'personalData[rif]',
          isset($rif) ? $rif : old('personalData.rif'),
          [
            'id' => 'rif',
            'class' => 'form-control'
          ]
        )
      }}
    </div>
  </div>
  @endif

  @includeWhen(
    request()->is('app/*/teachers/postulant*') ||
    (request()->is('app/*/students*') &&
    !request()->is('app/*/students/*/edit')) ||
    request()->is('app/*/representatives*'),
    'elements.form.__input.email'
  )

  <div class="col-sm-6 form-group">
    <div class="controls">
      <label>{{ __('Phone') }}</label>
      {{
        Form::text(
          'personalData[phone]',
          old('personalData.phone'),
          [
            'id' => 'phone',
            'class' => 'form-control phone',
            'placeholder' => '(00) 0000-0000',
            'required'
          ]
        )
      }}
    </div>
  </div>
  <div class="col-sm-6 form-group">
    <div class="controls">
      <label>{{ __('Emergency phone') }}</label>
      {{
        Form::text(
          'personalData[emergency_phone]',
          old('personalData.emergency_phone'),
          [
            'id' => 'emergency_phone',
            'class' => 'form-control phone',
            'placeholder' => '(00) 0000-0000',
            'required'
          ]
        )
      }}
    </div>
  </div>
</div>

@if (request()->has('student'))
  <input type="hidden" name="student" value="1">
  <input type="hidden" name="user_id">
@endif

@section('vendor-style')
  @parent
  <!-- vendor_perosnal.css -->
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/datepicker/bootstrap-datepicker.min.css')) }}">
@endsection
@section('vendor-script')
  @parent
  <!-- vendor_personal.js -->
  <script src="{{ asset(mix('vendors/js/datepicker/bootstrap-datepicker.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/datepicker/locales/'.App::getLocale().'.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/extensions/moment.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/mask/jquery.mask.min.js')) }}"></script>
@endsection
@section('page-script')
  @parent
  <!-- page_personal.js -->
  <script>
    $('.pickadate').datepicker({
        language: "{{ App::getLocale() }}",
        format: 'dd/mm/yyyy'
    });

    $('.pickadate').mask("00r00r0000", {
      translation: {
        'r': {
          pattern: /[\/]/,
          fallback: '/'
        },
        placeholder: "__/__/____"
      }
    });

    $('.phone').mask('(000) 000-0000');

    checkEga()
    $('#birthdate').change(() => {
      checkEga()
    })

    function checkEga() {
      if (getEga() < 18) {
          $('.input-fci').addClass('d-none')
      } else {
          $('.input-fci').removeClass('d-none')
      }
    }

    function getEga() {
        let birthdate = moment($('#birthdate').val(), "DD-MM-YYYY")
        if (!isNaN(birthdate))
            return moment().diff(birthdate, 'years',false);

        return 0
    }
  </script>
  @if (request()->has('student'))
    <script>
      $('#ci').change(function() {
        let url = "{{ tenant_route('api.representatives.get') }}";
        let ci = $(this).val();

        $.ajax({
          method: 'GET',
          url: `${url}?ci=${ci}`,
          beforeSend: function( xhr ) {
            $('#form input').attr('disabled', 'disabled')
            $('#form select').attr('disabled', 'disabled')
          }
        }).done(function (res) {
          $('#form input').removeAttr('disabled')
          $('#form select').removeAttr('disabled')

          if (!Array.isArray(res)) {
            $.each(res, function (key, values) {
              $.each(values, function (k, v) {

                if ($(`input[name="${key}[${k}]"][type="text"]`).length) {
                  $(`input[name="${key}[${k}]"]`).val(v)
                }else if ($(`input[name="${key}[${k}]"][type="email"]`).length) {
                  $(`input[name="${key}[${k}]"]`).val(v)
                }else if ($(`input[name="${key}[${k}]"][type="radio"]`).length)
                  $(`input[name="${key}[${k}]"][value="${v}"]`).prop('checked', true)
                else if ($(`select[name="${key}[${k}]"]`).length)
                  $(`select[name="${key}[${k}]"] option[value="${v}"]`).prop('selected', true)


              })
            })

            $('input[name=user_id]').val(res.user.id)
          }

        })
      })
    </script>
  @endif
@endsection
