<script>
  var lang = {
    are_you_sure : "{{__('Are you sure?')}}",
    yes_sure :"{{__('Yes, sure!')}}",
    no_cancel: "{{__('No, cancel!')}}",
    updated: "{{__('Updated!')}}",
    ok: "{{__('Ok')}}",
    oops: "{{ __('Oops...') }}"
  }
</script>
