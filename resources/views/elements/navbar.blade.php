@if($configData["mainLayoutType"] == 'horizontal' && isset($configData["mainLayoutType"]))
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu {{ $configData['navbarColor'] }} navbar-fixed">
  <div class="navbar-header d-xl-block d-none">
    <ul class="nav navbar-nav flex-row">
      <li class="nav-item"><a class="navbar-brand" href="dashboard-analytics">
          <div class="brand-logo"></div>
        </a></li>
    </ul>
  </div>
  @else
  <nav
    class="header-navbar navbar-expand-lg navbar navbar-with-menu {{ $configData['navbarClass'] }} navbar-light navbar-shadow {{ $configData['navbarColor'] }}">
    @endif
    <div class="navbar-wrapper">
      <div class="navbar-container content">
        <div class="navbar-collapse" id="navbar-mobile">
          <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center"></div>
          <ul class="nav navbar-nav float-right">
            <li class="dropdown dropdown-user nav-item">
              <a
                class="dropdown-toggle nav-link dropdown-user-link"
                href="#"
                data-toggle="dropdown"
              >
                <div class="user-nav d-sm-flex d-none">
                  <span class="user-name text-bold-600">{{ Auth::user()->fullname }}</span>
                  <span class="user-status">{{ Auth::user()->role->name }}</span>
                </div>
                <span>
                  <img class="round"
                    src="{{ Auth::user()->profile_photo }}" alt="avatar" height="40"
                    width="40"
                  />
                </span>
              </a>
              <div class="dropdown-menu dropdown-menu-right">
                @if (!Auth::user()->hasRole('admin'))
                  <a
                    class="dropdown-item"
                    href="{{ tenant_route('user.profile') }}"
                  >
                    <i class="feather icon-user"></i> Profile
                  </a>
                  <div class="dropdown-divider"></div>
                @endif
                <a
                  class="dropdown-item"
                  href="{{ tenant_route('logout') }}"
                  onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                >
                  <i class="feather icon-power"></i> Logout
                  <form
                    id="logout-form"
                    action="{{ tenant_route('logout') }}"
                    method="POST"
                    style="display: none;"
                >
                    @csrf
                  </form>
                </a>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </nav>
