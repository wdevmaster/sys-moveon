<div class="dropdown m-r-5">
  <button
    class="btn btn-sm btn-light dropdown-toggle mr-1"
    type="button"
    id="dropdownMenu"
    data-toggle="dropdown"
    aria-haspopup="true"
    aria-expanded="false"
    data-toggle="tooltip"
    data-original-title="{{__('Options')}}"
  >
    <i class="feather icon-more-vertical"></i>
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenu">
    @if($type->value() != 'postulant')
      @if($type->value() != 'retired')
        <a
          class="dropdown-item btn-dropdown"
          href="javascript:;"
          data-id="{{ $row->hid }}"
          data-model="user"
          data-is_active="{{ $row->is_active ? 0 : 1 }}"
        >
          {{ $row->is_active ? __('Disabled') : __('Available') }}
        </a>
      @endif
      <a
        class="dropdown-item btn-dropdown"
        href="javascript:;"
        data-id="{{ $row->hid }}"
        data-model="staff"
        data-type="{{ $row->is_active ? 'retired' : 'active' }}"
      >
        {{ $row->is_active ? __('Retired') : __('Active') }}
      </a>
    @else
      @if ($row->status->is_onhold)
      <a
        class="dropdown-item btn-dropdown"
        href="javascript:;"
        data-id="{{ $row->hid }}"
        data-model="staff"
        data-status="{{ $teacherStatus::EXAM }}"
      >
        {{ $teacherStatus::EXAM() }}
      @elseif($row->status->is_exam)
      <a
        class="dropdown-item btn-dropdown"
        href="javascript:;"
        data-id="{{ $row->hid }}"
        data-model="staff"
        data-status="{{ $teacherStatus::MINICLASS }}"
      >
        {{ $teacherStatus::MINICLASS() }}
      @elseif($row->status->is_miniclass)
      <a
        class="dropdown-item btn-dropdown"
        href="javascript:;"
        data-id="{{ $row->hid }}"
        data-model="staff"
        data-status="{{ $teacherStatus::INTERVIEW }}"
      >
        {{ $teacherStatus::INTERVIEW() }}
      @elseif($row->status->is_interview)
      <a
        class="dropdown-item btn-dropdown"
        href="javascript:;"
        data-id="{{ $row->hid }}"
        data-model="staff"
        data-status="{{ $teacherStatus::JOURNEYS }}"
      >
        {{ $teacherStatus::JOURNEYS() }}
      @elseif($row->status->is_journeys)
      <a
        class="dropdown-item btn-dropdown"
        href="javascript:;"
        data-id="{{ $row->hid }}"
        data-model="staff"
        data-status="{{ $teacherStatus::APPROVED }}"
      >
        {{ $teacherStatus::APPROVED() }}
      @endif
      </a>
      @if(!$row->status->is_approved)
        @if(!$row->status->is_rejected)
        <a
          class="dropdown-item btn-dropdown"
          href="javascript:;"
          data-id="{{ $row->hid }}"
          data-model="staff"
          data-status="{{ $teacherStatus::REJECTED }}"
        >
          {{ $teacherStatus::REJECTED() }}
        @else
        <a
          class="dropdown-item btn-dropdown"
          href="javascript:;"
          data-id="{{ $row->hid }}"
          data-model="staff"
          data-status="{{ $teacherStatus::ONHOLD }}"
        >
          {{ __('Reintegrate') }}
        @endif
      @else
        <a
        class="dropdown-item btn-dropdown"
        href="javascript:;"
        data-id="{{ $row->hid }}"
        data-model="staff"
        data-type="active"
      >
        {{ __('Active') }}
      </a>
      @endif
      </a>
    @endif
  </div>
</div>
@section('page-script')
  @parent
  @include('elements.langJs')
  <script>
    let redirectUrl = "{{tenant_route('teachers.index', [ 'type' => ':type'])}}";
    let managerUrl = "{{ tenant_route('teachers.manager') }}";
    let token = '{{csrf_token()}}';
    let type = "{{ $type->value() }}";
  </script>
  <script src="{{ asset(mix('js/scripts/manager-row.js')) }}"></script>
@endsection
