@php
  $configData = Helper::applClasses();
@endphp
<div
  class="main-menu menu-fixed {{($configData['theme'] === 'light') ? "menu-light" : "menu-dark"}} menu-accordion menu-shadow"
  data-scroll-to-active="true"
>
  <div class="navbar-header">
      <ul class="nav navbar-nav flex-row">
          <li class="nav-item mr-auto">
              <a class="navbar-brand" href="dashboard-analytics">
                <div class="brand-logo"></div>
                <h2 class="brand-text mb-0">{{ config('app.name') }}</h2>
              </a>
          </li>
          <li class="nav-item nav-toggle">
              <a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse">
              <i class="feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon"></i>
              <i
                class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block primary collapse-toggle-icon"
                data-ticon="icon-disc"
              ></i>
              </a>
          </li>
      </ul>
  </div>
  <div class="shadow-bottom"></div>
  <div class="main-menu-content">
    <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
      {{-- Foreach menu item starts --}}
      @foreach(Helper::getMenu()->options as $option)
        @if($option)
          @if(isset($option->navheader))
            <li class="navigation-header">
                <span>{{ $option->navheader }}</span>
            </li>
          @else
            {{-- Add Custom Class with nav-item --}}
            @php
              $custom_classes = "";
              if(isset($option->classlist)) {
                $custom_classes = $option->classlist;
              }
              $translation = "";

              if(isset($option->i18n)){
                $translation = $option->i18n;
              }
            @endphp
            @can(isset($option->permissions) ? $option->permissions : [])
            <li class="nav-item {{ (isset($option->url) && request()->is($option->url.'**')) ? 'active' : '' }} {{ $custom_classes }}">
              <a href="{{ isset($option->url) ? url($option->url) : '#' }}">
                <i class="{{ $option->icon }}"></i>
                <span class="menu-title">{{ $option->name }}</span>
                @if (isset($option->badge))
                  <@php
                    $badgeClasses = "badge badge-pill badge-primary float-right"
                  @endphp
                  <span
                    class="{{
                        isset($option->badgeClass) ? $option->badgeClass.' test' : $badgeClasses.' notTest'
                    }}"
                  >
                    {{$option->badge}}
                  </span>
                @endif
              </a>
              @if(isset($option->submenu))
                @include('elements.submenu', ['menu' => $option->submenu])
              @endif
            </li>
            @endcan
          @endif
        @endif
      @endforeach
      {{-- Foreach menu item ends --}}
    </ul>
  </div>
</div>
<!-- END: Main Menu-->
