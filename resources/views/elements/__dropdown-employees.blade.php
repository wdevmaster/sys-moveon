<div class="dropdown m-r-5">
  <button
    class="btn btn-sm btn-light dropdown-toggle mr-1"
    type="button"
    id="dropdownMenu"
    data-toggle="dropdown"
    aria-haspopup="true"
    aria-expanded="false"
  >
    <i class="feather icon-more-vertical"></i>
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenu">
    @if($type != 'retired')
      <a
        class="dropdown-item btn-dropdown"
        href="javascript:;"
        data-id="{{ $row->hid }}"
        data-model="user"
        data-is_active="{{ !$row->is_active }}"
      >
        {{ $row->is_active ? __('Disabled') : __('Available') }}
      </a>
    @endif
    <a
      class="dropdown-item btn-dropdown"
      href="javascript:;"
      data-id="{{ $row->hid }}"
      data-model="staff"
      data-type="{{ $row->is_active ? 'retired' : 'active' }}"
    >
      {{ $row->is_active ? __('Retired') : __('Active') }}
    </a>
  </div>
</div>
