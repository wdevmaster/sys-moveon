<?php

return [

    'tenant' => include(base_path('resources/views/elements/menus/tenant.php')),

    'system' => include(base_path('resources/views/elements/menus/system.php')),

    'main' => include(base_path('resources/views/elements/menus/main.php'))
];
