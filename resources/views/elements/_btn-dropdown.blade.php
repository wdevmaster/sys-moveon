<!--
@includeWhen(request()->is('app/*/employees/*'), 'elements.__dropdown-employees', ['some' => 'data'])
-->
@includeWhen(request()->is('app/*/teachers/*'), 'elements.__dropdown-teacher')

@includeWhen(request()->is('app/*/students/*'), 'elements.__dropdown-student')

@includeWhen(request()->is('app/*/settings/halls*'), 'elements.__dropdown-halls')
