<div class="dropdown m-r-5">
  <button
    class="btn btn-sm btn-light dropdown-toggle mr-1"
    type="button"
    id="dropdownMenu"
    data-toggle="dropdown"
    aria-haspopup="true"
    aria-expanded="false"
    data-toggle="tooltip"
    data-original-title="{{__('Options')}}"
  >
    <i class="feather icon-more-vertical"></i>
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenu">
    @if($status->value() == 'active')
      <a
        class="dropdown-item btn-dropdown"
        href="javascript:;"
        data-id="{{ $row->hid }}"
        data-model="user"
        data-is_active="{{ $row->is_active ? 0 : 1 }}"
      >
        {{ $row->is_active ? __('Disabled') : __('Available') }}
      </a>
    @endif
    <a
      class="dropdown-item btn-dropdown"
      href="javascript:;"
      data-id="{{ $row->hid }}"
      data-model="enrollment"
      data-type="{{ $row->enrollment->status->is_active ? 'retired' : 'active' }}"
      data-status="{{ $row->enrollment->status->is_active ? 'retired' : 'active' }}"
    >
      {{ $row->enrollment->status->is_active ? __('Retired') : __('Active') }}
    </a>
  </div>
</div>
@section('page-script')
  @parent
  @include('elements.langJs')
  <script>
    let redirectUrl = "{{tenant_route('students.index', [ 'status' => ':type'])}}";
    let managerUrl = "{{ tenant_route('students.manager',['status' =>  $status->value()]) }}";
    let token = '{{csrf_token()}}';
    let type = "{{ $status->value() }}";
  </script>
  <script src="{{ asset(mix('js/scripts/manager-row.js')) }}"></script>
@endsection
