<div class="dropdown m-r-5">
  <button
    class="btn btn-sm btn-light dropdown-toggle mr-1"
    type="button"
    id="dropdownMenu"
    data-toggle="dropdown"
    aria-haspopup="true"
    aria-expanded="false"
  >
    <i class="feather icon-more-vertical"></i>
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenu">
    <a
      class="dropdown-item btn-dropdown"
      href="javascript:;"
      data-id="{{ $row->hid }}"
      data-model="hall"
      data-type="manager"
      data-status="{{ $row->status->is_available ? 'disable' : 'available' }}"
    >
      {{ $row->status->is_available ? __('Disabled') : __('Available') }}
    </a>
  </div>
</div>
@section('page-script')
  @parent
  @include('elements.langJs')
  <script>
    let redirectUrl = "{{tenant_route('halls.index')}}";
    let managerUrl = "{{ tenant_route('halls.manager') }}";
    let token = '{{csrf_token()}}';
  </script>
  <script src="{{ asset(mix('js/scripts/manager-row.js')) }}"></script>
@endsection

