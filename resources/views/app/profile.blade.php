@extends('layouts/default')

@section('title', __('Profile'))

@section('content')
<section id="basic-datatable">
  <div class="row">
    <div class="col-sm-12">
      <div class="card overflow-hidden">
        <div class="card-content">
          <div class="card-body">
            {!!
              Form::model(Auth::user(),
                [
                  'url' => tenant_route('user.sync'),
                  'method' => 'PUT',
                  'id' => 'form',
                  'files' => true
                ]
              )
            !!}
              {{ Form::hidden('user[role]', Auth::user()->role->slug) }}
              <div class="row pt-3">
                @include('elements.form.__input.avatar')
                <div class="col-sm-10">
                  <div class="row">
                    <div class="col-12 col-sm-7">
                      <h5 class="mb-1 mt-2 mt-sm-0">
                        <i class="feather icon-user mr-25"></i>
                        {{ __('Personal Information') }}
                      </h5>
                      @include('elements.form._personal')
                    </div>
                    <div class="col-12 col-sm-5">
                      <h5 class="mb-1 mt-2 mt-sm-0">
                        <i class="feather icon-map-pin mr-25"></i>
                        {{ __('Address') }}
                      </h5>
                      @include('elements.form._address')
                    </div>
                  </div>
                </div>
              </div>
              <div class="row pt-3">
                <div class="col-sm-2"></div>
                <div class="col-sm-10">
                  <div class="row">
                    @if (!Auth::user()->hasRole('representative'))
                    <div class="col-12 col-sm-7">
                      <h5 class="mb-1 mt-2 mt-sm-0">
                        <i class="feather icon-target mr-25"></i>
                        {{ __('Academic Information') }}
                      </h5>
                      @include('elements.form._academic')
                    </div>
                    @else
                      <div class="col-sm-7"></div>
                    @endif
                    <div class="col-12 col-sm-5">
                      <h5 class="mb-1 mt-2 mt-sm-0">
                        <i class="feather icon-map-pin mr-25"></i>
                        {{ __('Account settings') }}
                      </h5>
                      <div class="row">
                        <div class="form-group col-sm-12">
                          <label>{{ __('Username') }}</label>
                          {{
                            Form::text(
                              'user[username]',
                              old('user.username'),
                              [
                                'id' => 'user-name',
                                'class' => 'form-control'.($errors->has('username') ? ' is-invalid' : '' ),
                              ]
                            )
                          }}
                          @error('username')
                            <div class="invalid-feedback">
                              <strong>{{ $message }}</strong>
                            </div>
                          @enderror
                        </div>
                        <div class="form-group col-sm-12">
                          <label>{{ __('E-mail') }}</label>
                          {{
                            Form::email(
                              'user[email]',
                              old('user.email'),
                              [
                                'id' => 'user-name',
                                'class' => 'form-control'.($errors->has('email') ? ' is-invalid' : '' ),
                                'required',
                              ]
                            )
                          }}
                          @error('email')
                            <div class="invalid-feedback">
                              <strong>{{ $message }}</strong>
                            </div>
                          @enderror
                        </div>
                        <div class="form-group col-sm-12">
                          <label>{{ __('Password') }}</label>
                          {{
                            Form::password(
                              'user[password]',
                              [
                                'id' => 'user-password',
                                'class' => 'form-control'.($errors->has('password') ? ' is-invalid' : '' ),
                                Hash::check('password', Auth::user()->password) ? 'required' : ''
                              ]
                            )
                          }}
                          @error('password')
                            <div class="invalid-feedback">
                              <strong>{{ $message }}</strong>
                            </div>
                          @enderror
                          @if (Hash::check('password', Auth::user()->password))
                              <div class="invalid-feedback" style="display: block; color: rgb(255, 159, 67);">
                              <strong>{{ __('You need to change your password') }}</strong>
                            </div>
                          @endif
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <hr>
              <div class="form-group text-right gruop-btn">
                <button class="btn btn-success">{{ __('Save') }}</button>
              </div>
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('vendor-script')
  <!-- vendor files -->
  <script src="{{ asset(mix('vendors/js/validation/jquery.validate.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/validation/localization/messages_'.\App::getLocale().'.js')) }}"></script>
@endsection

@section('page-script')
  @parent
  <!-- Page js files -->
  <script src="{{ asset(mix('js/scripts/validate-form.js')) }}"></script>
@endsection


