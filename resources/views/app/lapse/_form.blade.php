<div class="card">
  <div class="card-content">
    <div class="card-header">
      <h4 class="card-title">
        <span id="action-title">{{ __('Add New') }}</span> {{ __('Lapse') }}
      </h4>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="form-group col-sm-12">
          @if (!$franchise->is_multibranch)
            <label>{{ __('Branch') }}</label>
            {{ Form::hidden('branch_id', $branches->first()->id, ['id' => 'branch_id']) }}
          @else
            <label>{{ __('Branches') }}</label>
          @endif
          {{
            Form::select(
              'branches[]',
              $branches->pluck('name', 'id'),
              old('branches') ?? (!$franchise->is_multibranch ? $branches->first()->id : null),
              [
                'id' => $franchise->is_multibranch ? 'branch_id' : '',
                'class' => 'form-control select2'.($errors->has('branch') ? ' is-invalid' : '' ),
                'required',
                'multiple',
                !$franchise->is_multibranch ? 'disabled' : ''
              ]
            )
          }}
        </div>


        <div class="form-group col-sm-6">
          <label>{{ __('Start time') }}</label>
          {!! Form::text('date', null, ['class' => 'form-control timepicker', 'id' => 'start_time']) !!}
        </div>

        <div class="form-group col-sm-6">
          <label>{{ __('End time') }}</label>
          {!! Form::text('date', null, ['class' => 'form-control timepicker', 'id' => 'end_time']) !!}
        </div>
      </div>
      @if(!request()->is('**/show'))
        <hr>
        <div class="form-group text-right">
          <button id="add-lapse" type="button" class="btn btn-success">{{ __('Add') }}</button>
        </div>
      @endif
    </div>
  </div>
</div>

@section('page-style')
  @parent
  <!-- page__input-branch.css -->
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/select/select2.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/timepicker/bootstrap-timepicker.min.css')) }}">
  <style>
    .select2-selection--multiple.is-invalid{
      border-color: #ea5455 !important;
    }
  </style>
@endsection

@section('vendor-script')
  @parent
  <!-- vendor__input-branch.js -->
  <script src="{{ asset(mix('vendors/js/select/select2.full.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/timepicker/bootstrap-timepicker.min.js')) }}"></script>
@endsection

@section('page-script')
  @parent
  <!-- page__input-branch.js -->
  <script>
    $(function() {
      initInputTime();

      $('#start_time').change(function() {
          initInputTime();
      });

      $('#add-lapse').click(function() {
        pushArray()
      });
    });

    $(".select2").select2({
      dropdownAutoWidth: true,
      width: '100%'
    });

    $('.timepicker').timepicker({
      //use24hours: true,
      format: 'HH:mm'
    });

    function initInputTime() {
      let start =  moment($('#start_time').val(), 'h:mm A')
      let end = moment($('#start_time').val(), 'h:mm A').add(90, 'minutes')

      $('#end_time').val(end.format('h:mm A'))
    }

    function pushArray() {
      let arry = []
      let val = $('#branch_id').val()

      if (!validateSelectBranch(val))
        return;

      if (typeof val === 'object') {
        $.each(val, function(i, id) {
          arry.push({
            branch_id: id,
            start_time: moment.utc($('#start_time').val(), 'h:mm A'),
            end_time: moment.utc($('#end_time').val(), 'h:mm A'),
          })
        });
      } else {
        arry.push({
          branch_id: val,
          start_time: moment.utc($('#start_time').val(), 'h:mm A'),
          end_time: moment.utc($('#end_time').val(), 'h:mm A'),
        })
      }

      mergeData(arry);

      renderTable();
      formatInputJson();
    }

    function validateSelectBranch(val) {
      if (typeof val === 'object' && val.length == 0) {
        $('.select2-selection--multiple').addClass('is-invalid')
        return false;
      }

      return true;
    }

    function formatInputJson() {
        let input = []

        for (let index = 0; index < data.length; index++) {
            let obj = {
                branch_id: data[index].branch_id,
                start_time: data[index].start_time.format('HH:mm'),
                end_time: data[index].end_time.format('HH:mm')
            }

            if (data[index].id)
                obj['id'] = data[index].id

            input.push(obj)
        }
        $('input[name=data]').val(JSON.stringify(input))
    }

    function changeInputTime() {
      initInputTime();
      //renderValidateTime(start, end)
    }

    function mergeData(rows) {
      let newData = data;

      for (let index = 0; index < rows.length; index++) {
        const row = rows[index];

        if (whereIsSame(row.branch_id, row.start_time, row.end_time) == -1)
          data.push(row);
      }

      return newData;
    }

    function whereIsSame(_id, start, end) {

      return data
              .filter(row => row.branch_id == _id)
              .findIndex(
                dat =>
                  (start.isSame(dat.start_time) || end.isSame(dat.end_time)) ||
                  (
                    start.isBetween(dat.start_time, dat.end_time) ||
                    end.isBetween(dat.start_time, dat.end_time)
                  )
              );
    }
  </script>
@endsection

