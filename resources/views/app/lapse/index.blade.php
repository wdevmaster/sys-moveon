
@extends('layouts/default')

@section('title', __('Lapses'))

@section('vendor-style')
  {{-- vendor css files --}}
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/animate/animate.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
@endsection

@section('content')
<section id="basic-datatable">
    <div class="row">
      <div class="col-4">
        @include('app.lapse._form')
      </div>
      <div class="col-8">
        <div class="card">
          <div class="card-content">
            <div class="card-body card-dashboard">
              @include('app.lapse._table')
            </div>
          </div>
        </div>
      </div>
    </div>
</section>
@endsection
@section('vendor-script')
{{-- vendor files --}}
  <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/extensions/polyfill.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/extensions/moment.min.js')) }}"></script>
@endsection
@section('page-script')
  @parent
  <script>
    let data = JSON.parse($('input[name=data]').val());
    const branches = {!! $branches->pluck('name', 'id')->toJson() !!};
  </script>
@endsection
