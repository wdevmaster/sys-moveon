{!! Form::open(['url' => tenant_route('lapses.sync'), 'id' => 'form']) !!}
    <input type="hidden" name="data" value="{{ old('data') ?? $rows->toJson() }}">

    <div class="content-table-lapses">
        <table class="table table-striped table-bordered">
            <thead class="thead-lapse">
                <tr class="bg-success text-white">
                </tr>
            </thead>
            <tbody class="tbody-lapse">
                <tr>
                    <td>{{ __('No data available in table') }}</td>
                </tr>
            </tbody>
        </table>
    </div>
    <hr>
    <div id="form-btn" class="form-group text-right">
        <button class="btn btn-success">{{ __('Save') }}</button>
    </div>
{!! Form::close() !!}

@section('page-style')
  @parent
  <style>
    .content-table-lapses {
      overflow: hidden;
      overflow-x: auto;
    }

    .content-table-lapses .table th,
    .content-table-lapses .table td {
      text-align: center;
    }

    .content-table-lapses .list-group .list-group-item {
      border: 1px solid rgba(0,0,0,.125);
    }

    .content-table-lapses .list-group-item  .btn {
      padding: 0.5rem .9rem !important;
    }

  </style>
@endsection

@section('page-script')
  @parent
  <script>
    let urlDelete = "{{ tenant_route('lapses.destroy', ':id') }}";

    $(function() {
      initRederTable();

      $(document).on('click', '.btn-list-item', function () {
        let indx = $(this).data('index')
        deleteRow(indx)
      });
    });

    function initRederTable() {
      $.each(data, function (key, val) {
        data[key].start_time = moment.utc(val.start_time)
        data[key].end_time = moment.utc(val.end_time)
      });

      renderTable()
    }

    function renderTable() {
      $('.thead-lapse th').remove()
      $('.tbody-lapse tr').remove()
      $('.tbody-lapse').append(`<tr></tr>`)

      if (data.length == 0) {
        let msg = "{{ __('No data available in table') }}"
        $(`.tbody-lapse tr`).append(`<td>${msg}</td>`);
        $('#form-btn').css('display', 'none');
        return;
      } else
        $('#form-btn').css('display', 'block');

      $.each(orderBy(data), function(index, row) {
        if ($(`th#branch_${row.branch_id}`).length == 0) {
          $('.thead-lapse tr').append(`<th id="branch_${row.branch_id}">
            ${branches[row.branch_id]}
          </th>`);

          $(`.tbody-lapse tr`).append(`<td>
            <ul id="column_${row.branch_id}" class="list-group">
            </ul>
          </td>`);
        }

        $(`ul#column_${row.branch_id}`).append(`
          <li id="item${index}" class="list-group-item">
            ${row.start_time.format('h:mm A')}
            -
            ${row.end_time.format('h:mm A')}
            <button type="button"
                data-index="${index}"
                class="btn btn-danger btn-sm btn-list-item">x</button>
          </li>
        `)
      })

      changeInputTime()
    }

    function orderBy(arry) {
      let temp = [];

      $.each(arry, function (i, v) {
        let k = parseInt(v.branch_id);
        let kk = parseInt(v.start_time.format('HH'));

        if (!temp[k])
          temp[k] = [];

        temp[k][kk] = v;

      });

      arry = [];
      $.each(temp, function (i, v) {
        if (v) {
          $.each(v, function (ii, val) {
            if (val)
              arry.push(val);
          })
        }
      })

      data = arry;
      formatInputJson();

      return arry;
    }

    function deleteRow(indx) {
      if (data[indx].id) {
        let id = data[indx].id

        deleteSchedule(id)
          .then(res => {
            if (res.ok) {
              data = data.unset(indx)
              renderTable()
              formatInputJson()
            }
          }).catch(error => {
            Swal.showValidationMessage(
              `Request failed: ${error}`
            )
          })
      } else {
        data = data.unset(indx);
        renderTable()
        formatInputJson()
      }
    }

    async function deleteSchedule(id) {
      let response = await fetch(urlDelete.replace(':id', id), {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        method: "DELETE",
        body: JSON.stringify({
          '_token': '{{csrf_token()}}'
        })
      })

      return await response
    }

    Array.prototype.unset = function (index) {
      let tmp = [];

      $.each(this, function (key, val) {
        if (key != index)
          tmp.push(val);
      })

      return tmp
    }
  </script>
@endsection
