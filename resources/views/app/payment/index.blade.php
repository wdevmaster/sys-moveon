
@extends('layouts/default')

@section('title', __('Payment plans'))

@section('btn-action')
  @can('plans.create')
    <a
      href="{{ tenant_route('payments.create') }}"
      class="btn btn-outline-primary"
    >
      <span>
        <i class="feather icon-plus"></i> {{ __('Add New') }}
      </span>
    </a>
  @endcan
@endsection

@section('content')
<section id="basic-datatable">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-content">
          <div class="card-body card-dashboard">
            <div class="tab-content pt-2">
              @include('app.payment._table', ['rows' => $rows])
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('vendor-style')
  {{-- vendor css files --}}
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/animate/animate.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
@endsection
@section('vendor-script')
  {{-- vendor files --}}
  <script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/extensions/polyfill.min.js')) }}"></script>
@endsection
@section('page-script')
  @parent
  @include('elements.langJs')
  <script>
    var routeDelete = "{{ tenant_route('plans.destroy', [':id']) }}";
    var routeRestore = "{{ tenant_route('plans.restore', [':id']) }}";
    var routeIndex = "{{ tenant_route('plans.index') }}";
    var csrfToken = "{{csrf_token()}}";
    var configTable = {
      ordering: false,
      columnDefs: [{
        targets: 'sort',
        orderable: true,
      }],
      language: {
        url: "{{ asset('json/datatable/'.App::getLocale().'.json') }}"
      }
    }
  </script>
  <script src="{{ asset(mix('js/scripts/basic-crud.js')) }}"></script>
@endsection
