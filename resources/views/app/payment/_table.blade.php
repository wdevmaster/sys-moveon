<div class="table-responsive">
  <table class="table">
    <thead>
        <tr>
          <th>{{ __('Date') }}</th>
          <th>{{ __('Code') }}</th>
          <th>{{ __('Course') }}</th>
          <th>{{ __('Student') }}</th>
          <th>{{ __('Amount') }}</th>
          <th>{{ __('Status') }}</th>
        </tr>
    </thead>
    <tbody>
      @foreach($rows as $key => $data)
        @php
          $row = $data->first();
        @endphp
        <tr id="row-{{ $key }}">
          <td>{{ $row->created_at->toFormattedDateString()}}</td>
          <td>
            <a href="{{ tenant_route('payments.show', [$row->code]) }}">
              {{ $row->code }}
            </a>
          </td>
          <td>
            {{ $row->coursable->course->name }} {{ $row->coursable->course->level->name }}
          </td>
          <td>{{ $row->user->fullname }}</td>
          <td>{{ number_format($data->sum('amount'), 2) }}</td>
          <td>
            <span
              class="
                badge
                badge-{{ $row->status->is_approved ? 'success' : ( !$row->status->is_rejected ? 'warning' : 'danger') }}
              "
            >
              {{ $row->status }}
            </span>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
</div>
