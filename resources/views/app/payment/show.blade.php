@extends('layouts/default')

@section('title', __('Payment').': '.$data->first()->code)

@php
  $row = $data->first();
@endphp

@section('btn-action')
  @if (!$row->status->is_approved)
    @can('payments.manager')
      <button
        class="btn btn-primary btn-manager"
        data-id="{{ $row->code }}"
        data-status="{{ $status[3] }}"
      >
        <span>
          <i class="feather icon-icon-check"></i> {{ __('Verify payment ') }}
        </span>
      </button>
    @endcan
  @endif
@endsection

@section('content')
<section id="basic-datatable">
  <div class="row">
    <div class="col-7">
      <div class="card">
        <div class="card-content">
          <div class="card-body card-dashboard">
            <h4>
              {{ $row->coursable->course->name }} {{ $row->coursable->course->level->name }}
              <span
                class="
                  badge
                  badge-{{ $row->status->is_approved ? 'success' : ( !$row->status->is_rejected ? 'warning' : 'danger') }}
                "
                style="float: right;"
              >
                {{ $row->status }}
              </span>
              <br>
              <small>
                <b>{{ __('Teacher') }}:</b> {{ $row->coursable->user->fullname }}
              </small>
            </h4>
            <hr>
            <div class="row">
              <div class="col-6">
                <h5>
                  {{ __('Payment') }}
                </h5>
                <p>
                  <b>{{ __('Payment method') }}:</b> {{ $row->payment_method }} <br>
                  @if ($row->payment_method->is_wiretransfer || $row->payment_method->is_apptransfer)
                    <b>{{ __('Bank') }}:</b> {{ $row->bank->name }} <br>
                  @endif
                  <b>{{ __('Amount') }}:</b> {{ number_format($data->sum('amount'), 2) }}<br>
                  <b>
                    {{ __('Voucher') }}:
                    <a href="{{ asset($row->Voucher) }}" target="_blank" rel="noopener noreferrer">{{ __('Show') }}</a>
                  </b>
                </p>
              </div>

              @if ($row->user->personalData->age < 18)
                @php
                  $parent = $row->user->parent->first();
                @endphp
                <div class="col-6">
                  <h5>
                    {{ __('Representative') }}
                  </h5>
                  <p>
                    <i class="feather icon-user"></i> {{ $parent->fullname }} <br>
                    <i class="feather icon-map-pin"></i> {{ $parent->personalData->address->fullAddress }}
                  </p>
                  <p>
                    <i class="feather icon-mail"></i> {{ $parent->email }} <br>
                    <i class="feather icon-phone"></i> {{ $parent->personalData->phone }}
                  </p>
                </div>
              @endif

              <div class="col-6">
                <h5>
                  {{ __('Student') }}
                </h5>
                <p>
                  <i class="feather icon-user"></i> {{ $row->user->fullname }} <br>
                  <i class="feather icon-map-pin"></i> {{ $row->user->personalData->address->fullAddress }}
                </p>
                <p>
                  <i class="feather icon-mail"></i> {{ $row->user->email }} <br>
                  <i class="feather icon-phone"></i> {{ $row->user->personalData->phone }}
                </p>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-5">
      <div class="card">
        <div class="card-content">
          <div class="card-body card-dashboard">
            <table class="table">
              <thead>
                <tr class="table-secondary">
                  <th colspan="2">
                    <h5 class="mb-0">
                      <b>{{ __('Payment details') }}</b>
                    </h5>
                  </th>
                </tr>
              </thead>
              <tbody>
                @foreach ($data as $item)
                  <tr>
                    <td class="w-70">
                      @if (!$plan->with_inscription)
                        {{ __('Dues') }}
                        <b><span id="dues">{{ $item->dues+1 }}</span></b>
                      @else
                        {{ $item->is_inscription ? __('Inscription') : __('Dues') }}
                        @if (!$item->is_inscription)
                          <b><span id="dues">{{ $item->dues }}</span></b>
                        @endif
                      @endif
                    </td>
                    <td class="text-right p-0 pr-1" style="width: 80px;">
                      {{ number_format($item->amount, 2) }}
                    </td>
                  </tr>
                @endforeach
              </tbody>
              <tfoot>
                <tr>
                  <td class="w-70 text-right pr-1">
                    <h5 class="mb-0">
                      <b>{{ __('Total') }}</b>
                    </h5>
                  </td>
                  <td class="text-right pr-1" style="width: 80px;">
                    <h5 class="mb-0">
                      <b>{{ number_format($data->sum('amount'), 2) }}</b>
                    </h5>
                  </td>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('vendor-style')
  {{-- vendor css files --}}
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
@endsection
@section('page-style')
  @parent
  <!-- Page css files -->
  <style>
    .table td {
      border-color: #d9d9d9 !important;
    }
  </style>
@endsection
@section('vendor-script')
  @parent
  <!-- vendor files -->
  <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
@endsection

@section('page-script')
  @parent
  @include('elements.langJs')
  <script>
    let redirectUrl = "{{tenant_route('payments.show', [$row->code])}}";
    let managerUrl = "{{ tenant_route('payments.manager', [$row->code]) }}";
    let token = '{{csrf_token()}}';

    $('.btn-manager').click(function() {
      let data = $(this).data()
      data['_token'] = token

      Swal.fire({
        title: lang.are_you_sure,
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#2ecc71',
        confirmButtonText: lang.yes_sure,
        cancelButtonText: lang.no_cancel,
        showLoaderOnConfirm: true,
        inputAttributes: {
          autocapitalize: 'off'
        },
        preConfirm: () => {
          return fetch(managerUrl, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "PUT",
            body: JSON.stringify(data)
          }).catch(error => {
            Swal.showValidationMessage(
              `Request failed: ${error}`
            )
          })
        },
        allowOutsideClick: () => !Swal.isLoading()
      }).then((result) => {
        if (result.value && result.value.ok) {
            Swal.fire({
              title: lang.updated,
              type:'success',
              confirmButtonText: lang.ok,
            }).then(() => {
              window.location.href = redirectUrl
            })
        } else {
            if (result.value)
                Swal.fire({
                  position: 'top-end',
                  type: 'error',
                  title: lang.oops,
                  text: result.value.statusText,
                  showConfirmButton: false,
                  timer: 2500
                })
        }
      })
    });
  </script>
@endsection

