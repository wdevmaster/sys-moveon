<div class="row">
  <div class="form-group col-sm-6">
    @if (!$franchise->is_multibranch)
      <label>{{ __('Branch') }}</label>
      {{ Form::hidden('branch_id', $branches->first()->id, ['id' => 'branch_id']) }}
    @else
      <label>{{ __('Branches') }}</label>
    @endif
    {{
      Form::select(
        'branches',
        $branches->pluck('name', 'id'),
        old('branches') ?? (!$franchise->is_multibranch ? $branches->first()->id : null),
        [
          'id' => $franchise->is_multibranch ? 'branch_id' : '',
          'class' => 'form-control select2'.($errors->has('branch') ? ' is-invalid' : '' ),
          'required',
          !$franchise->is_multibranch ? 'disabled' : ''
        ]
      )
    }}
  </div>

  <div class="form-group col-sm-6">
    <label>{{ __('Course') }}</label>
    {{
      Form::select(
        'coursable_id',
        [],
        old("coursable_id"),
        [
          'id' => 'coursable_id',
          'class' => 'form-control',
          'placeholder' => __('Select :name', [ 'name' => '']),
          'required',
        ]
      )
    }}
  </div>

  <hr>

  <div class="col-12">
    <table id="list-course" class="table table-striped table-bordered">
      <thead>
        <tr>
          <th scope="col">{{__('CI')}}</th>
          <th scope="col">{{__('Name')}}</th>
          <th scope="col">{{ __('Payment plan') }}</th>
        </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
    <input type="hidden" name="user_id" id="user_id">
    <input type="hidden" name="plan_id" id="plan_id">
  </div>

</div>

@section('vendor-style')
  @parent
  {{-- vendor css files --}}
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
@endsection
@section('vendor-script')
  @parent
  {{-- vendor files --}}
  <script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/extensions/polyfill.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/extensions/axios.min.js')) }}"></script>
@endsection

@section('page-script')
  @parent
  <script>
    let plans = {!! $plans->toJson() !!}
    let fullplans = {!! $fullplans->toJson() !!}
    getCourse();

    $('#branch_id').change(function () {
      getCourse();
    });

    $('#coursable_id').change(function () {
      listCourseTable.ajax.url(`${urlCourseStudent}${$('#coursable_id').val()}`).load();
    });

    function getCourse() {
      let url = "{{ tenant_route('courses.api.index', [':id']) }}";
      let id = $('#branch_id').val();
      let options = [];

      ajaxData(url.replace(':id', id)).then(function (data) {
        $.each(data, function (key, val) {
          options.push({
            value: key,
            text: val
          })
        });
        renderOptionSelect($('#coursable_id'), options)
      });
    }

    function ajaxData(url) {
      return $.ajax({
        url: url
      });
    }

    function renderOptionSelect(select, options) {
      $(select).children('option').remove()
      $(select).append(`
        <option>{{ __('Select :name', [ 'name' => '...' ]) }}</option>
      `);

      for (let index = 0; index < options.length; index++) {
          const option = options[index];
          $(select).append(`
            <option value="${option.value}">${option.text}</option>
          `)
      }
    }

    let urlCourseStudent = "{{ tenant_route('courses.api.students', ['coursable_id' => '']) }}";

    let listCourseTable = $('#list-course').DataTable({
      language: {
        url: "{{ asset('json/datatable/'.App::getLocale().'.json') }}"
      },
      order: [[ 0, 'desc' ]],
      paging: false,
      info: false,
      scrollY: '300px',
      scrollCollapse: true,
      columns: [
        { data: 'ci' },
        { data: 'fullname' },
      ],
      ajax: {
        url: `${urlCourseStudent}`,
        dataSrc: ''
      },
      columnDefs: [
        {
          targets: 2,
          data: null,
          orderable: false,
          render: ( data, type, row ) => {
            return plans[data.plan_id];
          }
        }
      ]
    })


    function getPayments(data) {
      let urlPayments = "{{ tenant_route('payments.api.get') }}";
      $('#user_id').val(data.id)
      $('#plan_id').val(data.plan_id)
      $.ajax({
        url: urlPayments,
        data: {
          branch_id: $('#branch_id').val(),
          coursable_id: $('#coursable_id').val(),
          user_id: data.id,
          plan_id: data.plan_id
        }
      }).then(function (res) {
        console.log('res :>> ', res);
        $('#plan').text(res.plan);
        $('#dues_total').text(res.dues_total);

        let amount = 0;

        if (res.dues_total == res.dues.length) {
          if (res.dues_count == 0 )
            amount = parseFloat(res.dues[0]);
          else
            amount = parseFloat(res.dues[res.dues_count - 1]);

        } else {
          if (res.dues_count == 0 )
            amount = parseFloat(res.dues[0]) + parseFloat(res.dues[1]);
          else {
            amount = parseFloat(res.dues[res.dues_count]);
            res.dues_count--;
          }
        }

        $('#due_paid').text(amount.toFixed(2));
        $('#amount').val(amount.toFixed(2));
        $('#dues_count').text(res.dues_count);
        $('#total_paid').text(parseFloat(res.total_paid).toFixed(2));

        if (res.total_paid == 0) {
          $('#form-payment').addClass('d-none');
          $('#submit-payment').attr('disabled', 'disabled');
          $('#success-payment').removeClass('d-none');
          $('#jumbotron').addClass('d-none');
        } else {
          $('#form-payment').removeClass('d-none');
          $('#submit-payment').removeAttr('disabled');
          $('#success-payment').addClass('d-none');
          $('#jumbotron').removeClass('d-none');
        }
      });
    }

    $(function () {
      $('.table tbody').on('click', 'tr', function () {
        var data = listCourseTable.row( this ).data();
        $('.table tbody tr').removeClass('table-primary');
        $(this).addClass('table-primary');
        getPayments(data)
      });
    })
  </script>
@endsection
