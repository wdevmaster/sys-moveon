<div id="jumbotron" class="jumbotron p-1">
  <h4 class="display-5">
    {{ __('Payment plan') }}: <span id="plan"></span>
  </h4>
  <div class="d-flex justify-content-between">
    <div>
      {{ __('Dues') }}: <span id="dues_count">0</span>/<small id="dues_total">0</small>
    </div>
    <div>
      {{ __('Fee Amount ') }}: $<span id="due_paid">0.00</span>
    </div>
    <div>
      {{ __('Total to pay ') }}: $<span id="total_paid">0.00</span>
    </div>
  </div>
</div>

<div id="form-payment" class="row">
  <div class="form-group col-sm-6">
    <label>{{ __('Payment method') }}</label>
    {{
      Form::select(
        'payment_method',
        $paymentMethods,
        old("payment_method"),
        [
          'id' => 'payment_method',
          'class' => 'form-control',
          'placeholder' => __('Select :name', [ 'name' => '']),
          'required',
        ]
      )
    }}
  </div>

  <div class="form-group col-sm-6 d-none">
    <label>{{ __('Bank') }}</label>
    {!!
      Form::text(
        'bank',
        old('bank'),
        [
          'id' => 'bank',
          'class' => 'form-control',
          'autocomplete' => 'off',
        ]
      )
    !!}
  </div>

  <div class="form-group col-sm-6">
    <label>{{ __('Amount') }}</label>
    {!!
      Form::text(
        'amount',
        old('amount'),
        [
          'id' => 'amount',
          'class' => 'form-control money',
          'required'
        ]
      )
    !!}
  </div>

  <div class="form-group col-sm-12">
    <label>{{ __('Voucher') }}</label>
    {!!
      Form::file(
        'voucher_path',
        [
          'id' => 'voucher',
          'class' => 'form-control',
          'required'
        ]
      )
    !!}
  </div>
</div>

<div
  id="success-payment"
  class="swal2-popup swal2-modal swal2-icon-success swal2-show border mt-1 d-none"
  style="display: flex;"
>
  <div class="swal2-header">
    <div class="swal2-icon swal2-success swal2-icon-show" style="display: flex;">
      <div class="swal2-success-circular-line-left" style="background-color: rgb(255, 255, 255);"></div>
      <span class="swal2-success-line-tip"></span> <span class="swal2-success-line-long"></span>
      <div class="swal2-success-ring"></div> <div class="swal2-success-fix" style="background-color: rgb(255, 255, 255);"></div>
      <div class="swal2-success-circular-line-right" style="background-color: rgb(255, 255, 255);"></div>
    </div>
    <h2 class="swal2-title" id="swal2-title" style="display: flex;">
      {{ __('Fully paid course ') }}
    </h2>
  </div>
</div>
@section('vendor-style')
  @parent
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/autocomplete/autocomplete.css')) }}">
@endsection
@section('vendor-script')
  @parent
  <script src="{{ asset(mix('vendors/js/mask/jquery.mask.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/autocomplete/autocomplete.js')) }}"></script>
@endsection
@section('page-script')
  @parent
  <script>
    $('.money').mask("#,##0.00", {reverse: true});

    $('#payment_method').change(function() {
      let val = $(this).val();

      if (val == 'wire_transfer' || val == 'app_transfer') {
        $('#bank').parent().removeClass('d-none');
        $('#voucher').parent().removeClass('col-sm-12');
        $('#voucher').parent().addClass('col-sm-6');
        autocomplete(document.getElementById("bank"), filterBank(val));
      } else {
        $('#bank').parent().addClass('d-none');
        $('#voucher').parent().removeClass('col-sm-6');
        $('#voucher').parent().addClass('col-sm-12');
        $('#bank').val('');
      }
    });

    function filterBank(type) {
      let arry = [];
      let data = {!! $banks->toJson() !!};
      data.filter(item => item.type == type).forEach(item => arry.push(item.name));

      return arry;
    }

  </script>
@endsection
