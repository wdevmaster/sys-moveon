<div class="row">
  <div class="col-4">
    <div class="form-group">
      <div class="controls">
        <label>{{ __('Branch') }}</label>
        @if (!$franchise->is_multibranch)
          {{ Form::hidden('branch_id', $branches->first()->id, ['id' => 'branch_id']) }}
        @endif
        {{
          Form::select(
            'branch_id',
            $branches->pluck('name', 'id'),
            old('branches') ?? (!$franchise->is_multibranch ? $branches->first()->id : null),
            [
              'id' => $franchise->is_multibranch ? 'branch_id' : '',
              'class' => 'form-control',
              'placeholder' => __('Select :name', [ 'name' => '' ]),
              'required',
              !$franchise->is_multibranch ? 'disabled' : '',
            ]
          )
        }}
      </div>
    </div>
  </div>

  <div class="col-4">
    <div class="form-group">
      <div class="controls">
        <label>{{ __('Language') }}</label>
        {{
          Form::select(
            'language_id',
            [],
            old("language_id"),
            [
              'id' => 'language_id',
              'class' => 'form-control',
              'placeholder' => __('Select :name', [ 'name' => '']),
              'required',
            ]
          )
        }}
      </div>
    </div>
  </div>

  <div class="col-4">
    <div class="form-group">
      <div class="controls">
        <label>{{ __('Level students') }}</label>
        {{
          Form::select(
            'student_type_id',
            $studentType,
            old("student_type_id"),
            [
              'id' => 'student_type_id',
              'class' => 'form-control',
              'placeholder' => __('Select :name', [ 'name' => '' ]),
              'required',
            ]
          )
        }}
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-8">
    <div class="form-group">
      <div class="controls">
        <label>{{ __('Days') }}</label>
        <div class="row col pr-0">
          @php
            $carbon = Carbon\Carbon::today()->startOfWeek()->subDays(2);
          @endphp
          @for ($i = 0; $i < 7; $i++)
            @php $carbon->addDay() @endphp
            <div class="col-6 col-sm-4 px-0">
              <fieldset>
                <div class="vs-checkbox-con vs-checkbox-primary">
                  <input
                    value="1"
                    name="days[{{ $i }}]"
                    type="checkbox"
                    class="checkbox-days"
                    data-day="{{ $i }}"
                    data-label="{{ __($carbon->shortLocaleDayOfWeek) }}"
                  >
                  <span class="vs-checkbox">
                    <span class="vs-checkbox--check">
                      <i class="vs-icon feather icon-check"></i>
                    </span>
                  </span>
                  <span style="font-size: 12px;">
                    {{ __($carbon->format('l')) }}
                  </span>
                </div>
              </fieldset>
            </div>
          @endfor
        </div>
      </div>
    </div>
  </div>
  <div class="col-4">
    <div class="form-group">
      <div class="controls">
        <label>{{ __('Lapse') }}</label>
        {{
          Form::select(
            'schedule_id',
            [],
            old("schedule_id"),
            [
              'id' => 'schedule_id',
              'class' => 'form-control',
              'placeholder' => __('Select :name', [ 'name' => '' ]),
              'required',
            ]
          )
        }}
      </div>
    </div>
  </div>
</div>

@section('vendor-script')
  @parent
  <script src="{{ asset(mix('vendors/js/extensions/moment.min.js')) }}"></script>
@endsection

@section('page-script')
  @parent
  <!-- _config.js -->
  <script>
    const json = {!! $json->toJson() !!}
    let branch_id = language_id = null;
    let coursable = null;

    if ($('#branch_id').val() != '') {
      getLanguages();
      getDataCourse();
    }

    $('#branch_id').change(function() {
      branch_id = $(this).val();
      getLanguages();
      getDataCourse();
    })

    $('#language_id').change(function() {
      language_id = $(this).val();
      $('#student_type_id').removeAttr('disabled');
      getName();
      getDataCourse();
    })

    $('#student_type_id').change(function() {
      getLapse();
      getName();
      getDataCourse();
    })

    $('#schedule_id').change(function () {
      getDataCourse();
    })

    function getDataCourse() {
      let dat = getDataAjax();

      if (
        dat.branch_id != '' &&
        dat.language_id != '' &&
        dat.student_type_id != '' &&
        dat.days.length > 0 &&
        dat.schedule_id != ''
      ) {
        getTeachers()
        getHalls()
        getLevel()
      }
    }

    function getLanguages() {
      let dat = json.find(element => element.id == $('#branch_id').val());
      let options = [];

      for (let index = 0; index < dat.programs.length; index++) {
        const program = dat.programs[index];

        options.push({
          value: program.id,
          text: program.name
        })

      }

      renderOptionSelect($('#language_id'), options)
    }

    function getLapse() {
      let dat = json.find(element => element.id == $('#branch_id').val());
      let options = [];

      for (let index = 0; index < dat.schedules.length; index++) {
        const schedule = dat.schedules[index];
        let start_time = moment.utc(schedule.start_time)
        let end_time = moment.utc(schedule.end_time)

        options.push({
          value: schedule.id,
          text: `${start_time.format('hh:mm A')} - ${end_time.format('hh:mm A')}`
        })
      }

      renderOptionSelect($('#schedule_id'), options)
    }

    function getName() {
      let str = type = '';

      if ($(`select#language_id`).val() != '')
        str = $(`select#language_id option:selected`).text();

      if ($(`select#student_type_id`).val() != '')
        type = $(`select#student_type_id option:selected`).text();

      $('input#name').val(`${str} ${type}`)
      $('div#name').text(`${str} ${type}`)
    }

    async function getTeachers() {
      let url = "{{ tenant_route('teachers.api.index') }}";
      let options = [];
      await ajaxData(url).then(function (data) {
        $.each(data, function (key, val) {
          options.push({
            value: key,
            text: val
          })
        })

        renderOptionSelect($('#user_id'), options)
      })

      if (coursable)
        $(`#user_id option[value="${coursable.user_id}"]`).prop('selected', true);
    }

    async function getHalls() {
      let url = "{{ tenant_route('halls.api.index') }}";
      let options = [];
      await ajaxData(url).then(function (data) {
        $.each(data, function (key, val) {
          options.push({
            value: key,
            text: val
          })
        })

        renderOptionSelect($('#hall_id'), options)
      })

      if (coursable)
        $(`#hall_id option[value="${coursable.hall_id}"]`).prop('selected', true);
    }

    function getLevel() {
      let options = [];
      let dat = json.find(element => element.id == $('#branch_id').val());
      dat = dat.programs.find(element => element.id == $('select#language_id').val());

      for (let index = 0; index < dat.levels.length; index++) {
        const level = dat.levels[index];

        options.push({
          value: level.id,
          text: level.name
        })
      }
      renderOptionSelect($('#level_id'), options)
    }

    function ajaxData(url) {
      return $.ajax({
        url: url,
        data: getDataAjax(),
      });
    }

    function getDataAjax() {
      return {
        branch_id: $('#branch_id').val(),
        language_id: $('select#language_id').val(),
        student_type_id: $('select#student_type_id').val(),
        days: getDataCheckbox(),
        schedule_id: $('select#schedule_id').val(),
        course: "{{ request()->is('app/*/courses/create') }}"
      }
    }

    function getDataCheckbox() {
      let array = []
      let checkbox = $('.checkbox-days:checked')

      for (let index = 0; index < checkbox.length; index++)
        array.push($(checkbox[index]).data('day'))

      return array
    }

    function renderOptionSelect(select, options) {
      $(select).children('option').remove()
      $(select).append(`
        <option>{{ __('Select :name', [ 'name' => '...' ]) }}</option>
      `);

      for (let index = 0; index < options.length; index++) {
          const option = options[index];
          $(select).append(`
            <option value="${option.value}">${option.text}</option>
          `)
      }

      $(select).removeAttr('disabled')
    }

  </script>
  @isset($data)
  <script>
    initRenderData()

    function initRenderData() {
      getLanguages();
      $('#student_type_id').removeAttr('disabled');
      getLapse();

      $(`#language_id option[value="${data.language_id}"]`).prop('selected', true);
      coursable = data.coursables[data.coursables.length -1];
      $(`#schedule_id option[value="${coursable.schedule_id}"]`).prop('selected', true);

      $.each(coursable.days, function (key, row) {
        $(`.checkbox-days[name="days[${row.day}]"]`).prop('checked', true);
      });

      getName();
      getTeachers();
      getHalls();
      getLevel();
      $(`#level_id option[value="${data.level_id}"]`).prop('selected', true);
    }
  </script>
  @endisset
@endsection
