<div class="row">
  <div class="col-6">
    <div class="form-group">
      <div class="controls">
        <label>{{ __('Teacher') }}</label>
        {{
          Form::select(
            'user_id',
            [],
            old("user_id"),
            [
                'id' => 'user_id',
                'class' => 'form-control',
                'placeholder' => __('Select :name', [ 'name' => '' ]),
                'required',
            ]
          )
        }}
      </div>
    </div>
  </div>

  <div class="col-6">
    <div class="form-group">
      <div class="controls">
        <label>{{ __('Hall') }}</label>
        {{
          Form::select(
            'hall_id',
            [],
            old("hall_id"),
            [
                'id' => 'hall_id',
                'class' => 'form-control',
                'placeholder' => __('Select :name', [ 'name' => '' ]),
                'required',
            ]
          )
        }}
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-6">
    <label class="form-lable">@lang('Name course')</label>
    <input type="hidden" name="name" id="name">
    <div id="name"></div>
  </div>
  <div class="col-6">
    <div class="form-group">
      <div class="controls">
        <label>{{ __('Level') }}</label>
        {{
          Form::select(
            'level_id',
            [],
            old("level_id"),
            [
                'id' => 'level_id',
                'class' => 'form-control',
                'placeholder' => __('Select :name', [ 'name' => '' ]),
                'required',
            ]
          )
        }}
      </div>
    </div>
  </div>
</div>
