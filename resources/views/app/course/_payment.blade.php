<div class="row">
  <div class="col-sm-6">
    <div class="row">
      <div class="form-group col-sm-6">
        <label>{{ __('Payment plan') }}</label>
        {!!
          Form::select(
            'plan_id',
            $plans,
            old('plan_id'),
            [
              'id' => 'plan_id',
              'class' => 'form-control'
            ]
          )
        !!}
      </div>
      <div class="form-group col-sm-6">
        <label>{{ __('Course duration') }}</label>
        <div class="input-group">
          <div class="input-group-prepend">
            <span class="input-group-text" id="input-group-interval">
              {{ __('Week') }}
            </span>
          </div>
          {!!
            Form::number(
              'duration_week',
              old('duration_week') ?? (isset($data) ? $data->duration_week : null),
              [
                'aria-describedby' => 'input-group-interval',
                'class' => 'form-control',
                'id' => 'interval_week'
              ]
            )
          !!}
        </div>
      </div>
    </div>
  </div>
  <div class="col-sm-6">
    <label>{{ __('Calculation table ') }}</label>
    <table class="table mb-0">
      <tbody>
        <tr>
          <td class="w-70">{{ __('Base price') }}</td>
          <td class="text-right p-0 pr-1" style="width: 80px;">
            {{
              Form::text(
                'price',
                old('price') ?? (isset($data) ? $data->price : null),
                [
                  'id' => 'price',
                  'class' => 'form-control money',
                  'placeholder' => '0.00',
                  'required'
                ]
              )
            }}
          </td>
        </tr>
        <tr>
          <td class="w-70">
            {{ __('Dues') }}
            <b><span id="dues">0</span></b>
          </td>
          <td class="text-right p-0 pr-1" style="width: 80px;">
            <input type="text" id="mony_dues" class="form-control money" placeholder="0.00">
          </td>
        </tr>
      </tbody>
    </table>
    <table id="feactures" class="table d-none">
      <thead>
        <tr class="table-secondary">
          <th colspan="2">
            <b>{{ __('Plan feactures') }}</b>
          </th>
        </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
  </div>
</div>

@section('page-style')
  @parent
  <style>
    .table td {
      border-color: #d9d9d9 !important;
    }
  </style>
@endsection

@section('vendor-script')
  @parent
  <!-- vendor files -->
  <script src="{{ asset(mix('vendors/js/mask/jquery.mask.min.js')) }}"></script>
@endsection
@section('page-script')
  @parent
  <!-- Page js files -->
  <script>
    $('.money').mask("#,##0.00", {reverse: true});
    let intervals = {!! collect($intervals)->toJson() !!};
    let featureTypes = {!! collect($featureTypes)->toJson() !!};
    let featureOrigin = {!! collect($featureOrigin)->toJson() !!};
    let plan = null;
    getPlan();

    function getPlan() {
      let id = $('#plan_id').val();

      if (id != '') {
        $.ajax({
          url: "{{ tenant_route('plans.api.index', [':id']) }}".replace(':id', id),
        }).then(function (data) {
          let n = 4;
          plan = data;

          if ($('#interval_week').val() == '') {
            if (plan.interval_unit == 'month') {
              n = n * plan.interval_count
              $('#interval_week').val(n);
            } else if (plan.interval_unit == 'week'){
              $('#interval_week').val(plan.interval_count);
            }
          }

          calDues();
          renderFeature();
        })
      }
    }

    $('#plan_id').change(function() {
      getPlan()
    });

    $('#interval_week').change(function () {
      if ($(this).val() < 0)
        $(this).val(0)
      else {
        let n = calDues();

        if ($('#price').val())
          $('#mony_dues').val((parseFloat($('#price').val())/n).toFixed(2));

        renderFeature();
      }
    })

    function calDues() {
      let n = parseInt($('#interval_week').val());

      if (plan.interval_unit == 'month') {
        n = Math.round((n / 4)/plan.interval_count , 1);
      } else if (plan.interval_unit == 'week') {
        n = Math.round(n /plan.interval_count , 1);
      }

      $('span#dues').text(n);
      return n;
    }

    $('#price').change(function () {
      let n = calDues();

      $('#mony_dues').val((parseFloat($(this).val())/n).toFixed(2));
      renderFeature();
    })

    $('#mony_dues').change(function () {
      let n = calDues();

      $('#price').val((parseFloat($(this).val())*n).toFixed(2))
      renderFeature();
    });

    function renderFeature() {
      if (plan.features.length > 0) {
        $('#feactures').removeClass('d-none');
        $('#feactures tbody tr').remove();
      }

      let price = parseFloat($('#price').val())
      let dues = parseFloat($('#mony_dues').val())
      let money = 0;
      let total = 0

      $.each(plan.features, function (key, row) {
        if (price && dues) {
          let value = row.is_percentage == 1 ? row.value/100 : row.value;

          if (row.origin == 'base')
            money = row.is_percentage == 1 ? price * value : price - value;
          else
            money = row.is_percentage == 1 ? dues * value : dues - value;
        }

        if (row.type != 'inscription')
          $('#feactures tbody').append(`
            <tr>
              <td class="w-70">
                ${featureTypes[row.type]}
                (
                  ${featureOrigin[row.origin]} -
                  ${row.is_percentage == 0 ? '$' : ''}${row.value}${row.is_percentage == 1 ? '%' : ''}
                )
              </td>
              <td class="text-right p-0 pr-1" style="width: 80px;">
                $${money.toFixed(2)}
              </td>
            </tr>
          `);
        else
          $('#feactures tbody').append(`
            <tr>
              <td class="w-70">
                ${featureTypes[row.type]}
              </td>
              <td class="text-right p-0 pr-1" style="width: 80px;">
                $${row.value.toFixed(2)}
              </td>
            </tr>
          `);
      })
    }

  </script>
@endsection
