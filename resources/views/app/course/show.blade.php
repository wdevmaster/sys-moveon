@extends('layouts/default')

@section('title', __('Add student course').": {$data->name} {$data->level->name}")

@section('content')
<section id="basic-datatable">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-content">
          <div class="card-body card-dashboard">
            {!!
              Form::model($data, [
                'url' => tenant_route('courses.update', $data->hid),
                'method' => 'PUT',
                'id' => 'form',
                'class' => 'steps-validation wizard-circle',
              ])
            !!}

              <div class="row">
                <div class="col-sm-12">
                  <div class="row">
                    <div class="col-12 col-sm-6">
                      <h5 class="mb-1 mt-2 mt-sm-0">
                        <i class="feather icon-users mr-25"></i>
                        {{ __('Waiting students') }}
                      </h5>
                      <table id="list-student" class="table table-striped table-bordered">
                        <thead>
                          <tr>
                            <th></th>
                            <th scope="col">{{__('CI')}}</th>
                            <th scope="col">{{__('Name')}}</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                    <div class="col-12 col-sm-6">
                      <h5 class="mb-1 mt-2 mt-sm-0">
                        <i class="feather icon-user-check mr-25"></i>
                        {{ __('Enrolled students ') }}
                      </h5>
                      <table id="list-course" class="table table-striped table-bordered">
                        <thead>
                          <tr>
                            <th></th>
                            <th scope="col">{{__('CI')}}</th>
                            <th scope="col">{{__('Name')}}</th>
                            <th scope="col">{{ __('Payment plan') }}</th>
                            <th scope="col"></th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>

              <hr>
              <div class="form-group text-right gruop-btn">
                <a href="{{ tenant_route('courses.index') }}" class="btn btn-link text-muted">{{ __('Cancel') }}</a>
              </div>

            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('vendor-style')
  @parent
  {{-- vendor css files --}}
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
@endsection
@section('vendor-script')
  @parent
  {{-- vendor files --}}
  <script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/extensions/polyfill.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/extensions/axios.min.js')) }}"></script>
@endsection
@section('page-script')
  @parent
  <script>
    let urlStudent = "{{ tenant_route('students.api.index') }}";
    let urlCourseStudent = "{{ tenant_route('courses.api.students') }}";
    let urlSync = "{{ tenant_route('courses.api.sync', $data->hid) }}"
    let data = {!! $data->toJson() !!}
    let plans = {!! $plans->toJson() !!}

    function getUrlOptions() {
      let str = `?branch_id=${data.branch_id}`;

      str += `&course_id=${data.id}`;
      str += `&student_type_id=${data.student_type_id}`;

      let coursable = data.coursables[data.coursables.length - 1];
      str += `&coursable_id=${coursable.id}`;

      return str;
    }

    let listStudentTable = $('#list-student').DataTable({
      language: {
        url: "{{ asset('json/datatable/'.App::getLocale().'.json') }}"
      },
      order: [[ 1, 'desc' ]],
      paging: false,
      info: false,
      scrollY: '300px',
      scrollCollapse: true,
      columns: [
        {
          orderable: false,
          data: 'profile_photo', render: function ( data, type, row, meta ) {
          return `
            <img class="round"
              src="${data}"
              alt="avatar"
              height="40"
              width="40"
              class="m-r-1"
            />
          `
        }},
        { data: 'ci' },
        { data: 'fullname' },
      ],
      ajax: {
        url: `${urlStudent}${getUrlOptions()}`,
        dataSrc: ''
      },
      columnDefs: [
        {
          targets: 3,
          data: null,
          orderable: false,
          render: ( data, type, row ) => {
            let renderBtn = `
              <button
                type="button"
                class="btn btn-sm btn-success btn-sync"
                data-type="attach"
                data-id="${data.id}"
              >
                <i class="fa fa-plus"></i>
              </button>
            `
            return renderBtn;
          }
        }
      ]
    });

    let listCourseTable = $('#list-course').DataTable({
      language: {
        url: "{{ asset('json/datatable/'.App::getLocale().'.json') }}"
      },
      order: [[ 1, 'desc' ]],
      paging: false,
      info: false,
      scrollY: '300px',
      scrollCollapse: true,
      columns: [
        {
          orderable: false,
          data: 'profile_photo', render: function ( data, type, row, meta ) {
          return `
            <img class="round"
              src="${data}"
              alt="avatar"
              height="40"
              width="40"
              class="m-r-1"
            />
          `
        }},
        { data: 'ci' },
        { data: 'fullname' },
      ],
      ajax: {
        url: `${urlCourseStudent}${getUrlOptions()}`,
        dataSrc: ''
      },
      columnDefs: [
        {
          targets: 3,
          data: null,
          orderable: false,
          render: ( data, type, row ) => {
            var select = $("<select></select>", {});
            select.addClass('form-control').addClass('plans');
            $.each(plans, function (k, v) {
              var option = $("<option></option>", {
                "text": v,
                "value": k
              });

              if (data.plan_id == k)
                option.attr("selected", "selected")

              select.append(option);
            });
            return select.prop("outerHTML");
          }
        },
        {
          targets: 4,
          data: null,
          orderable: false,
          render: ( data, type, row ) => {
            return `
              <button
                type="button"
                class="btn btn-sm btn-danger btn-sync"
                data-type="detach"
                data-id="${data.id}"
              >
                <i class="fa fa-minus"></i>
              </button>
            `;
          }
        }
      ]
    })

    const http = axios.create();
    async function sync(form) {
      try {
        const { data } = await http.put(urlSync, form);
        return data
      } catch (e) {
        console.error(e);
      }
    }

    $(function () {
      $('.table tbody').on('click', 'button.btn-sync', function() {
        $(this).addClass('disabled')

        sync({
            _token: "{{ csrf_token() }}",
            type: $(this).data('type'),
            id: $(this).data('id')
        }).then( () => {
          listStudentTable.ajax.reload();
          listCourseTable.ajax.reload();
        })
      })

      $('.table tbody').on('change', 'select.plans', function() {
        let $this = $(this).parent().parent();
        let dataRow = listCourseTable.row( $this ).data();
        sync({
            _token: "{{ csrf_token() }}",
            type: 'sync_plan',
            id: dataRow.id,
            plan_id: $(this).val()
        }).then( () => {
          listCourseTable.ajax.reload();
        })
      })
    });
  </script>
@endsection
