@extends('layouts/default')

@section('title', __('Create new :name', [ 'name' => __('Course') ]))

@section('content')
<section id="basic-datatable">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-content">
          <div class="card-body card-dashboard">
            {!!
              Form::open([
                'url' => tenant_route('courses.store'),
                'id' => 'form',
                'class' => 'steps-validation wizard-circle',
              ])
            !!}

              <!-- Step 1 -->
              <h6>
                <i class="step-icon feather icon-info"></i>
                {{ __('Course settings') }}
              </h6>
              <fieldset class="py-1">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="row">
                      <div class="col-12 col-sm-7">
                        <h5 class="mb-1 mt-2 mt-sm-0">
                          <i class="feather icon-codepen mr-25"></i>
                          {{ __('Setting') }}
                        </h5>
                        @include('app.course._config')
                      </div>
                      <div class="col-12 col-sm-5">
                        <h5 class="mb-1 mt-2 mt-sm-0">
                          <i class="feather icon-airplay mr-25"></i>
                          {{ __('Data') }}
                        </h5>
                        @include('app.course._data')
                      </div>
                    </div>
                  </div>
                </div>
              </fieldset>

              <!-- Step 2 -->
              @can('courses.manager')
                <h6>
                  <i class="step-icon feather icon-layers"></i>
                  {{ __('Payment') }}
                </h6>
                <fieldset class="py-1">
                  @include('app.course._payment')
                </fieldset>
              @endcan
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('page-style')
  @parent
  <!-- Page css files -->
  <link rel="stylesheet" href="{{ asset(mix('css/plugins/forms/wizard.css')) }}">
@endsection
@section('vendor-script')
  <!-- vendor files -->
  <script src="{{ asset(mix('vendors/js/extensions/jquery.steps.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/validation/jquery.validate.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/validation/localization/messages_'.\App::getLocale().'.js')) }}"></script>
@endsection

@section('page-script')
  @parent
  <!-- Page js files -->
  <script src="{{ asset(mix('js/scripts/validate-form.js')) }}"></script>
@endsection

