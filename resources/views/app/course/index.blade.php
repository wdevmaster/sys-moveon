@extends('layouts/default')

@section('title', __('Courses'))

@section('btn-action')
  @can('courses.create')
    <a
      href="{{ tenant_route('courses.create') }}"
      class="btn btn-outline-primary"
    >
      <span>
        <i class="feather icon-plus"></i> {{ __('Add New') }}
      </span>
    </a>
  @endcan
@endsection

@section('content')
<section id="basic-datatable">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-content">
          <div class="card-body card-dashboard">
            <div class="table-responsive">
              <table class="table">
                <thead>
                    <tr>
                      <th>{{__('Course')}}</th>
                      <th class="no-sort">{{__('Teacher')}}</th>
                      @if($franchise->is_multibranch)
                        <th  class="sort">{{__('Branch')}}</th>
                      @endif
                      <th class="no-sort">{{__('Hall')}}</th>
                      <th class="no-sort">{{__('Capacity')}}</th>
                      <th class="no-sort">{{__('Status')}}</th>
                      <th class="no-sort" style="width: 15%">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                  @foreach($rows as $course)
                    @php
                      if(!$coursable = $course->coursables->last())
                        continue;
                    @endphp
                    <tr id="row-{{ $coursable->hid }}">
                      <td>{{ $course->name }} {{ $course->level->name }}</td>
                      <td>{{ $coursable->user->fullname }}</td>
                      @if($franchise->is_multibranch)
                        <td>{{ $course->branch->name }}</td>
                      @endif
                      <td>{{ $coursable->hall->name }}</td>
                      <td>
                        @php
                          $phcapacy = ($coursable->students->count() / $coursable->hall->capacity) * 100;
                          $badgeClass = 'primary';

                          if ($phcapacy <= 40)
                            $badgeClass = 'success';
                          elseif ($phcapacy > 40 && $phcapacy <= 70)
                            $badgeClass = 'warning';
                          else
                            $badgeClass = 'danger';
                        @endphp
                        <h3 class="badge badge-{{ $badgeClass }}">
                            {{ $coursable->students->count() }} <small>/ {{ $coursable->hall->capacity }}</small>
                        </h3>
                      </td>
                      <td>
                        @php
                          $badgeClass = $coursable->status->is_inpro
                              ? 'success'
                              : ( $coursable->status->is_finis ? 'danger' : 'warning');
                        @endphp
                        <span class="badge badge-{{ $badgeClass }}">
                            {{ $coursable->status }}
                        </span>
                      </td>
                      <td class="text-left">
                        @if (!$coursable->status->is_finis)
                          @can('courses.manager')
                            <a
                              href="{{ tenant_route('courses.students', [$course->hid]) }}"
                              class="btn btn-sm btn-secondary btn-edit m-r-5"
                              data-toggle="tooltip"
                              data-original-title="{{__('Add :type', ['type' => __('Student')])}}"
                            >
                              <i class="fa fa-user-plus font-14"></i>
                            </a>
                          @endcan
                          @can('courses.edit')
                            <a
                              href="{{ tenant_route('courses.edit', [$course->hid]) }}"
                              class="btn btn-sm btn-primary btn-edit m-r-5"
                              data-toggle="tooltip"
                              data-original-title="{{__('Edit')}}"
                            >
                              <i class="fa fa-pencil font-14"></i>
                            </a>
                          @endcan
                        @endif
                        @can('teachers.delete')
                          <button
                            type="button"
                            data-id="{{ $course->hid }}"
                            class="btn btn-sm btn-danger btn-delete"
                            data-toggle="tooltip"
                            data-original-title="{{__('Remove')}}"
                          >
                            <i class="fa fa-trash font-14"></i>
                          </button>
                        @endcan
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('vendor-style')
  {{-- vendor css files --}}
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/animate/animate.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
@endsection
@section('vendor-script')
{{-- vendor files --}}
  <script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/extensions/polyfill.min.js')) }}"></script>
@endsection
@section('page-script')
  @parent
  @include('elements.langJs')
  <script>
    var routeDelete = "{{ tenant_route('courses.destroy', [':id']) }}";
    var routeIndex = "{{ tenant_route('courses.index') }}";
    var csrfToken = "{{csrf_token()}}";
    var configTable = {
      ordering: false,
      columnDefs: [{
        targets: 'sort',
        orderable: true,
      }],
      language: {
        url: "{{ asset('json/datatable/'.App::getLocale().'.json') }}"
      }
    }
  </script>
  <script src="{{ asset(mix('js/scripts/basic-crud.js')) }}"></script>
@endsection
