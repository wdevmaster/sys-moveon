@extends('layouts/default')

@section('title', __('Editing :name', [ 'name' => __('Course') ]))

@section('content')
<section id="basic-datatable">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-content">
          <div class="card-body card-dashboard">
            {!!
              Form::model($data, [
                'url' => tenant_route('courses.update', $data->hid),
                'method' => 'PUT',
                'id' => 'form',
                'class' => 'steps-validation wizard-circle',
              ])
            !!}

              @include('app.course._form')

            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('page-script')
  <!-- Page js files -->
  <script src="{{ asset(mix('js/scripts/validate-form.js')) }}"></script>
  <script>
    let data = {!! $data->toJson() !!}
  </script>
@endsection

