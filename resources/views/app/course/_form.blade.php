{!! Form::hidden('coursable_id', $data->coursables->last()->id) !!}

<ul class="nav nav-tabs" role="tablist">
  <li class="nav-item">
    <a
      class="nav-link active"
      id="course-tab"
      data-toggle="tab"
      href="#course"
      aria-controls="course"
      role="tab"
      aria-selected="true"
    >
      {{ __('Course settings') }}
    </a>
  </li>
  @can('courses.manager')
    <li class="nav-item">
      <a
        class="nav-link"
        id="payment-tab"
        data-toggle="tab"
        href="#payment"
        aria-controls="payment"
        role="tab"
        aria-selected="true"
      >
        {{ __('Payment') }}
      </a>
    </li>
  @endcan
</ul>
<div class="tab-content pt-2">
  <div class="tab-pane active" id="course" aria-labelledby="course-tab" role="tabpanel">
    <div class="row">
      <div class="col-sm-12">
        <div class="row">
          <div class="col-12 col-sm-7">
            <h5 class="mb-1 mt-2 mt-sm-0">
              <i class="feather icon-codepen mr-25"></i>
              {{ __('Setting') }}
            </h5>
            @include('app.course._config')
          </div>
          <div class="col-12 col-sm-5">
            <h5 class="mb-1 mt-2 mt-sm-0">
              <i class="feather icon-airplay mr-25"></i>
              {{ __('Data') }}
            </h5>
            @include('app.course._data')
          </div>
        </div>
      </div>
    </div>
  </div>
  @can('courses.manager')
    <div class="tab-pane" id="payment" aria-labelledby="payment-tab" role="tabpanel">
      @include('app.course._payment')
    </div>
  @endcan
</div>
<hr>
<div class="form-group text-right gruop-btn">
  <button class="btn btn-success">{{ __('Save') }}</button>
  <a href="#" onclick="history.back();" class="btn btn-link text-muted">{{ __('Cancel') }}</a>
</div>
