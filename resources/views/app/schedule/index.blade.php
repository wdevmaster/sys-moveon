@extends('layouts/default')

@section('title', __(ucfirst($model)).' '.__('Schedules'))

@section('content')
<section id="basic-datatable">
  <div class="row">
    <div class="col-12">
      @includeWhen($model == 'teachers', 'app.schedule._teacher')
    </div>
  </div>
</section>
@endsection

@section('page-script')
  @parent
  <script>
    clearTable();

    function clearTable() {
      $('button.schedules').remove();
      $('input.check-schedules').remove();
    }
  </script>
@endsection
