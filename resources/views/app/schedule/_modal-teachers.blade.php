<div
  class="modal fade text-left"
  id="model-teachers"
  tabindex="-1"
  role="dialog"
  aria-labelledby="ModalLabel"
  aria-hidden="true"
>
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="ModalLabel">{{ __('Teachers') }} </h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table id="table-teachers" class="table nowrap scroll-horizontal-vertical">
          <thead>
            <tr>
              <th></th>
              <th>{{__('CI')}}</th>
              <th class="sort">{{__('Name')}}</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>


@section('vendor-style')
  @parent
  {{-- vendor css files --}}
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
@endsection
@section('vendor-script')
  @parent
  {{-- vendor files --}}
  <script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/extensions/polyfill.min.js')) }}"></script>
@endsection
@section('page-script')
  @parent
  <script>
    let id = null;
    var table = $('#table-teachers').DataTable( {
      language: {
        url: "{{ asset('json/datatable/'.App::getLocale().'.json') }}"
      },
      ajax: {
        url: urlTeachers,
        dataSrc: ''
      },
      columns: [
        { data: 'profile_photo', render: function ( data, type, row, meta ) {
          return `
            <img class="round"
              src="${data}"
              alt="avatar"
              height="40"
              width="40"
              class="m-r-1"
            />
          `
        }},
        { data: 'ci' },
        { data: 'fullname' },
      ],
      searching: false,
      ordering:  false,
      paging: false,
      info: false,
      scrollY: '200px',
      scrollCollapse: true,
    });
  </script>
@endsection
