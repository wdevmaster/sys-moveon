<div class="card">
  <div class="card-header">
    <div class="row col-12 col-md-3">
      @if (!$franchise->is_multibranch)
        {{ Form::hidden('branch_id', $branches->first()->id, ['id' => 'branch']) }}
      @else
        {{
          Form::select(
            'branches[]',
            $branches->pluck('name', 'id'),
            old('branches') ?? (!$franchise->is_multibranch ? $branches->first()->id : null),
            [
              'id' => 'branch',
              'class' => 'form-control',
            ]
          )
        }}
      @endif
    </div>
  </div>
  <div class="card-content">
    <div class="card-body">
      @include('app.schedule._form')
    </div>
  </div>
</div>

@include('app.schedule._modal-teachers')

@section('page-style')
  @parent
  <style>
    .show-teachers {
      padding: 5px !important;
    }
  </style>
@endsection

@section('page-script')
  @parent
  <script>
    let data = null;

    $(function () {
      getData();

      $('select#branch').change(function() {
          getData();
      });
    });

    function getData() {
      $('button.show-teachers').remove();
      let branchId = $('#branch').val()
      const url = "{{ tenant_route('schedules.api.index') }}"

      $.ajax({
        url: url,
        data: {
            b: branchId,
        },
        beforeSend: function( xhr ) {
          $('select#branch').attr('disabled', 'disabled')
        }
      }).done(function (resp) {
        $('select#branch').removeAttr('disabled')
        if (resp) {
          data = resp
          renderTbody()
        }
      })
    }

    function renderTbody() {
      $.each(data, function (day, rows) {
        $.each(rows, function (lapse, teachers) {
          $(`td#l${lapse}d${day}`).append(`
            <button
              type="button"
              class="btn btn-info show-teachers"
              data-day="${day}"
              data-lapse="${lapse}"
            >
            {{ __('Teachers available') }}
            </button>
          `)
        })
      })
    }

    let urlTeachers = "{{ tenant_route('schedules.teachers') }}";
    $(document).on('click', '.show-teachers', function () {
        let day = $(this).data('day');
        let lapse = $(this).data('lapse');

        table.ajax.url( `${urlTeachers}?id=${JSON.stringify(data[day][lapse])}` ).load();
        $('#model-teachers').modal('toggle');
    })
  </script>
@endsection
