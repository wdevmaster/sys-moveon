<div class="table-responsive">
  <div class="table-wrap">
  <table id="table-schedule" class="table table-bordered mb-0">
    <thead>
      <tr>
        <th class="hours">
          <div>{{ __('Hours') }}</div>
        </th>
        <th>{{ __('Sunday') }}</th>
        <th>{{ __('Monday') }}</th>
        <th>{{ __('Tuesday') }}</th>
        <th>{{ __('Wednesday') }}</th>
        <th>{{ __('Thursday') }}</th>
        <th>{{ __('Friday') }}</th>
        <th>{{ __('Saturday') }}</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($schedules as $item)
        <tr>
          <th class="hours">
            <div>
              <span class="start">{{ $item->start_time->format('h:i A') }}</span>
              <span class="end">{{ $item->end_time->format('h:i A')}}</span>
            </div>
          </th>
          @for ($i = 0; $i < 7; $i++)
          <td id="l{{ $item->start_time->format('H') }}d{{ $i }}">
            <button
              type="button"
              id="btn-l{{ $item->start_time->format('H') }}d{{ $i }}"
              class="btn btn-outline-light schedules"
              data-check="l{{ $item->start_time->format('H') }}d{{ $i }}"
            >
              ---
            </button>
            {!!
              Form::checkbox(
                "schedules[{$i}][{$item->id}]",
                '1',
                null,
                ['id' => "l{$item->start_time->format('H')}d{$i}", 'class' => 'd-none check-schedules']
              )
            !!}
          </td>
          @endfor
        </tr>
      @empty
        <tr id="empty">
          <td colspan="8">{{ __('No registered schedules') }}</td>
        </tr>
      @endforelse
    </tbody>
  </table>
  </div>
</div>

@section('page-style')
  @parent
  <style>
    .table-responsive {
	    position:relative;
	    margin:auto;
	    overflow:hidden;
    }

    .table-wrap {
      width:100%;
      overflow:auto;
    }

    #table-schedule thead th,
    #table-schedule tbody td,
    #table-schedule tfoot td {
      min-width: 150px;
      text-align:center;
    }

    table#table-schedule, th, td {
      border-color:rgba(34, 41, 47, 0.1) !important;
    }


    th.hours {
      position: sticky;
      left: 0;
      background-color: white;
      border:1px solid #000;
      visibility:visible;
      padding: 0;
      box-shadow: 0 4px 20px 0 rgba(0, 0, 0, 0.07);
      z-index: 10;
    }

    table#table-schedule thead th {
      background: #3dcc7d !important;
      color: white;
    }

    table#table-schedule td.hours {
      vertical-align: middle;
    }

    table#table-schedule .hours div{
      display: flex;
      flex-direction: column;
      text-align: center;
      padding: 14px;
      border: 1px solid rgba(34, 41, 47, 0.1);
    }
  </style>
@endsection

@section('vendor-script')
  @parent
  <script src="{{ asset(mix('vendors/js/extensions/moment.min.js')) }}"></script>
@endsection

@section('page-script')
  @parent
  <!-- page__input-branch.js -->
  <script>
    let schedules= {!! isset($data) ? $data->schedules->toJson() : collect([])->toJson() !!}
    initSchedules();

    $('.schedules').click(function() {
      let check = $(this).data('check')
      checkSchedules(check)
    })

    function initSchedules() {
      $.each(schedules, function (k, row) {
        checkSchedules(`l${moment.utc(row.start_time).format('HH')}d${row.day}`)
      })
    }

    function checkSchedules(check) {
      let checkout = $(`input#${check}`);
      let btn = $(`button#btn-${check}`);

      if ($(btn).hasClass('btn-outline-light')) {
        $(btn).removeClass('btn-outline-light')
        $(btn).addClass('btn-info')
        $(btn).text("{{ __('Available') }}")

        $(checkout).prop('checked', true)

      } else if ($(btn).hasClass('btn-info')){
        $(btn).removeClass('btn-info')
        $(btn).addClass('btn-outline-light')
        $(btn).text("---")

        $(checkout).prop('checked', false)
      }
    }

  </script>
@endsection
