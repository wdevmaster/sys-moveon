@extends('layouts/default')

@if ($status->is_active)
    @section('title', __('Students'))
@else
    @section('title', $status." ".__('Students'))
@endif

@section('btn-action')
  @if ($status->is_active)
    @can('students.create')
      <div class="btn-group dropdown">
        <button
          type="button"
          class="btn btn-outline-primary dropdown-toggle"
          data-toggle="dropdown"
          aria-haspopup="true"
          aria-expanded="false"
        >
          {{ __('Add New') }}
        </button>
        <div class="dropdown-menu">
          <a
            class="dropdown-item"
            href="{{ tenant_route('students.create', ['status' =>$status->value()]) }}"
          >
            {{ __('Adult') }}
          </a>
          <a
            class="dropdown-item"
            href="{{ tenant_route('representatives.create', ['student' => true]) }}"
          >
            {{ __('Younger') }}
          </a>
        </div>
      </div>
    @endcan
  @endif
@endsection

@section('content')
<section id="basic-datatable">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-content">
          <div class="card-body card-dashboard">
            @if (!$status->is_retired)
              <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                  <a
                    class="nav-link active"
                    id="active-tab"
                    data-toggle="tab"
                    href="#active"
                    aria-controls="active"
                    role="tab"
                    aria-selected="true"
                  >
                    {{ __('Active') }}
                  </a>
                </li>
                <li class="nav-item">
                  <a
                    class="nav-link"
                    id="trash-tab"
                    data-toggle="tab"
                    href="#trash"
                    aria-controls="trash"
                    role="tab"
                    aria-selected="true"
                  >
                    {{ __('In trash') }}
                  </a>
                </li>
              </ul>
            @endif
            <div class="tab-content pt-2">
              <div class="tab-pane active" id="active" aria-labelledby="active-tab" role="tabpanel">
                @include('app.student._table', ['rows' => $rows['active']])
              </div>
              @if (!$status->is_retired)
                <div class="tab-pane" id="trash" aria-labelledby="trash-tab" role="tabpanel">
                  @include('app.student._table', ['rows' => $rows['trash']])
                </div>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('vendor-style')
  {{-- vendor css files --}}
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/animate/animate.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
@endsection
@section('vendor-script')
{{-- vendor files --}}
  <script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/extensions/polyfill.min.js')) }}"></script>
@endsection
@section('page-script')
  @parent
  @include('elements.langJs')
  <script>
    var routeDelete = "{{ tenant_route('students.destroy', ['status' => $status->value(), ':id']) }}";
    var routeRestore = "{{ tenant_route('students.restore', ['status' => $status->value(), ':id']) }}";
    var routeIndex = "{{ tenant_route('students.index', ['status' => $status->value()]) }}";
    var csrfToken = "{{csrf_token()}}";
    var configTable = {
      ordering: false,
      columnDefs: [{
        targets: 'sort',
        orderable: true,
      }],
      language: {
        url: "{{ asset('json/datatable/'.App::getLocale().'.json') }}"
      }
    }
  </script>
  <script src="{{ asset(mix('js/scripts/basic-crud.js')) }}"></script>
@endsection
