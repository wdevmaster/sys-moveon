<div class="table-responsive">
  <table class="table">
    <thead>
        <tr>
          <th></th>
          <th>{{__('CI')}}</th>
          <th class="sort">{{__('Name')}}</th>
          <th class="sort">{{__('Languages')}}</th>
          <th  class="sort">{{__('Status')}}</th>
          @if($status->value() != 'preenrolled')
            <th>{{ $status->value() != 'retired' ?  __('Last login') : __('Retired') }}</th>
          @endif
          <th class="no-sort" style="width: 15%">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
      @foreach($rows as $row)
        <tr id="row-{{ $row->hid }}">
          <td>
            <img class="round"
              src="{{ $row->profile_photo }}"
              alt="avatar"
              height="40"
              width="40"
              class="m-r-1"
            />
          </td>
          <td>
            <a href="{{ tenant_route('students.show', [$status->value(), $row->hid]) }}">
              {{ $row->personalData->ci }}
            </a>
          </td>
          <td>
            {{ $row->fullname }}
          </td>
          <td>
            @foreach ($row->languages as $language)
              <span class="badge badge-info">
                {{ $language->name }}
              </span>
            @endforeach
          </td>
          <td>
            @if(!$row->profile_is_complete)
              <span class="badge badge-warning">
                {{ !$row->profile_is_complete ? __('Profile not completed') : '' }}
              </span>
            @else
              <span class="badge {{ $row->is_active ? 'badge-success' : 'badge-danger' }}">
                {{ $row->is_active ? __('Available') : __('Disabled') }}
              </span>
            @endif
          </td>
          @if($status->value() != 'preenrolled')
          <td>
            {{ $row->last_login_at ? $row->last_login_at->diffForHumans() : __('Never') }}
          </td>
          @endif
          <td class="text-left">
            @if($status->value() != 'retired')
              @if(!$row->trashed())
                @can('students.edit')
                  <a
                    href="{{ tenant_route('students.edit', [$status->value(), $row->hid]) }}"
                    class="btn btn-sm btn-primary btn-edit m-r-5"
                    data-toggle="tooltip"
                    data-original-title="{{__('Edit')}}"
                  >
                    <i class="fa fa-pencil font-14"></i>
                  </a>
                @endcan
              @else
                <button
                    type="button"
                    data-id="{{ $row->hid }}"
                    class="btn btn-sm btn-primary btn-restore m-r-5"
                    data-toggle="tooltip"
                    data-type="restore"
                    data-original-title="{{__('Restore')}}">
                        <i class="fa fa-history font-14"></i>
                </button>
              @endif
            @endif
            @can('students.manager')
              @if (!$row->trashed())
                @includeWhen($row->profile_is_complete, 'elements._btn-dropdown')
              @endif
            @endcan
            @can('students.delete')
              <button
                type="button"
                data-id="{{ $row->hid }}"
                class="btn btn-sm btn-danger btn-delete"
                data-toggle="tooltip"
                data-original-title="{{__('Remove')}}"
              >
                <i class="fa fa-trash font-14"></i>
              </button>
            @endcan
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
</div>
