
@extends('layouts/default')

@section('title', __('Create new :name', [ 'name' => __('Role') ]))

@section('page-style')

@endsection

@section('content')
<section id="basic-datatable">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-content">
            <div class="card-body card-dashboard">
              {!! Form::open(['url' => tenant_route('roles.store'), 'id' => 'form']) !!}
                @include('app.role._form')
                <hr>
                <div class="form-group text-right">
                    <button class="btn btn-success">{{ __('Create :name', [ 'name' => __('Role') ]) }}</button>
                    <a href="#" onclick="history.back();" class="btn btn-link text-muted">{{ __('Cancel') }}</a>
                </div>
              {!! Form::close() !!}
            </div>
          </div>
        </div>
      </div>
    </div>
</section>
@endsection

@section('vendor-script')
  @parent
  <!-- vendor files -->
  <script src="{{ asset(mix('vendors/js/validation/jquery.validate.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/validation/localization/messages_'.\App::getLocale().'.js')) }}"></script>
@endsection
@section('page-script')
  @parent
  <!-- Page js files -->
  <script src="{{ asset(mix('js/scripts/validate-form.js')) }}"></script>
@endsection
