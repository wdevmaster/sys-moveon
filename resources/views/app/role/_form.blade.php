<div class="form-group">
  {{ Form::text(
          'name',
          old('name'),
          [
              'id' => 'name',
              'class' => 'form-control',
              'placeholder' => __('Name of the :name', [ 'name' => __('role') ]),
              'required',
              'autofocus'
          ]
      )
  }}
</div>

@include('elements.form._permission', [
  'title' => __('Role permissions'),
  'show' => true,
  'data' => $role ?? null
])
