
@extends('layouts/default')

@section('title', __('Halls'))

@section('vendor-style')
  {{-- vendor css files --}}
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/animate/animate.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
@endsection

@section('content')
<section id="basic-datatable">
    <div class="row">
      <div class="col-4">
        @include('app.hall.form')
      </div>
      <div class="col-8">
        <div class="card">
          <div class="card-content">
            <div class="card-body card-dashboard">
              <div class="table-responsive">
                <table class="table">
                  <thead>
                      <tr>
                        <th class="no-sort">{{__('Name')}}</th>
                        @if(Auth::user()->branches->count() > 1)
                          <th class="no-sort">{{__('Branch')}}</th>
                        @endif
                        <th class="no-sort">{{__('Capacity')}}</th>
                        <th class="no-sort">{{__('Status')}}</th>
                        <th class="no-sort" style="width: 15%">&nbsp;</th>
                      </tr>
                  </thead>
                  <tbody>
                    @foreach($rows as $row)
                      <tr id="row-{{ $row->hid }}">
                        <td>{{ $row->name }}</td>
                        @if(Auth::user()->branches->count() > 1)
                          <td>
                            <span class="badge badge-info">
                              {{ $row->branch->name }}
                            </span>
                          </td>
                        @endif
                        <td>
                          {{ $row->capacity }}
                        </td>
                        <td>
                          <span
                            class="
                              badge
                              {{
                                $row->status->is_available
                                    ? 'badge-success'
                                    : 'badge-danger'
                              }}
                            "
                          >
                            {{ $row->status }}
                          </span>
                        </td>
                        <td class="text-left">
                          @if(!$row->trashed())
                            @can('halls.edit')
                              <button
                                type="button"
                                data-id="{{ $row->hid }}"
                                class="btn btn-sm btn-primary btn-edit m-r-5"
                                data-toggle="tooltip"
                                data-original-title="{{__('Edit')}}">
                                  <i class="fa fa-pencil font-14"></i>
                              </button>
                            @endcan
                          @else
                            <button
                                type="button"
                                data-id="{{ $row->hid }}"
                                class="btn btn-sm btn-primary btn-restore m-r-5"
                                data-toggle="tooltip"
                                data-type="restore"
                                data-original-title="{{__('Restore')}}">
                                    <i class="fa fa-history font-14"></i>
                            </button>
                          @endif
                          @can('halls.manager')
                            @includeWhen(!$row->trashed(), 'elements._btn-dropdown')
                          @endcan
                          @can('halls.destroy')
                            <button
                              type="button"
                              data-id="{{ $row->hid }}"
                              class="btn btn-sm btn-danger btn-delete"
                              data-toggle="tooltip"
                              data-original-title="{{
                                !$row->trashed()
                                    ? __('Remove')
                                    : __('Permanently deleting')
                            }}"
                            >
                              @if(!$row->trashed())
                                <i class="fa fa-trash-o font-14"></i>
                              @else
                                <i class="fa fa-trash font-14"></i>
                              @endif
                            </button>
                          @endcan
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</section>
@endsection
@section('vendor-script')
{{-- vendor files --}}
  <script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/extensions/polyfill.min.js')) }}"></script>
@endsection
@section('page-script')
<script>

  var routeDelete = "{{ tenant_route('halls.destroy', ':id') }}";
  var routeShow = "{{ tenant_route('halls.show', ':id') }}";
  var routeIndex = "{{ tenant_route('halls.index') }}";
  var csrfToken = "{{csrf_token()}}";
  var configTable = {
    ordering: false,
    columnDefs: [{
      targets: 'sort',
      orderable: true,
    }],
    language: {
      url: "{{ asset('json/datatable/'.App::getLocale().'.json') }}"
    }
  }

  var lang = {
    are_you_sure : "{{__('Are you sure?')}}",
    yes_sure :"{{__('Yes, sure!')}}",
    no_cancel: "{{__('No, cancel!')}}",
    updated: "{{__('Removed!')}}",
    ok: "{{__('Ok')}}",
    oops: "{{ __('Oops...') }}"
  }

  $('.btn-edit').click(function () {
      let id = $(this).data('id')

      $.ajax({
        url: routeShow.replace(':id', id),
        success: function(data) {
          $.each(data, function(k, v) {
            if (k == 'branch_id')
              $(`select#${k} option[value='${v}']`).attr("selected", true);
            else
              $(`input#${k}`).val(v)
          })

          $('#action-title').text("{{ __('Edit') }}")
        }
      });
  })
</script>
<script src="{{ asset(mix('js/scripts/basic-crud.js')) }}"></script>
@endsection
