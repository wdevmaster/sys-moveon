<div class="card">
  <div class="card-content">
    <div class="card-header">
      <h4 class="card-title">
        <span id="action-title">{{ __('Add New') }}</span> {{ __('Hall') }}
      </h4>
    </div>
    <div class="card-body">
      {!! Form::open(['method' => 'POST', 'url' => tenant_route('halls.sync'), 'id' => 'form-hall']) !!}
        @if ($errors->any())
          <div class="alert alert-danger">
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        @endif
        <div class="row">
          {!! Form::hidden('id', old('id'), ['id' => 'id']) !!}

          <div class="form-group col-sm-12">
            <label>{{ __('Branch') }}</label>
            @if (!$franchise->is_multibranch)
              {{ Form::hidden('branch_id', $branches->first()->id) }}
            @endif
            {{
              Form::select(
                'branch_id',
                $branches->pluck('name', 'id'),
                old('branch_id') ?? (!$franchise->is_multibranch ? $branches->first()->id : null),
                [
                  'id' => 'branch_id',
                  'class' => 'form-control'.($errors->has('branch') ? ' is-invalid' : '' ),
                  'placeholder' => __('Select :name', [ 'name' => __('Branch') ]),
                  'required',
                  !$franchise->is_multibranch ? 'disabled' : ''
                ]
              )
            }}
          </div>

          <div class="form-group col-sm-8">
            <label>{{ __('Name') }}</label>
            {{
              Form::text(
                'name',
                old('name'),
                [
                  'id' => 'name',
                  'class' => 'form-control'.($errors->has('name') ? ' is-invalid' : '' ),
                  'required',
                ]
              )
            }}
          </div>

          <div class="form-group col-sm-4">
            <label>{{ __('Capacity') }}</label>
            {{
              Form::number(
                'capacity',
                old('capacity'),
                [
                  'id' => 'capacity',
                  'class' => 'form-control'.($errors->has('capacity') ? ' is-invalid' : '' ),
                  'required',
                ]
              )
            }}
          </div>
        </div>
        @if(!request()->is('**/show'))
          <hr>
          <div class="form-group text-right">
            <button class="btn btn-success">{{ __('Save') }}</button>
            <button id="btn-cancel" type="button" class="btn btn-link text-muted">{{ __('Cancel') }}</button>
          </div>
        @endif
      {!! Form::close() !!}
    </div>
  </div>
</div>

@section('page-script')
  @parent
  <script>
    $("#capacity").change(function () {
      if ($(this).val() < 0)
        $(this).val(0);
    });

    $('#btn-cancel').click(function () {
      $('#action-title').text("{{ __('Add New') }}")
      $(`select option`).attr("selected", false)
      $( "#form-hall" ).trigger("reset");
    });
  </script>
@endsection

