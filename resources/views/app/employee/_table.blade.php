<div class="table-responsive">
  <table class="table">
    <thead>
        <tr>
          <th></th>
          <th class="sort">{{__('Name')}}</th>
          <!--
          <th>{{__('E-Mail')}}</th>
          -->
          @if($franchise->is_multibranch)
            <th  class="sort">{{__('Branch')}}</th>
          @endif
          <th  class="sort">{{__('Role')}}</th>
          <th  class="sort">{{__('Status')}}</th>
          <th>{{__('Last login')}}</th>
          <th class="no-sort" style="width: 15%">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
      @foreach($rows as $row)
        <tr id="row-{{ $row->hid }}">
          <td>
            <img class="round"
              src="{{ $row->profile_photo }}"
              alt="avatar"
              height="40"
              width="40"
              class="m-r-1"
            />
          </td>
          <td>
            <a href="{{ tenant_route('employees.show', ['role' => $role, $row->hid]) }}">
              {{ $row->fullname }}
            </a>
          </td>
          <!--
          <td>
            {{ $row->email }}
          </td>
          -->
          @if($franchise->is_multibranch)
            <td>
              @forelse ($row->branches as $branch)
                <span class="badge badge-info">
                  {{ $branch->name }}
                </span>
              @empty
                <span class="badge badge-warning">
                  {{ __('Not assigned') }}
                </span>
              @endforelse
            </td>
          @endif
          <td>{{ $row->role->name }}</td>
          <td>
            <span class="badge {{ $row->is_active ? 'badge-success' : 'badge-danger' }}">
              {{ $row->is_active ? __('Available') : __('Disabled') }}
            </span>
          </td>
          <td>
            {{ $row->last_login_at ? $row->last_login_at->diffForHumans() : __('Never') }}
          </td>
          <td class="text-left">
            @if(!$row->trashed())
              @can('employees.edit')
                <a
                  href="{{ tenant_route('employees.edit', ['role' => $role, $row->hid]) }}"
                  class="btn btn-sm btn-primary btn-edit m-r-5"
                  data-toggle="tooltip"
                  data-original-title="{{__('Edit')}}"
                >
                  <i class="fa fa-pencil font-14"></i>
                </a>
              @endcan
            @else
              <button
                  type="button"
                  data-id="{{ $row->hid }}"
                  class="btn btn-sm btn-primary btn-restore m-r-5"
                  data-toggle="tooltip"
                  data-type="restore"
                  data-original-title="{{__('Restore')}}">
                      <i class="fa fa-history font-14"></i>
              </button>
            @endif
            @can('employees.manager')
              @includeWhen(!$row->trashed(), 'elements._btn-dropdown')
            @endcan
            @can('employees.destroy')
              <button
                type="button"
                data-id="{{ $row->hid }}"
                class="btn btn-sm btn-danger btn-delete"
                data-toggle="tooltip"
                data-original-title="{{
                  !$row->trashed()
                      ? __('Remove')
                      : __('Permanently deleting')
              }}"
              >
                @if(!$row->trashed())
                  <i class="fa fa-trash-o font-14"></i>
                @else
                  <i class="fa fa-trash font-14"></i>
                @endif
              </button>
            @endcan
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
</div>
