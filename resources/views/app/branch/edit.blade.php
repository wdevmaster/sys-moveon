
@extends('layouts/default')

@section('title', __('Editing :name', [ 'name' => __('Branch') ]).' '. $data->name)

@section('content')
<section id="basic-datatable">
  <div class="row">
    <div class="col-sm-12">
      <div class="card overflow-hidden">
        <div class="card-content">
          <div class="card-body">
            {!!
              Form::model(
                $data,
                [
                  'url' => tenant_route('branches.update', $data->hid),
                  'method' => 'PUT',
                  'id' => 'form'
                ]
              )
            !!}
              <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                  <a
                    class="nav-link active"
                    id="general-tab"
                    data-toggle="tab"
                    href="#general"
                    aria-controls="general"
                    role="tab"
                    aria-selected="true"
                  >
                    {{ __('General') }}
                  </a>
                </li>
                <li class="nav-item">
                  <a
                    class="nav-link"
                    id="general-tab"
                    data-toggle="tab"
                    href="#program"
                    aria-controls="program"
                    role="tab"
                    aria-selected="true"
                  >
                    {{ __('Programs') }}
                  </a>
                </li>
              </ul>
              <div class="tab-content pt-2">
                <div class="tab-pane active" id="general" aria-labelledby="general-tab" role="tabpanel">
                  @include('app.branch._form')
                </div>
                <div class="tab-pane" id="program" aria-labelledby="program-tab" role="tabpanel">
                  @include('app.program._form', ['data' => $data->programs->pluck('name', 'id')])
                </div>
              </div>
              @if(!request()->is('**/show'))
              <hr>
              <div class="form-group text-right">
                <button class="btn btn-success">{{ __('Save') }}</button>
                <a href="#" onclick="history.back();" class="btn btn-link text-muted">{{ __('Cancel') }}</a>
              </div>
              @endif
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('page-style')
  <!-- Page css files -->

@endsection
@section('vendor-script')
  <!-- vendor files -->
  @if (!request()->is('app/teachers/*/*/show'))
    <script src="{{ asset(mix('vendors/js/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/validation/localization/messages_'.\App::getLocale().'.js')) }}"></script>
  @endif
@endsection
@section('page-script')
@parent
<!-- Page js files -->
  @if (request()->is('app/*/branches/*/show'))
    <script>
      $('#form').removeAttr('action')
      $('.gruop-btn').addClass('d-none')
      $('#form input').attr('disabled', 'disabled')
      $('#form select').attr('disabled', 'disabled')
      $.each($('.vs-radio-con input[type=radio]'), function (i, radio) {
        if (!$(radio).is( ':checked' ))
          $(radio).parent().parent().parent().remove()
      })
    </script>
  @else
    <script src="{{ asset(mix('js/scripts/validate-form.js')) }}"></script>
  @endif
@endsection
