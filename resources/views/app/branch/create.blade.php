
@extends('layouts/default')

@section('title', __('Create new :name', [ 'name' => __('Branch') ]))

@section('content')
<section id="basic-datatable">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-content">
            <div class="card-body card-dashboard">
              {!!
                Form::open([
                  'url' => tenant_route('branches.store'),
                  'id' => 'form',
                  'class' => 'steps-validation wizard-circle'
                ])
              !!}
                <!-- Step 1 -->
                <h6><i class="step-icon feather icon-home"></i> {{ __('Branch') }}</h6>
                <fieldset>
                  @include('app.branch._form')
                </fieldset>

                <!-- Step 2 -->
                <h6><i class="step-icon feather icon-briefcase"></i> {{ __('Programs') }}</h6>
                <fieldset>
                  @include('app.program._form')
                </fieldset>

                <!-- Step 3 --
                <h6><i class="step-icon feather icon-image"></i> Step 3</h6>
                <fieldset>
                  <div class="row">
                  </div>
                </fieldset>
                -->
              {!! Form::close() !!}
            </div>
          </div>
        </div>
      </div>
    </div>
</section>
@endsection

@section('page-style')
  @parent
  <!-- Page css files -->
  <link rel="stylesheet" href="{{ asset(mix('css/plugins/forms/wizard.css')) }}">
@endsection
@section('vendor-script')
  @parent
  <!-- vendor files -->
  <script src="{{ asset(mix('vendors/js/extensions/jquery.steps.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/validation/jquery.validate.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/validation/localization/messages_'.\App::getLocale().'.js')) }}"></script>
@endsection
@section('page-script')
  @parent
  <!-- Page js files -->
  <script src="{{ asset(mix('js/scripts/validate-form.js')) }}"></script>
@endsection
