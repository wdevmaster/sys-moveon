
@extends('layouts/default')

@section('title', __('Branches'))

@section('vendor-style')
  {{-- vendor css files --}}
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/animate/animate.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
@endsection

@section('btn-action')
  @can('branches.create')
    <a href="{{ tenant_route('branches.create') }}" class="btn btn-outline-primary">
      <span>
        <i class="feather icon-plus"></i> {{ __('Add New') }}
      </span>
    </a>
  @endcan
@endsection

@section('content')
<section id="basic-datatable">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-content">
            <div class="card-body card-dashboard">
              <div class="table-responsive">
                <table class="table">
                  <thead>
                      <tr>
                        <th class="no-sort">{{__('Name')}}</th>
                        <th class="no-sort">{{__('Address')}}</th>
                        <th class="no-sort">{{__('Status')}}</th>
                        <th class="no-sort" style="width: 15%">&nbsp;</th>
                      </tr>
                  </thead>
                  <tbody>
                    @foreach($rows as $row)
                      <tr id="row-{{ $row->hid }}">
                        <td>{{ $row->name }}</td>
                        <td>{{ $row->full_address }}</td>
                        <td>
                          <sapn
                            class="badge badge-{{ !$row->trashed() ? 'success' : 'danger' }}"
                          >
                            {{ !$row->trashed() ? __('Active') : __('Disabled') }}
                          </sapn>
                        </td>
                        <td class="text-left">
                          @if(!$row->trashed())
                            @can('branches.edit')
                              <a
                                href="{{ tenant_route('branches.edit', $row->hid) }}"
                                class="btn btn-sm btn-primary btn-edit m-r-5"
                                data-toggle="tooltip"
                                data-original-title="{{__('Edit')}}"
                              >
                                <i class="fa fa-pencil font-14"></i>
                              </a>
                            @endcan
                          @else
                            <button
                                type="button"
                                data-id="{{ $row->hid }}"
                                class="btn btn-sm btn-primary btn-restore m-r-5"
                                data-toggle="tooltip"
                                data-type="restore"
                                data-original-title="{{__('Restore')}}">
                                    <i class="fa fa-history font-14"></i>
                            </button>
                          @endif
                          @can('branches.destroy')
                            <button
                              type="button"
                              data-id="{{ $row->hid }}"
                              class="btn btn-sm btn-danger btn-delete"
                              data-toggle="tooltip"
                              data-original-title="{{
                                !$row->trashed()
                                    ? __('Remove')
                                    : __('Permanently deleting')
                            }}"
                            >
                              @if(!$row->trashed())
                                <i class="fa fa-trash-o font-14"></i>
                              @else
                                <i class="fa fa-trash font-14"></i>
                              @endif
                            </button>
                          @endcan
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</section>
@endsection
@section('vendor-script')
{{-- vendor files --}}
  <script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/extensions/polyfill.min.js')) }}"></script>
@endsection
@section('page-script')
<script>
  var routeDelete = "{{ tenant_route('branches.destroy', ':id') }}";
  var routeRestore = "{{ tenant_route('branches.restore', ':id') }}";
  var routeIndex = "{{ tenant_route('branches.index') }}";
  var csrfToken = "{{csrf_token()}}";
  var configTable = {
    ordering: false,
    columnDefs: [{
      targets: 'sort',
      orderable: true,
    }],
    language: {
      url: "{{ asset('json/datatable/'.App::getLocale().'.json') }}"
    }
  }

  var lang = {
    are_you_sure : "{{__('Are you sure?')}}",
    yes_sure :"{{__('Yes, sure!')}}",
    no_cancel: "{{__('No, cancel!')}}",
    updated: "{{__('Updated!')}}",
    ok: "{{__('Ok')}}",
    oops: "{{ __('Oops...') }}"
  }
</script>
<script src="{{ asset(mix('js/scripts/basic-crud.js')) }}"></script>
@endsection
