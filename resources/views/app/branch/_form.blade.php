<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      <label for="name">{{ __('Name') }}</label>
      {{
        Form::text(
          'name',
          old('name'),
          [
            'id' => 'name',
            'class' => 'form-control',
            'placeholder' => __('Name of the :name', [ 'name' => __('Branch') ]),
            'required',
            'autofocus'
          ]
        )
      }}
    </div>
  </div>
</div>
<div class="divider divider-left">
  <div class="divider-text">
    <b>{{ __('Branch') }} {{ __('Address') }}</b>
  </div>
</div>
@include('elements.form._address', ['column' => true])
