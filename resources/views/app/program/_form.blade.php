<table class="table table-hover">
  <thead>
      <tr>
          <th>{{ __('Name') }}</th>
          <th>{{ __('Levels') }}</th>
          <th>{{ __('Activate') }}</th>
      </tr>
  </thead>
  <tbody>
    @foreach ($programs as $program)
      <tr>
          <th scope="row">
            {{ $program->name }}
          </th>
          <td>
            @foreach ($program->levels as $level)
              <div class="badge badge-pill badge-info">
                {{ $level->name }}
              </div>
            @endforeach
          </td>
          <td>
            <div class="vs-checkbox-con vs-checkbox-primary">
              {{
                Form::checkbox(
                  "programs[{$program->id}]",
                  true,
                  !isset($data)
                    ? true
                    : $data->has($program->id)
                )
              }}
              <span class="vs-checkbox vs-checkbox-lg">
                <span class="vs-checkbox--check">
                  <i class="vs-icon feather icon-check"></i>
                </span>
              </span>
            </div>
          </td>
      </tr>
    @endforeach
  </tbody>
</table>
