@extends('layouts/default')

@if (!request()->is('app/**/show'))
  @section('title', __('Editing :name', [ 'name' => __('Teacher') ]) .' '. $data->fullname)
@else
  @section('title', __('Teacher') .': '. $data->fullname)
@endif

@section('content')
<section id="basic-datatable">
  <div class="row">
    <div class="col-sm-12">
      <div class="card overflow-hidden">
        <div class="card-content">
          <div class="card-body">
            {!!
              Form::model($data,
                [
                  'url' => tenant_route('teachers.update', ['type' => $type->value(), $data->hid]),
                  'method' => 'PUT',
                  'id' => 'form',
                  'files' => true
                ]
              )
            !!}
              <input type="hidden" name="type" value="{{ $type->value() }}">
              <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                  <a
                    class="nav-link active"
                    id="account-tab"
                    data-toggle="tab"
                    href="#account"
                    aria-controls="account"
                    role="tab"
                    aria-selected="true"
                  >
                    {{ __('Account') }}
                  </a>
                </li>
                <li class="nav-item">
                  <a
                    class="nav-link"
                    id="information-tab"
                    data-toggle="tab"
                    href="#information"
                    aria-controls="information"
                    role="tab"
                    aria-selected="true"
                  >
                    {{ __('Information') }}
                  </a>
                </li>
                <li class="nav-item">
                  <a
                    class="nav-link"
                    id="add-information-tab"
                    data-toggle="tab"
                    href="#add-information"
                    aria-controls="add-information"
                    role="tab"
                    aria-selected="true"
                  >
                    {{ __('Additional Information') }}
                  </a>
                </li>
                <li class="nav-item">
                  <a
                    class="nav-link"
                    id="hours-tab"
                    data-toggle="tab"
                    href="#hours"
                    aria-controls="hours"
                    role="tab"
                    aria-selected="true"
                  >
                    {{ __('Hours of availability ') }}
                  </a>
                </li>
              </ul>
              <div class="tab-content pt-2">
                <div class="tab-pane active" id="account" aria-labelledby="account-tab" role="tabpanel">
                  <div class="row">
                    @include('elements.form.__input.avatar')
                    <div class="col-sm-10">
                      <div class="row">
                        <div class="col-12 col-sm-7">
                          <h5 class="mb-1 mt-2 mt-sm-0">
                            <i class="feather icon-user mr-25"></i>
                            {{ __('Personal Information') }}
                          </h5>
                          @include('elements.form._personal')

                          @if (!request()->is('app/*/teachers/postulant*'))
                            <h5 class="mb-1 mt-2 mt-sm-0">
                              <i class="feather icon-user mr-25"></i>
                              {{ __('Account Information') }}
                            </h5>

                            @includeWhen(
                              !request()->is('app/*/teachers/postulant*'),
                              'elements.form.__input.username'
                            )

                            @includeWhen(
                              !request()->is('app/*/teachers/postulant*'),
                              'elements.form.__input.email'
                            )

                            @includeWhen(
                              !request()->is('app/*/teachers/postulant*'),
                              'elements.form.__input.password'
                            )
                            @endif
                        </div>
                        <div class="col-12 col-sm-5">
                          <h5 class="mb-1 mt-2 mt-sm-0">
                            <i class="feather icon-map-pin mr-25"></i>
                            {{ __('Account settings') }}
                          </h5>
                          @includeWhen(request()->is(
                            'app/*/teachers*'),
                            'elements.form.__input.rating'
                          )
                          @include('elements.form.__input.role')

                          @include('elements.form.__input.branch', ['col' => 'col-12'])

                          @includeWhen(
                            request()->is('app/*/teachers*')
                            , 'elements.form.__input.program'
                          )

                          @includeWhen(
                            request()->is('app/*/teachers*'),
                            'elements.form.__input.student_type'
                          )
                          <div class="form-group col-sm-12">
                            <div class="row">
                              @includeWhen(
                                !request()->is('app/*/teachers/postulant*') &&
                                !request()->is('app/*/teachers/student*'),
                                'elements.form.__input.contract'
                              )
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="information" aria-labelledby="information-tab" role="tabpanel">
                  <div class="row">
                    <div class="col-12 col-sm-6">
                      <h5 class="mb-1 mt-2 mt-sm-0">
                        <i class="feather icon-map-pin mr-25"></i>
                        {{ __('Address') }}
                      </h5>
                      @include('elements.form._address')
                    </div>
                    <div class="col-12 col-sm-6">
                      <h5 class="mb-1 mt-2 mt-sm-0">
                        <i class="feather icon-target mr-25"></i>
                        {{ __('Academic Information') }}
                      </h5>
                      @include('elements.form._academic')
                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="hours" aria-labelledby="hours-tab" role="tabpanel">
                  @include('app.schedule._form')
                </div>
                <div class="tab-pane" id="add-information" aria-labelledby="add-information-tab" role="tabpanel">
                  <div class="row">
                    <div class="col-12 col-sm-6">
                      <h5 class="mb-1 mt-2 mt-sm-0">
                        <i class="feather icon-umbrella mr-25"></i>
                        {{ __('Health information') }}
                      </h5>
                      <x-form-log type="health" :select="$logTypes['health']"/>
                    </div>
                    <div class="col-12 col-sm-6">
                      <h5 class="mb-1 mt-2 mt-sm-0">
                        <i class="feather icon-message-square mr-25"></i>
                        {{ __('Recommendations and observations') }}
                      </h5>
                      <x-form-log type="recomm" :select="$logTypes['recomm']"/>
                    </div>
                  </div>
                </div>
              </div>
              <hr>
              {{
                Form::hidden(
                  'personalData[logs]',
                  old('personalData.logs'),
                  ['id' => 'logs']
                )
              }}
              <div class="form-group text-right gruop-btn">
                <button class="btn btn-success">{{ __('Save') }}</button>
                <a href="#" onclick="history.back();" class="btn btn-link text-muted">{{ __('Cancel') }}</a>
              </div>
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('page-style')
  <!-- Page css files -->

@endsection
@section('vendor-script')
  <!-- vendor files -->
  @if (!request()->is('app/teachers/*/*/show'))
    <script src="{{ asset(mix('vendors/js/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/validation/localization/messages_'.\App::getLocale().'.js')) }}"></script>
  @endif
@endsection
@section('page-script')
  @parent
  <script>
    var disabled = ""
    let logs = JSON.parse($('#logs').val());
    const langLogType = {!! json_encode($langLogType, JSON_UNESCAPED_SLASHES) !!}
  </script>
  <script src="{{ asset(mix('js/scripts/form-log.js')) }}"></script>
  <script>
    renderLogs('recomm')
    renderLogs('health')
  </script>
  <!-- Page js files -->
  @if (request()->is('app/**/show'))
    <script>
      $('#form').removeAttr('action')
      $('.gruop-btn').addClass('d-none')
      $('#form input').attr('disabled', 'disabled')
      $('#form select').attr('disabled', 'disabled')
      $.each($('.vs-radio-con input[type=radio]'), function (i, radio) {
        if (!$(radio).is( ':checked' ))
          $(radio).parent().parent().parent().remove()
      })
      $('.group-logs').addClass('d-none');
      $('.log-remove').addClass('d-none');
      $('.schedules').attr('disabled', 'disabled')
    </script>
  @else
    <script src="{{ asset(mix('js/scripts/validate-form.js')) }}"></script>
  @endif
@endsection
