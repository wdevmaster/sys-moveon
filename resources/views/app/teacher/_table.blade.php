<div class="table-responsive">
  <table class="table">
    <thead>
        <tr>
          <th></th>
          <th>{{__('Ratings')}}</th>
          <th>{{__('CI')}}</th>
          <th class="sort">{{__('Name')}}</th>
          @if($franchise->is_multibranch)
            <th>
              {{
                  $type->value() != 'postulant' ? __('Branch') : __('Application in')
              }}
            </th>
          @endif
          <th  class="sort">{{__('Status')}}</th>
          @if($type->value() != 'postulant')
            <th>{{ $type->value() != 'retired' ?  __('Last login') : __('Retired') }}</th>
          @endif
          <th class="no-sort" style="width: 15%">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
      @foreach($rows as $row)
        <tr id="row-{{ $row->hid }}">
          <td>
            <img class="round"
              src="{{ $row->profile_photo }}"
              alt="avatar"
              height="40"
              width="40"
              class="m-r-1"
            />
          </td>
          <td>{{ $row->ratings }}Pts</td>
          <td>
            <a href="{{ tenant_route('teachers.show', [$type->value(), $row->hid]) }}">
              {{ $row->personalData->ci }}
            </a>
          </td>
          <td>
            {{ $row->fullname }}
          </td>
          @if($franchise->is_multibranch)
            <td>
              @forelse ($row->branches as $branch)
                <span class="badge badge-info">
                  {{ $branch->name }}
                </span>
              @empty
                <span class="badge badge-warning">
                  {{ __('Not assigned') }}
                </span>
              @endforelse
            </td>
          @endif
          @if($type->value() != 'postulant')
          <td>
            <span class="badge {{ $row->is_active ? 'badge-success' : 'badge-danger' }}">
              {{ $row->is_active ? __('Available') : __('Disabled') }}
            </span>
          </td>
          <td>
            {{ $row->last_login_at ? $row->last_login_at->diffForHumans() : __('Never') }}
          </td>
          @else
            <td>
              @if($row->status->is_approved)
                <span class="badge badge-success">
              @elseif($row->status->is_rejected)
                <span class="badge badge-danger">
              @else
                <span class="badge badge-secondary">
              @endif
                {{ $row->status }}
              </span>
            </td>
          @endif
          <td class="text-left">
            @if($type->value() != 'retired')
              @if(!$row->trashed())
                @can('teachers.edit')
                  <a
                    href="{{ tenant_route('teachers.edit', [$type->value(), $row->hid]) }}"
                    class="btn btn-sm btn-primary btn-edit m-r-5"
                    data-toggle="tooltip"
                    data-original-title="{{__('Edit')}}"
                  >
                    <i class="fa fa-pencil font-14"></i>
                  </a>
                @endcan
              @else
                <button
                    type="button"
                    data-id="{{ $row->hid }}"
                    class="btn btn-sm btn-primary btn-restore m-r-5"
                    data-toggle="tooltip"
                    data-type="restore"
                    data-original-title="{{__('Restore')}}">
                      <i class="fa fa-history font-14"></i>
                </button>
              @endif
            @endif
            @can('teachers.manager')
              @includeWhen(!$row->trashed(), 'elements._btn-dropdown')
            @endcan
            @can('teachers.delete')
              <button
                type="button"
                data-id="{{ $row->hid }}"
                class="btn btn-sm btn-danger btn-delete"
                data-toggle="tooltip"
                data-original-title="{{__('Remove')}}"
              >
                <i class="fa fa-trash font-14"></i>
              </button>
            @endcan
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
</div>
