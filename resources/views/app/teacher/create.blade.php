@extends('layouts/default')

@if ($type->isPostulant())
  @section('title', __('Create new :name', [ 'name' => $type.' '.__('Teacher') ]))
@else
  @section('title', __('Create new :name', [ 'name' => __('Teacher') ]))
@endif

@section('content')
<section id="basic-datatable">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-content">
          <div class="card-body card-dashboard">
            {!!
              Form::open([
                'url' => tenant_route('teachers.store', ['type' => $type->value()]),
                'id' => 'form',
                'class' => 'steps-validation wizard-circle',
                'files' => true
              ])
            !!}
              <input type="hidden" name="type" value="{{ $type->value() }}">
              <input
                type="hidden"
                name="user[status]"
                value="{{ $type->value() == 'postulant' ? 'on_hold' : 'approved' }}"
              >

              <!-- Step 1 -->
              <h6>
                <i class="step-icon feather icon-info"></i>
                {{ __('Information') }}
              </h6>
              <fieldset class="py-1">
                <div class="row">
                  @include('elements.form.__input.avatar')
                  <div class="col-sm-10">
                    <div class="row">
                      <div class="col-12 col-sm-7">
                        <h5 class="mb-1 mt-2 mt-sm-0">
                          <i class="feather icon-user mr-25"></i>
                          {{ __('Personal Information') }}
                        </h5>
                        @include('elements.form._personal')
                      </div>
                      <div class="col-12 col-sm-5">
                        <h5 class="mb-1 mt-2 mt-sm-0">
                          <i class="feather icon-map-pin mr-25"></i>
                          {{ __('Address') }}
                        </h5>
                        @include('elements.form._address')
                      </div>
                    </div>
                  </div>
                </div>
              </fieldset>

              <!-- Step 2 -->
              <h6>
                <i class="step-icon feather icon-hash"></i>
                {{ __('Additional Information') }}
              </h6>
              <fieldset class="py-1">
                <div class="row">
                  <div class="col-12 col-sm-6">
                    <h5 class="mb-1 mt-2 mt-sm-0">
                      <i class="feather icon-target mr-25"></i>
                      {{ __('Academic Information') }}
                    </h5>
                    @include('elements.form._academic')
                  </div>
                  <div class="col-12 col-sm-6">
                    <h5 class="mb-1 mt-2 mt-sm-0">
                      <i class="feather icon-umbrella mr-25"></i>
                      {{ __('Health information') }}
                    </h5>
                    <x-form-log type="health" :select="$logTypes['health']"/>
                  </div>
                </div>
              </fieldset>

              <h6>
                <i class="step-icon feather icon-clock"></i>
                {{ __('Hours of availability ') }}
              </h6>
              <fieldset class="py-1">
                @include('app.schedule._form')
              </fieldset>

              <!-- Step 4 -->
              <h6>
                <i class="step-icon feather icon-user"></i>
                {{ __('Account') }}
              </h6>
              <fieldset class="py-1">
                @include('elements.form._user')
              </fieldset>

              {{
                Form::hidden(
                  'personalData[logs]',
                  old('personalData.logs'),
                  ['id' => 'logs']
                )
              }}
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('page-style')
  @parent
  <!-- Page css files -->
  <link rel="stylesheet" href="{{ asset(mix('css/plugins/forms/wizard.css')) }}">
@endsection
@section('vendor-script')
  @parent
  <!-- vendor files -->
  <script src="{{ asset(mix('vendors/js/extensions/jquery.steps.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/validation/jquery.validate.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/validation/localization/messages_'.\App::getLocale().'.js')) }}"></script>
@endsection
@section('page-script')
  @parent
  <!-- Page js files -->
  <script src="{{ asset(mix('js/scripts/validate-form.js')) }}"></script>
  <script>
    //var disabled = "{{request()->is('*/show') ? 'd-none' : '' }}"
    var disabled = ""
    let logs = $('#logs').val() != '' ? JSON.parse($('#logs').val()) : [];
    const langLogType = {!! json_encode($langLogType, JSON_UNESCAPED_SLASHES) !!}
  </script>
  <script src="{{ asset(mix('js/scripts/form-log.js')) }}"></script>
  <script>
    renderLogs('recomm')
    renderLogs('health')
  </script>
@endsection
