@extends('layouts/default')

@section('title', __('Create new :name', [ 'name' => __('Payment plan') ]))

@section('content')
<section id="basic-datatable">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-content">
          <div class="card-body card-dashboard">
            {!!
              Form::open([
                'url' => tenant_route('plans.store'),
                'id' => 'form',
                'class' => 'steps-validation wizard-circle',
                'files' => true
              ])
            !!}
              <!-- Step 1 -->
              <h6>
                <i class="step-icon feather icon-info"></i>
                {{ __('Information') }}
              </h6>
              <fieldset class="py-1">
                <div class="row">
                  <div class="col-12 col-sm-6">
                    <h5 class="mb-1 mt-2 mt-sm-0">
                      <i class="feather icon-credit-card mr-25"></i>
                      {{ __('Plan Information') }}
                    </h5>
                    <div class="row">
                      @include('app.plan._config')
                    </div>
                  </div>
                  <div class="col-12 col-sm-6">
                    <h5 class="mb-1 mt-2 mt-sm-0">
                      <i class="feather icon-check-square mr-25"></i>
                      {{ __('Features') }}
                    </h5>
                    <div class="row">
                      @include('app.plan._features')
                    </div>
                  </div>
                  </div>
                </div>
              </fieldset>

            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('page-style')
  @parent
  <!-- Page css files -->
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/select/select2.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/plugins/forms/wizard.css')) }}">
@endsection
@section('vendor-script')
  @parent
  <!-- vendor files -->
  <script src="{{ asset(mix('vendors/js/extensions/jquery.steps.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/validation/jquery.validate.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/validation/localization/messages_'.\App::getLocale().'.js')) }}"></script>
@endsection
@section('page-script')
  @parent
  <!-- Page js files -->
  <script src="{{ asset(mix('vendors/js/select/select2.full.min.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/validate-form.js')) }}"></script>
@endsection
