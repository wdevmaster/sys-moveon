<div class="table-responsive">
  <table class="table">
    <thead>
        <tr>
          <th>{{__('Code')}}</th>
          <th>{{__('Name')}}</th>
          @if($franchise->is_multibranch)
            <th>{{ __('Branches') }}</th>
          @endif
          <th>{{__('Type')}}</th>
          <th>{{__('Default')}}</th>
          <th class="no-sort" style="width: 15%">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
      @foreach($rows as $row)
        <tr id="row-{{ $row->hid }}">
          <td>{{ $row->code }}</td>
          <td>{{ $row->name }}</td>
          @if($franchise->is_multibranch)
            <td>
              @foreach ($row->branches as $branch)
                <span class="badge badge-info">
                  {{ $branch->name }}
                </span>
              @endforeach
            </td>
          @endif
          <td>{{ $row->interval_unit }}</td>
          <td>
            <span class="badge {{ $row->is_default ? 'badge-success' : 'badge-danger' }}">
              {{ $row->is_default ? __('Yes') : __('No') }}
            </span>
          </td>
          <td class="text-left">
            @if(!$row->trashed())
              @can('teachers.edit')
                <a
                  href="{{ tenant_route('plans.edit', [$row->hid]) }}"
                  class="btn btn-sm btn-primary btn-edit m-r-5"
                  data-toggle="tooltip"
                  data-original-title="{{__('Edit')}}"
                >
                  <i class="fa fa-pencil font-14"></i>
                </a>
              @endcan
            @else
              <button
                  type="button"
                  data-id="{{ $row->hid }}"
                  class="btn btn-sm btn-primary btn-restore m-r-5"
                  data-toggle="tooltip"
                  data-type="restore"
                  data-original-title="{{__('Restore')}}">
                    <i class="fa fa-history font-14"></i>
              </button>
            @endif
            @can('teachers.delete')
              <button
                type="button"
                data-id="{{ $row->hid }}"
                class="btn btn-sm btn-danger btn-delete"
                data-toggle="tooltip"
                data-original-title="{{__('Remove')}}"
              >
                <i class="fa fa-trash font-14"></i>
              </button>
            @endcan
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
</div>
