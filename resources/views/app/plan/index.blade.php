
@extends('layouts/default')

@section('title', __('Payment plans'))

@section('btn-action')
  @can('plans.create')
    <a
      href="{{ tenant_route('plans.create') }}"
      class="btn btn-outline-primary"
    >
      <span>
        <i class="feather icon-plus"></i> {{ __('Add New') }}
      </span>
    </a>
  @endcan
@endsection

@section('content')
<section id="basic-datatable">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-content">
          <div class="card-body card-dashboard">
            <ul class="nav nav-tabs" role="tablist">
              <li class="nav-item">
                <a
                  class="nav-link active"
                  id="active-tab"
                  data-toggle="tab"
                  href="#active"
                  aria-controls="active"
                  role="tab"
                  aria-selected="true"
                >
                  {{ __('Active') }}
                </a>
              </li>
              <li class="nav-item">
                <a
                  class="nav-link"
                  id="trash-tab"
                  data-toggle="tab"
                  href="#trash"
                  aria-controls="trash"
                  role="tab"
                  aria-selected="true"
                >
                  {{ __('In trash') }}
                </a>
              </li>
            </ul>
            <div class="tab-content pt-2">
              <div class="tab-pane active" id="active" aria-labelledby="active-tab" role="tabpanel">
                @include('app.plan._table', ['rows' => $rows['active']])
              </div>
              <div class="tab-pane" id="trash" aria-labelledby="trash-tab" role="tabpanel">
                @include('app.plan._table', ['rows' => $rows['trash']])
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('vendor-style')
  {{-- vendor css files --}}
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/animate/animate.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
@endsection
@section('vendor-script')
  {{-- vendor files --}}
  <script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/extensions/polyfill.min.js')) }}"></script>
@endsection
@section('page-script')
  @parent
  @include('elements.langJs')
  <script>
    var routeDelete = "{{ tenant_route('plans.destroy', [':id']) }}";
    var routeRestore = "{{ tenant_route('plans.restore', [':id']) }}";
    var routeIndex = "{{ tenant_route('plans.index') }}";
    var csrfToken = "{{csrf_token()}}";
    var configTable = {
      ordering: false,
      columnDefs: [{
        targets: 'sort',
        orderable: true,
      }],
      language: {
        url: "{{ asset('json/datatable/'.App::getLocale().'.json') }}"
      }
    }
  </script>
  <script src="{{ asset(mix('js/scripts/basic-crud.js')) }}"></script>
@endsection
