@extends('layouts/default')

@section('title', __('Editing :name', [ 'name' => __('Payment plan') ]) .': '. $data->name)

@section('content')
<section id="basic-datatable">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-content">
          <div class="card-body card-dashboard">
            {!!
              Form::model($data,
                [
                  'url' => tenant_route('plans.update', [$data->hid]),
                  'method' => 'PUT',
                  'id' => 'form',
                  'files' => true
                ]
              )
            !!}
              <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                  <a
                    class="nav-link active"
                    id="plan-tab"
                    data-toggle="tab"
                    href="#plan"
                    aria-controls="plan"
                    role="tab"
                    aria-selected="true"
                  >
                    {{ __('Information') }}
                  </a>
                </li>
              </ul>
              <div class="tab-content pt-2">
                <div class="tab-pane active" id="plan" aria-labelledby="plan-tab" role="tabpanel">
                  <div class="row">
                    <div class="col-12 col-sm-6">
                      <h5 class="mb-1 mt-2 mt-sm-0">
                        <i class="feather icon-credit-card mr-25"></i>
                        {{ __('Plan Information') }}
                      </h5>
                      <div class="row">
                        @include('app.plan._config')
                      </div>
                    </div>
                    <div class="col-12 col-sm-6">
                      <h5 class="mb-1 mt-2 mt-sm-0">
                        <i class="feather icon-check-square mr-25"></i>
                        {{ __('Features') }}
                      </h5>
                      <div class="row">
                        @include('app.plan._features')
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <hr>
              <div class="form-group text-right gruop-btn">
                <button class="btn btn-success">{{ __('Save') }}</button>
                <a href="#" onclick="history.back();" class="btn btn-link text-muted">{{ __('Cancel') }}</a>
              </div>
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('vendor-script')
  @parent
  <!-- vendor files -->
  <script src="{{ asset(mix('vendors/js/validation/jquery.validate.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/validation/localization/messages_'.\App::getLocale().'.js')) }}"></script>
@endsection

