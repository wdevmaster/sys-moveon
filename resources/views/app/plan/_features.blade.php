<div class="col-sm-6 form-group">
  <label>{{ __('Type') }}</label>
  <select id="feature_type" class="form-control">
    @foreach ($featureTypes as $value => $text)
      <option value="{{ $value }}">{{ $text }}</option>
    @endforeach
  </select>
</div>

<div class="col-sm-6 form-group">
  <label>{{ __('Apply to') }}</label>
  <select id="feature_origin" class="form-control">
    @foreach ($featureOrigin as $value => $text)
      <option value="{{ $value }}">{{ $text }}</option>
    @endforeach
  </select>
</div>

<div class="form-group col-sm-3 pr-0">
  <div class="controls">
    <label>{{ __('Value type') }}</label>
    <ul class="list-unstyled mb-0">
      <li class="d-inline-block" style="margin-right: 3px;">
        <fieldset>
         <div class="vs-radio-con">
          <input type="radio" name="percentage" class="is_percentage" value="0" checked>
            <span class="vs-radio">
              <span class="vs-radio--border"></span>
              <span class="vs-radio--circle"></span>
            </span>
            <span class="">$</span>
          </div>
        </fieldset>
      </li>
      <li class="d-inline-block mr-2">
        <fieldset>
         <div class="vs-radio-con">
            <input type="radio" name="percentage" class="is_percentage" value="1">
            <span class="vs-radio">
              <span class="vs-radio--border"></span>
              <span class="vs-radio--circle"></span>
            </span>
            <span class="">%</span>
          </div>
        </fieldset>
      </li>
    </ul>
  </div>
</div>

<div class="form-group col-sm-3">
  <label>{{ __('Value') }}</label>
  <input type="text" id="feature_value"  class="form-control money" placeholder="0.00">
</div>

<div class="form-group col-sm-6 text-right pt-1">
  <button type="button" id="btn-add" class="btn btn-info">Add</button>
</div>

<div class="col-12">
  <hr>
  <div id="list-features" class="list-group">
    <div class="list-group-item list-empty text-center" style="background-color: whitesmoke">
      <div class="d-flex w-100 justify-content-between">
        <!--
        <h5 class="mb-0">title</h5>
        <small>
          <a
            href="#"
            class="log-remove"
            data-key=""
            data-group=""
            data-toggle="tooltip"
            data-original-title="{{__('Remove')}}"
          >
              <i class="feather icon-trash-2 danger"></i>
          </a>
        </small>
      -->
      </div>
      <p class="mb-0">{{ __('No data available') }}</p>
    </div>
  </div>
</div>

<input type="hidden" name="features" id="features">

@section('vendor-script')
  @parent
  <!-- vendor files -->
  <script src="{{ asset(mix('vendors/js/mask/jquery.mask.min.js')) }}"></script>
@endsection
@section('page-script')
  @parent
  <!-- Page js files -->
  <script>
    $(function() {
      let features = {!! isset($data) ? $data->features->toJson() : collect([])->toJson() !!};
      let featureTypes = {!! collect($featureTypes)->toJson() !!};
      let featureOrigin = {!! collect($featureOrigin)->toJson() !!};
      $('.money').mask("#,##0.00", {reverse: true});
      renderFeatures();
      disabledOriginInput();

      $('#feature_type').change(function () {
        disabledOriginInput();
      });

      function disabledOriginInput() {
        if ($('#feature_type').val() == 'advancement' || $('#feature_type').val() == 'inscription') {
          $('#feature_origin option[value="base"]').prop('selected', true);
          $('#feature_origin').attr('disabled', 'disabled');
        } else
          $('#feature_origin').removeAttr('disabled');
      }


      $('#btn-add').click(function () {
        let type = $('#feature_type').val();
        let origin = $('#feature_origin').val();
        let isPercentage = $('.is_percentage:checked').val() == 1;
        let value = $('#feature_value').val();

        if (type && value){
          features.push({
            type: type,
            origin: origin,
            is_percentage: isPercentage,
            value: parseFloat(value)
          });

          renderFeatures();
        }
      })

      function renderFeatures() {
        $('#list-features .list-group-item').remove();

        if (features.length == 0)
          renderEmpty()
        else {
          $.each(features, function (i, row) {
            let msg = `${parseFloat(row.value)}`;
            if (row.type != 'inscription')
              msg += ` => ${featureOrigin[row.origin]}`

            $('#list-features').append(`
              <div class="list-group-item list-group-item-action">
                <div class="d-flex w-100 justify-content-between">
                  <h5 class="mb-0">${featureTypes[row.type]}</h5>
                  <small>
                    <a
                      href="#"
                      class="feature-remove"
                      data-key="${i}"
                      data-toggle="tooltip"
                      data-original-title="{{__('Remove')}}"
                    >
                        <i class="feather icon-trash-2 danger"></i>
                    </a>
                  </small>
                </div>
                <p class="mb-0">
                  ${row.is_percentage ? '%' : '$'}
                  ${msg}
                </p>
              </div>
            `)
          })

          $('input#features').val(JSON.stringify(features))
        }
      }

      function renderEmpty() {
        $('#list-features').append(`
          <div class="list-group-item list-empty text-center" style="background-color: whitesmoke">
            <p class="mb-0">{{ __('No data available') }}</p>
          </div>
        `);
      }

      $(document).on('click', '.feature-remove', function (e) {
        e.preventDefault()
        const key = $(this).data('key')
        let arry = []

        $.each(features, function(i,row) {
            if (i != key)
                arry.push(row)
        })
        features = arry

        renderFeatures()
      })

    })
  </script>
@endsection
