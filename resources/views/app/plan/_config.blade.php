<div class="form-group col-sm-6">
  @if (!$franchise->is_multibranch)
    <label>{{ __('Branch') }}</label>
    {{ Form::hidden('branch_id', $branches->first()->id, ['id' => 'branch_id']) }}
  @else
    <label>{{ __('Branches') }}</label>
  @endif
  {{
    Form::select(
      'branches[]',
      $branches->pluck('name', 'id'),
      old('branches') ?? (!$franchise->is_multibranch ? $branches->first()->id : null),
      [
        'id' => $franchise->is_multibranch ? 'branch_id' : '',
        'class' => 'form-control select2'.($errors->has('branch') ? ' is-invalid' : '' ),
        'required',
        'multiple',
        !$franchise->is_multibranch ? 'disabled' : ''
      ]
    )
  }}
</div>

<div class="col-sm-6 form-group">
  <div class="controls">
    <label>{{ __('Code') }}</label>
    {{
      Form::text(
        'plan[code]',
        old('plan.code'),
        [
          'id' => 'code',
          'class' => 'form-control',
          'required'
        ]
      )
    }}
  </div>
</div>

<div class="col-sm-6 form-group">
  <div class="controls">
    <label>{{ __('Name') }}</label>
    {{
      Form::text(
        'plan[name]',
        old('plan.name'),
        [
          'id' => 'name',
          'class' => 'form-control',
          'required'
        ]
      )
    }}
  </div>
</div>

<div class="col-sm-6 form-group">
  <label>{{ __('Payment type') }}</label>
  {{
    Form::select(
      'plan[interval_unit]',
      $intervals,
      old('plan.interval_unit') ?? (isset($data) ? $data->interval_unit->value() : 'month'),
      [
        'id' => 'interval_unit',
        'class' => 'form-control',
        'required',
      ]
    )
  }}
</div>

<div class="form-group col-sm-6">
  {{ __('Payment interval') }}
  {{
    Form::number(
      'plan[interval_count]',
      old('plan.interval_count') ?? (isset($data) ? $data->interval_count : 1),
      [
        'id' => 'interval_count',
        'class' => 'form-control',
        'required'
      ]
    )
  }}
</div>

<div class="form-group col-sm-6">
  <div class="controls">
    <label>{{ __('Default') }}</label>
    <ul class="list-unstyled mb-0">
      <li class="d-inline-block mr-2">
        <fieldset>
         <div class="vs-radio-con">
            {{  Form::radio("plan[is_default]", true) }}
            <span class="vs-radio">
              <span class="vs-radio--border"></span>
              <span class="vs-radio--circle"></span>
            </span>
            <span class="">{{ __('Yes') }}</span>
          </div>
        </fieldset>
      </li>
      <li class="d-inline-block mr-2">
        <fieldset>
         <div class="vs-radio-con">
            {{  Form::radio("plan[is_default]", false, true) }}
            <span class="vs-radio">
              <span class="vs-radio--border"></span>
              <span class="vs-radio--circle"></span>
            </span>
            <span class="">{{ __('No') }}</span>
          </div>
        </fieldset>
      </li>
    </ul>
  </div>
</div>

@section('page-style')
  @parent
  <!-- Page css files -->
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/select/select2.min.css')) }}">
@endsection
@section('vendor-script')
  @parent
  <!-- vendor files -->
  <script src="{{ asset(mix('vendors/js/select/select2.full.min.js')) }}"></script>
@endsection
@section('page-script')
  @parent
  <!-- Page js files -->
  <script>
    $(function() {
      $(".select2").select2({
        dropdownAutoWidth: true,
        width: '100%'
      });

      $('#interval_count').change(function () {
        if ($(this).val() <= 0)
          $(this).val(1)
      })
    })
  </script>
@endsection
