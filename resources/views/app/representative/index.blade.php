
@extends('layouts/default')

@section('title', __('Representatives'))

@section('vendor-style')
  {{-- vendor css files --}}
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/animate/animate.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
@endsection

@section('btn-action')
  @can('representatives.create')
    <a href="{{ tenant_route('representatives.create') }}" class="btn btn-outline-primary">
      <span>
        <i class="feather icon-plus"></i> {{ __('Add New') }}
      </span>
    </a>
  @endcan
@endsection

@section('content')
<section id="basic-datatable">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-content">
            <div class="card-body card-dashboard">
              <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                  <a
                    class="nav-link active"
                    id="active-tab"
                    data-toggle="tab"
                    href="#active"
                    aria-controls="active"
                    role="tab"
                    aria-selected="true"
                  >
                    {{ __('Active') }}
                  </a>
                </li>
                <li class="nav-item">
                  <a
                    class="nav-link"
                    id="trash-tab"
                    data-toggle="tab"
                    href="#trash"
                    aria-controls="trash"
                    role="tab"
                    aria-selected="true"
                  >
                    {{ __('In trash') }}
                  </a>
                </li>
              </ul>
              <div class="tab-content pt-2">
                <div class="tab-pane active" id="active" aria-labelledby="active-tab" role="tabpanel">
                  @include('app.representative._table', ['rows' => $rows['active']])
                </div>
                <div class="tab-pane" id="trash" aria-labelledby="trash-tab" role="tabpanel">
                  @include('app.representative._table', ['rows' => $rows['trash']])
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</section>
@endsection
@section('vendor-script')
{{-- vendor files --}}
  <script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/extensions/polyfill.min.js')) }}"></script>
@endsection
@section('page-script')
<script>
  var redirectStudent = "{{ tenant_route('students.edit', ['status' => 'active', 'student' => ':id']) }}";
  var routeDelete = "{{ tenant_route('representatives.destroy', ':id') }}";
  var routeIndex = "{{ tenant_route('representatives.index') }}";
  var csrfToken = "{{csrf_token()}}";
  var configTable = {
    ordering: false,
    columnDefs: [{
      targets: 'sort',
      orderable: true,
    }],
    language: {
      url: "{{ asset('json/datatable/'.App::getLocale().'.json') }}"
    }
  }

  var lang = {
    are_you_sure : "{{__('Are you sure?')}}",
    yes_sure :"{{__('Yes, sure!')}}",
    no_cancel: "{{__('No, cancel!')}}",
    updated: "{{__('Updated!')}}",
    ok: "{{__('Ok')}}",
    oops: "{{ __('Oops...') }}"
  }

  let managerUrl = "{{ tenant_route('representatives.manager') }}";
  $('.btn-student').click(function () {
    let data = {
      id: $(this).data('id'),
      type: $(this).data('type'),
      model: $(this).data('model')
    };
    data['_token'] = csrfToken;

    Swal.fire({
      title: lang.are_you_sure,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#2ecc71',
      confirmButtonText: lang.yes_sure,
      cancelButtonText: lang.no_cancel,
      showLoaderOnConfirm: true,
      inputAttributes: {
        autocapitalize: 'off'
      },
      preConfirm: () => {
        return fetch(managerUrl, {
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
          method: "POST",
          body: JSON.stringify(data)
        }).catch(error => {
            Swal.showValidationMessage(
              `Request failed: ${error}`
            )
        })
      },
      allowOutsideClick: () => !Swal.isLoading()
    }).then((result) => {
      if (result.value && result.value.ok) {
          Swal.fire({
            title: lang.updated,
            type:'success',
            confirmButtonText: lang.ok,
          }).then(() => {
            window.location.href = redirectStudent.replace(':id', data.id)
          })
      } else {
          if ($(`#row-${data.id}`))
              $(`#row-${data.id}`).toggleClass('table-info')

          if (result.value)
              Swal.fire({
                position: 'top-end',
                type: 'error',
                title: lang.oops,
                text: result.value.statusText,
                showConfirmButton: false,
                timer: 2500
              })
      }
    })
  })
</script>
<script src="{{ asset(mix('js/scripts/basic-crud.js')) }}"></script>
@endsection
