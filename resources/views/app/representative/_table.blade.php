<div class="table-responsive">
  <table class="table">
    <thead>
        <tr>
          <th></th>
          <th class="sort">{{__('Name')}}</th>
          <th>{{__('E-Mail')}}</th>
          <th  class="sort">{{__('Represented')}}</th>
          <th>{{__('Last login')}}</th>
          <th class="no-sort" style="width: 15%">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
      @foreach($rows as $row)
        <tr id="row-{{ $row->hid }}">
          <td>
            <img class="round"
              src="{{ $row->profile_photo }}"
              alt="avatar"
              height="40"
              width="40"
              class="m-r-1"
            />
          </td>
          <td>
            <a href="{{ tenant_route('representatives.show', $row->hid) }}">
              {{ $row->fullname }}
            </a>
          </td>
          <td>
            {{ $row->email }}
          </td>
          <td>
            <span class="badge badge-info">
              0
            </span>
          </td>
          <td>
            {{ $row->last_login_at ? $row->last_login_at->diffForHumans() : __('Never') }}
          </td>
          <td class="text-left">
            @if (!$row->hasRole('student'))
              @if(!$row->trashed())
                @can('representatives.edit')
                  <a
                    href="{{ tenant_route('representatives.edit', $row->hid) }}"
                    class="btn btn-sm btn-primary btn-edit m-r-5"
                    data-toggle="tooltip"
                    data-original-title="{{__('Edit')}}"
                  >
                    <i class="fa fa-pencil font-14"></i>
                  </a>
                @endcan
                @can('representatives.manager')
                  <button
                    type="button"
                    data-id="{{ $row->hid }}"
                    class="btn btn-sm btn-info btn-student m-r-5"
                    data-toggle="tooltip"
                    data-type="student"
                    data-model="role"
                    data-original-title="{{__('Become a student')}}"
                  >
                    <i class="fa fa-smile-o font-14"></i>
                  </button>
                @endcan
              @else
                <button
                    type="button"
                    data-id="{{ $row->hid }}"
                    class="btn btn-sm btn-primary btn-restore m-r-5"
                    data-toggle="tooltip"
                    data-type="restore"
                    data-original-title="{{__('Restore')}}">
                        <i class="fa fa-history font-14"></i>
                </button>
              @endif
              @can('representatives.destroy')
                <button
                  type="button"
                  data-id="{{ $row->hid }}"
                  class="btn btn-sm btn-danger btn-delete"
                  data-toggle="tooltip"
                  data-original-title="{{
                    !$row->trashed()
                        ? __('Remove')
                        : __('Permanently deleting')
                }}"
                >
                  @if(!$row->trashed())
                    <i class="fa fa-trash-o font-14"></i>
                  @else
                    <i class="fa fa-trash font-14"></i>
                  @endif
                </button>
              @endcan
            @endif
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
</div>
