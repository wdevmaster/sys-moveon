<fieldset class="mb-2 pt-2 group-logs">
  <div class="input-group">
    <input id="{{ $type }}-body" class="form-control" type="text">
    <div class="input-group-append">
      {{
        Form::select(
            '',
            $select,
            null,
            [
                'id' => $type.'-type',
                'class' => 'form-control',
                'placeholder' => __('Select :name', [ 'name' => __('type') ]),
                'sytle' => 'border-radius: 0'
            ]
        )
    }}
    </div>
    <div class="input-group-append">
      <button class="btn btn-primary log-add" data-group="{{ $type }}" type="button">
        <i class="feather icon-plus"></i> {{ __('Add') }}
      </button>
    </div>
  </div>
</fieldset>
<div class="list-group {{ $type }}-render-log">
</div>

@section('page-style')
  @parent
  <!--page__input-{{ $type }}.css-->
  <style>
    .input-group-append select {
      border-radius: 0px !important;
    }
  </style>
@endsection

@section('page-script')
  @parent
  <!--page__input-{{ $type }}.js-->
  <script>
    $(`.log-add`).click(function() {
      funcAddLog($(this))
    });
  </script>
@endsection
