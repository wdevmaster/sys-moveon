@php
  if (isset($pageConfigs)) {
    Helper::updatePageConfig($pageConfigs);
  }
$configData = Helper::applClasses();
@endphp

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" data-textdirection="ltr">

@include('elements/head')

<body
    class="vertical-layout vertical-menu-modern 1-column {{ $configData['blankPageClass'] }} {{ $configData['bodyClass'] }} {{($configData['theme'] === 'light') ? '' : $configData['theme'] }}"
    data-menu="vertical-menu-modern" data-col="1-column" data-layout="{{ $configData['theme'] }}">

    <!-- BEGIN: Content-->
    <div id="app" class="app-content content">
        <div class="content-wrapper">
            <div class="content-body">

                {{-- Include Startkit Content --}}
                @yield('content')

            </div>
        </div>
    </div>
    <!-- End: Content-->

    {{-- include default scripts --}}
    @include('elements/scripts')

</body>

</html>
