<fieldset class="form-label-group form-group position-relative has-icon-left">
  <input
    type="text"
    name="username"
    class="form-control @error('username') is-invalid @enderror"
    id="user-name"
    placeholder="{{ __('Username') }}"
    value="{{ old('username') }}"
    required
  >
  <label for="user-name">{{ __('Username') }}</label>
  @error('username')
    <div class="invalid-feedback">
      <strong>{{ $message }}</strong>
    </div>
  @enderror
</fieldset>

<fieldset class="form-label-group form-group position-relative has-icon-left">
  <input
    type="email"
    name="email"
    class="form-control @error('email') is-invalid @enderror"
    id="user-name"
    placeholder="{{ __('E-Mail') }}"
    value="{{ old('email') }}"
    required
  >
  <label for="user-name">{{ __('E-Mail') }}</label>
  @error('email')
    <div class="invalid-feedback">
      <strong>{{ $message }}</strong>
    </div>
  @enderror
</fieldset>

<fieldset class="form-label-group position-relative has-icon-left">
  <input
    type="password"
    name="password"
    class="form-control @error('password') is-invalid @enderror"
    id="user-password"
    placeholder="{{ __('Password') }}"
    required
  >
  <label for="user-password">{{ __('Password') }}</label>
  @error('password')
    <div class="invalid-feedback">
      <strong>{{ $message }}</strong>
    </div>
  @enderror
</fieldset>

<fieldset class="form-label-group position-relative has-icon-left">
  <input
    type="password"
    name="password_confirmation"
    class="form-control @error('password_confirmation') is-invalid @enderror"
    id="user-password_confirmation"
    placeholder="{{ __('Confirm Password') }}"
    required
  >
  <label for="user-password">{{ __('Confirm Password') }}</label>
</fieldset>
