@extends('layouts/auth')

@section('title', __('Register'))

@section('page-style')
{{-- Page Css files --}}
<link rel="stylesheet" href="{{ asset(mix('css/pages/authentication.css')) }}">
@endsection

@section('content')
<section class="row flexbox-container">
  <div class="col-xl-8 col-11 d-flex justify-content-center">
    <div class="card bg-authentication rounded-0 mb-0">
      <div class="row m-0">
        <div class="col-lg-6 d-lg-block d-none text-center align-self-center px-1 py-0">
          <img src="{{ asset('images/pages/login.png') }}" alt="branding logo">
        </div>
        <div class="col-lg-6 col-12 p-0">
          <div class="card rounded-0 mb-0 px-2">
            <div class="card-header pb-1">
              <div class="card-title">
                <h4 class="mb-0">{{ __('Start learning ') }}</h4>
              </div>
            </div>
            <div class="card-content pb-2">
              <div class="card-body pt-1">
                {!! Form::open(['route' => 'tenant.register']) !!}

                  <div class="row">
                    <div class="col-sm-12 form-group">
                      <div class="controls">
                        <label>{{ __('For whom it is') }}</label>
                        <ul class="list-unstyled mb-0">
                          <li class="d-inline-block mr-2">
                            <fieldset>
                             <div class="vs-radio-con">
                                {{  Form::radio("role", 'student', true) }}
                                <span class="vs-radio">
                                  <span class="vs-radio--border"></span>
                                  <span class="vs-radio--circle"></span>
                                </span>
                                <span class="">{{ __('For me') }}</span>
                              </div>
                            </fieldset>
                          </li>

                          <li class="d-inline-block mr-2">
                            <fieldset>
                             <div class="vs-radio-con">
                                {{  Form::radio("role", 'representative') }}
                                <span class="vs-radio">
                                  <span class="vs-radio--border"></span>
                                  <span class="vs-radio--circle"></span>
                                </span>
                                <span class="">{{ __('For my children ') }}</span>
                              </div>
                            </fieldset>
                          </li>
                        </ul>
                      </div>
                    </div>

                    <div class="col-sm-6 form-group">
                      <div class="controls">
                        <label>{{ __('Firstname') }}</label>
                        {{
                          Form::text(
                            'first_name',
                            old('first_name'),
                            [
                                'id' => 'firstname',
                                'class' => 'form-control',
                                'required'
                            ]
                          )
                        }}
                      </div>
                    </div>

                    <div class="col-sm-6 form-group">
                      <div class="controls">
                        <label>{{ __('Lastname') }}</label>
                        {{
                          Form::text(
                            'last_name',
                            old('last_name'),
                            [
                                'id' => 'lastname',
                                'class' => 'form-control',
                                'required'
                            ]
                          )
                        }}
                      </div>
                    </div>

                    <div class="col-sm-12 form-group">
                      <label>{{ __('E-mail') }}</label>
                      {{
                        Form::email(
                          'email',
                          old('user.email'),
                          [
                            'id' => 'user-name',
                            'class' => 'form-control'.($errors->has('email') ? ' is-invalid' : '' ),
                            'required',
                          ]
                        )
                      }}
                      @error('email')
                        <div class="invalid-feedback">
                          <strong>{{ $message }}</strong>
                        </div>
                      @enderror
                    </div>

                    <div class="col-sm-6 form-group">
                      <label>{{ __('Country') }}</label>
                      {{
                        Form::select(
                          'country_str',
                          $countries,
                          old('country_str'),
                          [
                              'id' => 'country_str',
                              'class' => 'form-control',
                              'placeholder' => __('Select :name', [ 'name' => '...' ]),
                              'required'
                          ]
                        )
                      }}
                    </div>

                    <div class="col-sm-6 form-group">
                      <label>{{ __('Languages') }}</label>
                      {{
                        Form::select(
                          'language',
                          $languages,
                          old('language'),
                          [
                            'id' => 'language',
                            'class' => 'form-control',
                            'placeholder' => __('Select :name', [ 'name' => '...' ]),
                            'required'
                          ]
                        )
                      }}
                    </div>

                    <div class="col-sm-12 form-group">
                      <label>{{ __('Headquarters') }}</label>
                      {{
                        Form::select(
                          'franchise',
                          [],
                          old('franchise'),
                          [
                              'id' => 'franchise',
                              'class' => 'form-control',
                              'placeholder' => __('Select :name', [ 'name' => '...' ]),
                              'required',
                              'disabled'
                          ]
                        )
                      }}
                    </div>

                    <div class="col-sm-12 form-group">
                      <label>{{ __('How did you meet us?') }}</label>
                      {{
                        Form::select(
                          'origin',
                          $origins,
                          old('origin'),
                          [
                            'id' => 'origin',
                            'class' => 'form-control',
                            'placeholder' => __('Select :name', [ 'name' => '...' ]),
                            'required'
                          ]
                        )
                      }}
                    </div>

                  </div>

                  <button
                    type="submit"
                    class="btn btn-primary float-right btn-inline mb-50"
                  >
                    {{ __('Register') }}
                  </button>

                {!! Form::close() !!}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@section('page-script')
  @parent
  <script>
    $('#country_str').change(function () {
      getFranchises();
    });

    $('#language').change(function () {
      getFranchises();
    });


    function getFranchises() {
      let url = "{{ route('get.franchises') }}";
      let country = $('#country_str').val()
      let language = $('#language').val()

      if (country && language)
        $.ajax({
          method: 'GET',
          url: `${url}?c=${country}&l=${language}`
        }).done(function (res) {
          console.log(res)
          $('#franchise option').remove();
          $('#franchise').removeAttr('disabled');

          $('#franchise').append(
            $("<option></option>").attr('value', '').text("{{ __('Select :name', [ 'name' => '...' ]) }}")
          );

          $.each(res, function (key, value) {
            $('#franchise').append(
              $("<option></option>").attr('value', key).text(value)
            );
          })

        })
    }
  </script>
@endsection
