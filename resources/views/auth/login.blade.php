@extends('layouts/auth')

@section('title', __('Login'))

@section('page-style')
{{-- Page Css files --}}
<link rel="stylesheet" href="{{ asset(mix('css/pages/authentication.css')) }}">
@endsection

@section('content')
<section class="row flexbox-container">
  <div class="col-xl-8 col-11 d-flex justify-content-center">
    <div class="card bg-authentication rounded-0 mb-0">
      <div class="row m-0">
        <div class="col-lg-6 d-lg-block d-none text-center align-self-center px-1 py-0">
          <img src="{{ asset('images/pages/register.jpg') }}" alt="branding logo">
        </div>
        <div class="col-lg-6 col-12 p-0">
          <div class="card rounded-0 mb-0 px-2">
            <div class="card-header pb-1">
              <div class="card-title">
                <h4 class="mb-0">{{ __('Login') }}</h4>
              </div>
            </div>
            <p class="px-2">{{ __('Welcome back, please login to your account.') }}</p>
            <div class="card-content pb-2">
              <div class="card-body pt-1">
                {!! Form::open(['route' => request()->is('app/*') ? 'tenant.login' : 'system.login']) !!}
                  @if (request()->is('app/*'))
                    <fieldset class="form-label-group form-group position-relative has-icon-left">
                      <input
                        type="text"
                        name="ping"
                        class="form-control @error('ping') is-invalid @enderror"
                        id="user-name"
                        placeholder="{{ __('PING') }}"
                        value="{{ old('ping') }}"
                        required
                      >
                      <label for="user-name">{{ __('PING') }}</label>
                      @error('ping')
                        <div class="invalid-feedback">
                          <strong>{{ $message }}</strong>
                        </div>
                      @enderror
                    </fieldset>
                  @else
                    <input type="hidden" name="system" value="{{ true }}">
                  @endif

                  <fieldset class="form-label-group form-group position-relative has-icon-left">
                    <input
                      type="text"
                      name="username"
                      class="form-control @error('username') is-invalid @enderror"
                      id="user-name"
                      placeholder="{{ __('Username') }} {{ __('Or') }} {{ __('E-Mail') }}"
                      value="{{ old('username') }}"
                      required
                    >
                    <label for="user-name">{{ __('Username') }} {{ __('Or') }} {{ __('E-Mail') }}</label>
                    @error('username')
                      <div class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                      </div>
                    @enderror
                  </fieldset>

                  <fieldset class="form-label-group position-relative has-icon-left">
                    <input
                      type="password"
                      name="password"
                      class="form-control @error('password') is-invalid @enderror"
                      id="user-password"
                      placeholder="{{ __('Password') }}"
                      required
                    >
                    <label for="user-password">{{ __('Password') }}</label>
                    @error('password')
                      <div class="invalid-feedback">
                        <strong>{{ $message }}</strong>
                      </div>
                    @enderror
                  </fieldset>
                  <div class="form-group d-flex justify-content-between align-items-center">
                    <div class="text-left">
                      <fieldset class="checkbox">
                        <div class="vs-checkbox-con vs-checkbox-primary">
                          <input type="checkbox" name="remember">
                          <span class="vs-checkbox">
                            <span class="vs-checkbox--check">
                              <i class="vs-icon feather icon-check"></i>
                            </span>
                          </span>
                          <span class="">{{ __('Remember me') }}</span>
                        </div>
                      </fieldset>
                    </div>
                    <!--
                    <div class="text-right">
                      <a href="auth-forgot-password" class="card-link">{{ __('Forgot Password?') }}</a>
                    </div>
                    -->
                  </div>
                  <button type="submit" class="btn btn-primary btn-block">
                    {{ __('Login') }}
                  </button>
                {!! Form::close() !!}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
