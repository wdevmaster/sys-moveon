<?php

return [
    'username'          => __('Username'),
    'email'             => __('E-Mail'),
    'password'          => __('Password'),
    'name'              => __('Name'),
    'country_str'       => __('Country'),
    'city_str'          => __('City'),
    'address'           => __('Address'),
    'first_name'        => __('Firstname'),
    'last_name'         => __('Lastname'),
    'birthdate'         => __('Birthdate'),
    'phone'             => __('Phone'),
    'emergency_phone'   => __('Emergency phone'),
    'gender'            => __('Gender'),
    'branch_id'         => __('Branch'),
    'capacity'          => __('Capacity')
];
